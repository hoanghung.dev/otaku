<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\Product;

use App\Models\Product;

class EloquentProduct implements ProductRepository {

    public function getById($id) {
        $key = 'product_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $product = product::find($id);
        if($product != null) {
            cache_it($key, $product);
            return $product;
        }
        return false;
    }

    public function getByCategoryId($select, $categoryId, $order, $limit) {
        $key = 'allproduct_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listProduct = product::select($select)->where('status',1)->where('category_id',$categoryId)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listProduct != null) {
            cache_it($key, $listProduct);
            return $listProduct;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'product_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $product = Product::where('slug', $slug)->first();
        if($product != null) {
            cache_it($key, $product);
            return $product;
        }
        return false;
    }

    public function getByName($name) {
        $key = 'product_' . env('APP_SITE') . '_' . $name;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $product = Product::where('name', $name)->first();
        if($product != null) {
            cache_it($key, $product);
            return $product;
        }
        return false;
    }

    public function getSearchName($name) {
        $key = 'product_' . env('APP_SITE') . '_' . $name;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listProduct = Product::where('name', 'like', $name . '%')->limit(3)->get();
        if($listProduct != null) {
            cache_it($key, $listProduct);
            return $listProduct;
        }
        return false;
    }

    public function searchName($select, $name, $order, $limit) {
        $key = 'allproduct_' . env('APP_SITE') . '_' . md5(serialize($select) . $name . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listProduct = product::select($select)->where('status',1)->where('name', 'like','%'.$name.'%')->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listProduct != null) {
            cache_it($key, $listProduct);
            return $listProduct;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'allproduct_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) $listProduct = product::select($select)->whereRaw($where)->orderBy($order['col'], $order['mode'])->paginate($limit);
        else $listProduct = product::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listProduct != null) {
            cache_it($key, $listProduct);
            return $listProduct;
        }
        return false;
    }

    public function insert($data, $tags) {
        $result = product::create($data);
        if($result !== false) {
            if($tags) {
                foreach($tags as $tag) {
                    $result->tag($tag);
                }
            }
            return $result;
        }
        return false;
    }

    public function update($id, $data, $tags) {
        $product = product::find($id);
        if($product != null) {
            foreach($data as $item => $value) {
                $product->$item = $value;
            }
            if($product->save()) {
                $product->untag();
                if($tags) {
                    foreach($tags as $tag) {
                        $product->tag($tag);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $product = product::find($id);
        if($product != null) {
            if($product->delete()) {
                return true;
            }
        }
        return false;
    }

    public function publish($id) {
        $product = product::find($id);
        if($product != null) {
            if($product->status == 1) {
                $product->status = 0;
                $product->save();
                return 0;
            } else {
                $product->status = 1;
                $product->save();
                return 1;
            }
        }
        return false;
    }
}