<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:12
 */
namespace App\Repositories\Product;

interface ProductRepository {

    public function getById($id);

    public function getBySlug($slug);

    public function getByName($name);

    public function getSearchName($name);

    public function searchName($select, $name, $order, $limit);

    public function getByCategoryId($select, $categoryId, $order, $limit);

    public function getAll($select, $where, $order, $limit);

    public function insert($data, $tags);

    public function update($id, $data, $tags);

    public function delete($id);

    public function publish($id);
}