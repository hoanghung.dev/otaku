<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:21
 */
namespace App\Repositories\CategoryManufacturer;

use App\Models\CategoryManufacturer;
use App\Models\Manufacturer;

class EloquentCategoryManufacturer implements CategoryManufacturerRepository {

    public function getById($id) {
        $key = 'CategoryManufacturer_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $categoryManufacturer = CategoryManufacturer::find($id);
        if($categoryManufacturer != null) {
            cache_it($key, $categoryManufacturer);
            return $categoryManufacturer;
        }
        return false;
    }

    public function getByCategoryId($id) {
        $manufacturersId = CategoryManufacturer::where('category_id', $id)->get();
        if($manufacturersId != null) {
            return $manufacturersId;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'allCategoryManufacturer_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) {
            $listCategoryManufacturer = CategoryManufacturer::whereRaw($where)->select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        } else {
            $listCategoryManufacturer = CategoryManufacturer::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        }
        if($listCategoryManufacturer != null) {
            cache_it($key, $listCategoryManufacturer);
            return $listCategoryManufacturer;
        }
        return false;
    }

    public function insert($categoryId, $manufacturerName) {
        $manufacturer = Manufacturer::where('name', 'like', '%'.$manufacturerName.'%')->first();

        if($manufacturer !== null) {
            $result = CategoryManufacturer::create(['category_id'=>$categoryId, 'manufacturer_id' => $manufacturer->id]);
            if($result !== false) {
                return $result;
            }
        }
        return false;
    }

    public function update($categoryId, $manufacturerName) {
        $categoryManufacturer = CategoryManufacturer::where('category_id', $categoryId)->get();
        foreach ($categoryManufacturer as $item) {
            $item->delete();
        }

        foreach ($manufacturerName as $item) {
            $manufacturer = Manufacturer::where('name', 'like', '%'.$item.'%')->first();

            if($manufacturer !== null) {
                CategoryManufacturer::create(['category_id'=>$categoryId, 'manufacturer_id' => $manufacturer->id]);
            }
        }
        return true;
    }

    public function delete($id) {
        $categoryManufacturer = CategoryManufacturer::find($id);
        if($categoryManufacturer != null) {
            if($categoryManufacturer->delete()) {
                return true;
            }
        }
        return false;
    }
}