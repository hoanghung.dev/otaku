<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:20
 */
namespace App\Repositories\CategoryManufacturer;

interface CategoryManufacturerRepository {

    public function getById($id);

    public function getByCategoryId($id);

    public function getAll($select, $where, $order, $limit);

    public function insert($categoryId, $manufacturerName);

    public function update($categoryId, $manufacturerName);

    public function delete($id);
    
}