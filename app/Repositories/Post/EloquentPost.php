<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\Post;

use App\Models\Post;

class EloquentPost implements PostRepository {

    public function getById($id) {
        $key = 'post_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $post = Post::find($id);
        if($post != null) {
            cache_it($key, $post);
            return $post;
        }
        return false;
    }

    public function getByCategoryId($select, $categoryId, $order, $limit) {
        $key = 'allpost_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listPost = Post::select($select)->where('status', 1)->where('category_id',$categoryId)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listPost != null) {
            cache_it($key, $listPost);
            return $listPost;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'post_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $post = Post::where('slug', $slug)->first();
        if($post != null) {
            cache_it($key, $post);
            return $post;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'allPost_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) $listPost = Post::select($select)->whereRaw($where)->orderBy($order['col'], $order['mode'])->paginate($limit);
        else $listPost = Post::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listPost != null) {
            cache_it($key, $listPost);
            return $listPost;
        }
        return false;
    }

    public function insert($data, $tags) {
        $result = Post::create($data);
        if($result !== false) {
            if($tags) {
                foreach($tags as $tag) {
                    $result->tag($tag);
                }
            }
            return $result;
        }
        return false;
    }

    public function update($id, $data, $tags) {
        $post = Post::find($id);
        if($post != null) {
            foreach($data as $item => $value) {
                $post->$item = $value;
            }
            if($post->save()) {
                $post->untag();
                if($tags) {
                    foreach($tags as $tag) {
                        $post->tag($tag);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $post = Post::find($id);
        if($post != null) {
            if($post->delete()) {
                return true;
            }
        }
        return false;
    }

    public function getRelation($id, $select, $limit) {
        $key = 'postRelation_' . env('APP_SITE') . '_' . $id . $limit;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $post = Post::find($id);
        if($post != null) {
            $listPost = Post::select($select)->where('id', '!=', $id)->where('status', 1)->where('category_id', $post->category_id)->orderBy('created_at', 'desc')->paginate($limit);
            if($listPost != null) {
                cache_it($key, $listPost);
                return $listPost;
            }
        }
        return false;
    }
}