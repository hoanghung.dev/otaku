<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:20
 */
namespace App\Repositories\User;

interface UserRepository {

    public function getById($id);

    public function getBySlug($slug);

    public function getAll($select, $where, $order, $limit);

    public function insert($data);

    public function update($id, $data);

    public function delete($id);
}