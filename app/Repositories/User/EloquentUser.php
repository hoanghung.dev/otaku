<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:21
 */
namespace App\Repositories\User;

use App\Models\User;

class EloquentUser implements UserRepository {

    public function getById($id) {
        $key = 'user_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $user = User::find($id);
        if($user != null) {
            cache_it($key, $user);
            return $user;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'user_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $user = User::where('slug', $slug)->first();
        if($user != null) {
            cache_it($key, $user);
            return $user;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'alluser_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) {
            $listUser = User::whereRaw($where)->select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        } else {
            $listUser = User::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        }
        if($listUser != null) {
            cache_it($key, $listUser);
            return $listUser;
        }
        return false;
    }

    public function insert($data) {
        $result = User::create($data);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    public function update($id, $data) {
        $user = User::find($id);
        if($user != null) {
            foreach($data as $item => $value) {
                $user->$item = $value;
            }
            if($user->save()) {
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $user = User::find($id);
        if($user != null) {
            if($user->delete()) {
                return true;
            }
        }
        return false;
    }
}