<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:21
 */
namespace App\Repositories\Category;

use App\Models\Category;

class EloquentCategory implements CategoryRepository {

    public function getById($id) {
        $key = 'category_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $category = Category::find($id);
        if($category != null) {
            cache_it($key, $category);
            return $category;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'category_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $category = Category::where('slug', $slug)->first();
        if($category != null) {
            cache_it($key, $category);
            return $category;
        }
        return false;
    }

    public function searchByName($name) {
        $key = 'category_' . env('APP_SITE') . '_' . $name;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $category = Category::where('name', 'like', '%' . $name . '%')->first();
        if($category != null) {
            cache_it($key, $category);
            return $category;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'allCategory_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) {
            $listCategory = Category::whereRaw($where)->select($select)->orderBy($order['col'], $order['mode']);
        } else {
            $listCategory = Category::select($select)->orderBy($order['col'], $order['mode']);
        }
        if($limit) $listCategory = $listCategory->paginate($limit);
        else $listCategory = $listCategory->get();

        if($listCategory != null) {
            cache_it($key, $listCategory);
            return $listCategory;
        }
        return false;
    }

    public function insert($data) {
        $result = Category::create($data);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    public function update($id, $data) {
        $category = Category::find($id);
        if($category != null) {
            foreach($data as $item => $value) {
                $category->$item = $value;
            }
            if($category->save()) {
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $category = Category::find($id);
        if($category != null) {
            if($category->delete()) {
                return true;
            }
        }
        return false;
    }
}