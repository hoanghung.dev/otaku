<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\Order;

use App\Models\Order;

class EloquentOrder implements OrderRepository {

    public function getById($id) {
        $key = 'order_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $order = order::find($id);
        if($order != null) {
            cache_it($key, $order);
            return $order;
        }
        return false;
    }

    public function getByBillId($select, $billId, $order, $limit) {
        $key = 'allorder_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listOrder = order::select($select)->where('bill_id',$billId)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listOrder != null) {
            cache_it($key, $listOrder);
            return $listOrder;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'order_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $order = order::where('slug', $slug)->first();
        if($order != null) {
            cache_it($key, $order);
            return $order;
        }
        return false;
    }

    public function searchName($select, $name, $order, $limit) {
        $key = 'allorder_' . env('APP_SITE') . '_' . md5(serialize($select) . $name . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listOrder = order::select($select)->where('name', 'like','%'.$name.'%')->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listOrder != null) {
            cache_it($key, $listOrder);
            return $listOrder;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'allorder_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) $listOrder = order::select($select)->whereRaw($where)->orderBy($order['col'], $order['mode'])->paginate($limit);
        else $listOrder = order::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listOrder != null) {
            cache_it($key, $listOrder);
            return $listOrder;
        }
        return false;
    }

    public function insert($data, $tags) {
        $result = order::create($data);
        if($result !== false) {
            if($tags) {
                foreach($tags as $tag) {
                    $result->tag($tag);
                }
            }
            return $result;
        }
        return false;
    }

    public function update($id, $data, $tags) {
        $order = order::find($id);
        if($order != null) {
            foreach($data as $item => $value) {
                $order->$item = $value;
            }
            if($order->save()) {
                $order->untag();
                if($tags) {
                    foreach($tags as $tag) {
                        $order->tag($tag);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $order = order::find($id);
        if($order != null) {
            if($order->delete()) {
                return true;
            }
        }
        return false;
    }
}