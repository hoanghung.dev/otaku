<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:12
 */
namespace App\Repositories\Order;

interface OrderRepository {

    public function getById($id);

    public function getBySlug($slug);

    public function searchName($select, $name, $order, $limit);

    public function getByBillId($select, $billId, $order, $limit);

    public function getAll($select, $where, $order, $limit);

    public function insert($data, $tags);

    public function update($id, $data, $tags);

    public function delete($id);
}