<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:21
 */
namespace App\Repositories\Bill;

use App\Models\Bill;

class EloquentBill implements BillRepository {

    public function getById($id) {
        $key = 'bill_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $bill = Bill::find($id);
        if($bill != null) {
            cache_it($key, $bill);
            return $bill;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'bill_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $bill = Bill::where('slug', $slug)->first();
        if($bill != null) {
            cache_it($key, $bill);
            return $bill;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'allbill_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) {
            $list_bill = Bill::whereRaw($where)->select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        } else {
            $list_bill = Bill::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        }
        if($list_bill != null) {
            cache_it($key, $list_bill);
            return $list_bill;
        }
        return false;
    }

    public function insert($data) {
        $result = Bill::create($data);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    public function update($id, $data) {
        $bill = Bill::find($id);
        if($bill != null) {
            foreach($data as $item => $value) {
                $bill->$item = $value;
            }
            if($bill->save()) {
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $bill = Bill::find($id);
        if($bill != null) {
            if($bill->delete()) {
                return true;
            }
        }
        return false;
    }
}