<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\Tag;

use App\Models\Tag;

class EloquentTag implements TagRepository {

    public function getById($id) {
        $key = 'tag_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $tag = Tag::find($id);
        if($tag != null) {
            cache_it($key, $tag);
            return $tag;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'tag_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $tag = Tag::where('slug', $slug)->first();
        if($tag != null) {
            cache_it($key, $tag);
            return $tag;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'alltag_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) {
            $listTag = Tag::whereRaw($where)->select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        } else {
            $listTag = Tag::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        }
        if($listTag != null) {
            cache_it($key, $listTag);
            return $listTag;
        }
        return false;
    }

    public function insert($data) {
        $result = Tag::create($data);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    public function update($id, $data) {
        $tag = Tag::find($id);
        if($tag != null) {
            foreach($data as $item => $value) {
                $tag->$item = $value;
            }
            if($tag->save()) {
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $tag = Tag::find($id);
        if($tag != null) {
            if($tag->delete()) {
                return true;
            }
        }
        return false;
    }
}