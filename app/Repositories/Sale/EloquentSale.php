<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\Sale;

use App\Models\Product;
use App\Models\Sale;

class EloquentSale implements SaleRepository {

    public function getById($id) {
        $key = 'sale_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $sale = Sale::find($id);
        if($sale != null) {
            cache_it($key, $sale);
            return $sale;
        }
        return false;
    }

    public function getAll($select, $order, $limit) {
        $key = 'allsale_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listSale = Sale::select($select)->where('id', '!=', 0)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listSale != null) {
            cache_it($key, $listSale);
            return $listSale;
        }
        return false;
    }

    public function insert($data) {
        $result = Sale::create($data);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    public function update($id, $data) {
        $sale = Sale::find($id);
        if($sale != null) {
            foreach($data as $item => $value) {
                $sale->$item = $value;
            }

            if($sale->save()) {
                return $sale;
            }
        }
        return false;
    }

    public function delete($id) {
        $sale = Sale::find($id);
        if($sale != null) {
            $category_id = $sale->category_id;
            // Reset sale_id or product
            if($category_id != ''){     // Delete products.sale_id in list category_id
                foreach(explode(',', $category_id) as $cat_id) {
                    $listProduct = Product::where('category_id', $cat_id)->get();
                    foreach($listProduct as $prd) {
                        $prd->sale_id = '';
                        $prd->save();
                    }
                    unset($prd);
                }
                unset($cat_id);
            }
            $manufacturer_id = $sale->manufacturer_id;
            if($manufacturer_id != ''){     // Delete products.sale_id in list manufacturer_id
                foreach(explode(',', $manufacturer_id) as $manuf_id) {
                    $listProduct = Product::where('manufacturer_id', $manuf_id)->get();
                    foreach($listProduct as $prd) {
                        $prd->sale_id = '';
                        $prd->save();
                    }
                    unset($prd);
                }
                unset($manuf_id);
            }

            if($sale->delete()) {
                return true;
            }
        }
        return false;
    }
}