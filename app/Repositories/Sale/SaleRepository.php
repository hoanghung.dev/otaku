<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:12
 */
namespace App\Repositories\Sale;

interface SaleRepository {

    public function getById($id);

    public function getAll($select, $order, $limit);

    public function insert($data);

    public function update($id, $data);

    public function delete($id);
}