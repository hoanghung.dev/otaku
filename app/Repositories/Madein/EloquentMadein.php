<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\Madein;

use App\Models\Madein;

class EloquentMadein implements MadeinRepository {

    public function getById($id) {
        $key = 'madein_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $madein = Madein::find($id);
        if($madein != null) {
            cache_it($key, $madein);
            return $madein;
        }
        return false;
    }

    public function getByCategoryId($select, $categoryId, $order, $limit) {
        $key = 'allmadein_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listMadein = Madein::select($select)->where('category_id',$categoryId)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listMadein != null) {
            cache_it($key, $listMadein);
            return $listMadein;
        }
        return false;
    }

    public function getAll($select, $order, $limit) {
        $key = 'allmadein_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listMadein = Madein::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listMadein != null) {
            cache_it($key, $listMadein);
            return $listMadein;
        }
        return false;
    }

    public function insert($data) {
        $result = Madein::create($data);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    public function update($id, $data) {
        $madein = Madein::find($id);
        if($madein != null) {
            foreach($data as $item => $value) {
                $madein->$item = $value;
            }
            if($madein->save()) {
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $madein = Madein::find($id);
        if($madein != null) {
            if($madein->delete()) {
                return true;
            }
        }
        return false;
    }
}