<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\ProductRelate;

use App\Models\ProductRelate;

class EloquentProductRelate implements ProductRelateRepository {

    public function getById($id) {
        $key = 'ProductRelate_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $ProductRelate = ProductRelate::find($id);
        if($ProductRelate != null) {
            cache_it($key, $ProductRelate);
            return $ProductRelate;
        }
        return false;
    }

    public function getByCategoryId($select, $categoryId, $order, $limit) {
        $key = 'allProductRelate_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listProductRelate = ProductRelate::select($select)->where('status','publish')->where('category_id',$categoryId)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listProductRelate != null) {
            cache_it($key, $listProductRelate);
            return $listProductRelate;
        }
        return false;
    }

    public function getBySlug($slug) {
        $key = 'ProductRelate_' . env('APP_SITE') . '_' . $slug;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $ProductRelate = ProductRelate::where('slug', $slug)->first();
        if($ProductRelate != null) {
            cache_it($key, $ProductRelate);
            return $ProductRelate;
        }
        return false;
    }

    public function getByName($name) {
        $key = 'ProductRelate_' . env('APP_SITE') . '_' . $name;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $ProductRelate = ProductRelate::where('name', $name)->first();
        if($ProductRelate != null) {
            cache_it($key, $ProductRelate);
            return $ProductRelate;
        }
        return false;
    }

    public function getSearchName($name) {
        $key = 'ProductRelate_' . env('APP_SITE') . '_' . $name;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listProductRelate = ProductRelate::where('name', 'like', $name . '%')->limit(3)->get();
        if($listProductRelate != null) {
            cache_it($key, $listProductRelate);
            return $listProductRelate;
        }
        return false;
    }

    public function searchName($select, $name, $order, $limit) {
        $key = 'allProductRelate_' . env('APP_SITE') . '_' . md5(serialize($select) . $name . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listProductRelate = ProductRelate::select($select)->where('status','publish')->where('name', 'like','%'.$name.'%')->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listProductRelate != null) {
            cache_it($key, $listProductRelate);
            return $listProductRelate;
        }
        return false;
    }

    public function getAll($select, $where, $order, $limit) {
        $key = 'allProductRelate_' . env('APP_SITE') . '_' . md5(serialize($select) . $where . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        if($where) $listProductRelate = ProductRelate::select($select)->whereRaw($where)->orderBy($order['col'], $order['mode'])->paginate($limit);
        else $listProductRelate = ProductRelate::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listProductRelate != null) {
            cache_it($key, $listProductRelate);
            return $listProductRelate;
        }
        return false;
    }

    public function insert($data, $tags) {
        $result = ProductRelate::create($data);
        if($result !== false) {
            if($tags) {
                foreach($tags as $tag) {
                    $result->tag($tag);
                }
            }
            return $result;
        }
        return false;
    }

    public function update($id, $data, $tags) {
        $ProductRelate = ProductRelate::find($id);
        if($ProductRelate != null) {
            foreach($data as $item => $value) {
                $ProductRelate->$item = $value;
            }
            if($ProductRelate->save()) {
                $ProductRelate->untag();
                if($tags) {
                    foreach($tags as $tag) {
                        $ProductRelate->tag($tag);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $ProductRelate = ProductRelate::find($id);
        if($ProductRelate != null) {
            if($ProductRelate->delete()) {
                return true;
            }
        }
        return false;
    }

    public function publish($id) {
        $ProductRelate = ProductRelate::find($id);
        if($ProductRelate != null) {
            if($ProductRelate->status == 'publish') {
                $ProductRelate->status = 'draf';
                $ProductRelate->save();
                return 1;
            } else {
                $ProductRelate->status = 'publish';
                $ProductRelate->save();
                return 2;
            }
        }
        return false;
    }
}