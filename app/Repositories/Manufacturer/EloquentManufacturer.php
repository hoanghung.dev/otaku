<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:13
 */
namespace App\Repositories\Manufacturer;

use App\Models\Manufacturer;

class EloquentManufacturer implements ManufacturerRepository {

    public function getById($id) {
        $key = 'Manufacturer_' . env('APP_SITE') . '_' . $id;
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $manufacturer = Manufacturer::find($id);
        if($manufacturer != null) {
            cache_it($key, $manufacturer);
            return $manufacturer;
        }
        return false;
    }

    public function getByCategoryId($select, $categoryId, $order, $limit) {
        $key = 'allManufacturer_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listManufacturer = Manufacturer::select($select)->where('status','publish')->where('category_id',$categoryId)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listManufacturer != null) {
            cache_it($key, $listManufacturer);
            return $listManufacturer;
        }
        return false;
    }

    public function getAll($select, $order, $limit) {
        $key = 'allManufacturer_' . env('APP_SITE') . '_' . md5(serialize($select) . $order['col'] . $order['mode'] . $limit);
        $cache_value = get_from_cache($key);
        if($cache_value) {
            return $cache_value;
        }
        $listManufacturer = Manufacturer::select($select)->orderBy($order['col'], $order['mode'])->paginate($limit);
        if($listManufacturer != null) {
            cache_it($key, $listManufacturer);
            return $listManufacturer;
        }
        return false;
    }

    public function insert($data) {
        $result = Manufacturer::create($data);
        if($result !== false) {
            return $result;
        }
        return false;
    }

    public function update($id, $data) {
        $manufacturer = Manufacturer::find($id);
        if($manufacturer != null) {
            foreach($data as $item => $value) {
                $manufacturer->$item = $value;
            }
            if($manufacturer->save()) {
                return true;
            }
        }
        return false;
    }

    public function delete($id) {
        $manufacturer = Manufacturer::find($id);
        if($manufacturer != null) {
            if($manufacturer->delete()) {
                return true;
            }
        }
        return false;
    }
}