<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 15/05/2016
 * Time: 22:12
 */
namespace App\Repositories\Manufacturer;

interface ManufacturerRepository {

    public function getById($id);

    public function getByCategoryId($select, $categoryId, $order, $limit);

    public function getAll($select, $order, $limit);

    public function insert($data);

    public function update($id, $data);

    public function delete($id);
}