<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
session_start();

Route::group(['prefix' => 'api/v1/'], function() {
    
    Route::post('user/register', 'Api\UserController@postRegister');
    Route::post('user/login', 'Api\UserController@postLogin');
    Route::get('user/detail', 'Api\UserController@getDetail');

    Route::post('category/create', 'Api\CategoryController@postCreate');
    Route::post('category/edit', 'Api\CategoryController@postEdit');
    Route::delete('category/delete', 'Api\CategoryController@deleteDelete');
    Route::get('category/search', 'Api\CategoryController@getSearch');
    Route::get('category/detail', 'Api\CategoryController@getDetail');

    Route::post('house/create', 'Api\HouseController@postCreate');
    Route::post('house/edit', 'Api\HouseController@postEdit');
    Route::delete('house/delete', 'Api\HouseController@deleteDelete');
    Route::get('house/search', 'Api\HouseController@getSearch');
    Route::get('house/detail', 'Api\HouseController@getDetail');
    Route::post('house/block', 'Api\HouseController@postBlock');

    Route::post('favorite/add-house', 'Api\HouseFavoriteController@postCreate');
    Route::post('favorite/edit-house', 'Api\HouseFavoriteController@postEdit');
    Route::delete('favorite/delete-house', 'Api\HouseFavoriteController@deleteDelete');
    Route::get('favorite/house-search', 'Api\HouseFavoriteController@getSearch');

    Route::post('favorite/add-location', 'Api\LocationFavoriteController@postCreate');
    Route::post('favorite/edit-location', 'Api\LocationFavoriteController@postEdit');
    Route::delete('favorite/delete-location', 'Api\LocationFavoriteController@deleteDelete');
    Route::get('favorite/location-search', 'Api\LocationFavoriteController@getSearch');

    Route::post('fllow/create', 'Api\FllowController@postCreate');
    Route::post('fllow/edit', 'Api\FllowController@postEdit');
    Route::delete('fllow/delete', 'Api\FllowController@deleteDelete');
    Route::get('fllow/search', 'Api\FllowController@getSearch');
    Route::get('fllow/detail', 'Api\FllowController@getDetail');

    Route::get('service/search', 'Api\ServiceController@getSearch');
    Route::get('service/detail', 'Api\ServiceController@getDetail');

    Route::get('notification/search', 'Api\NotificationController@getSearch');
    Route::get('notification/detail', 'Api\NotificationController@getDetail');

    Route::post('report/create', 'Api\ReportController@postCreate');
    Route::post('report/edit', 'Api\ReportController@postEdit');
    Route::delete('report/delete', 'Api\ReportController@deleteDelete');
    Route::get('report/search', 'Api\ReportController@getSearch');
});

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function() {

    Route::get('/', 'Backend\DashBoardController@getIndex');

    Route::post('autocomplete', 'Backend\DashBoardController@postAutocomplete');

    Route::post('delete-image', 'Backend\DashBoardController@deleteImage')->name('deleteImage');

    Route::controller('module', 'Backend\ModuleController', [

        'getIndex'      => 'module',
        'getCreate'     => 'module.getCreate',
        'postCreate'    => 'module.postCreate',
        'postDelete'    => 'module.postDelete',
        'getEdit'       => 'module.getEdit',
        'postEdit'      => 'module.postEdit',
        'postPublish'   => 'module.postPublish'
    ]);

    Route::controller('module2', 'Backend\Module2Controller', [

        'getIndex'      => 'module2',
        'getCreate'     => 'module2.getCreate',
        'postCreate'    => 'module2.postCreate',
        'postDelete'    => 'module2.postDelete',
        'getEdit'       => 'module2.getEdit',
        'postEdit'      => 'module2.postEdit',
        'postPublish'   => 'module2.postPublish'
    ]);


    Route::controller('housetype', 'Backend\HousetypeController', [

        'getIndex'      => 'housetype',
        'getCreate'     => 'housetype.getCreate',
        'postCreate'    => 'housetype.postCreate',
        'postDelete'    => 'housetype.postDelete',
        'getEdit'       => 'housetype.getEdit',
        'postEdit'      => 'housetype.postEdit',
        'postPublish'   => 'housetype.postPublish'
    ]);

    Route::controller('house', 'Backend\HouseController', [

        'getIndex'      => 'house',
        'getCreate'     => 'house.getCreate',
        'postCreate'    => 'house.postCreate',
        'postDelete'    => 'house.postDelete',
        'getEdit'       => 'house.getEdit',
        'postEdit'      => 'house.postEdit',
        'postPublish'   => 'house.postPublish',
        'postVip'       => 'house.postVip',
    ]);

    Route::controller('save-house', 'Backend\SavehouseController', [

        'getIndex'      => 'savehouse',
        'getCreate'     => 'savehouse.getCreate',
        'postCreate'    => 'savehouse.postCreate',
        'postDelete'    => 'savehouse.postDelete',
        'getEdit'       => 'savehouse.getEdit',
        'postEdit'      => 'savehouse.postEdit',
        'postPublish'   => 'savehouse.postPublish',
        'postVip'       => 'savehouse.postVip',
    ]);

    Route::controller('save-location', 'Backend\SavelocationController', [

        'getIndex'      => 'savelocation',
        'getCreate'     => 'savelocation.getCreate',
        'postCreate'    => 'savelocation.postCreate',
        'postDelete'    => 'savelocation.postDelete',
        'getEdit'       => 'savelocation.getEdit',
        'postEdit'      => 'savelocation.postEdit',
        'postPublish'   => 'savelocation.postPublish',
        'postVip'       => 'savelocation.postVip',
    ]);

    Route::controller('notification', 'Backend\NotificationController', [

        'getIndex'      => 'notification',
        'getCreate'     => 'notification.getCreate',
        'postCreate'    => 'notification.postCreate',
        'postDelete'    => 'notification.postDelete',
        'getEdit'       => 'notification.getEdit',
        'postEdit'      => 'notification.postEdit',
        'postPublish'   => 'notification.postPublish',
        'postVip'       => 'notification.postVip',
    ]);

    Route::controller('fllow', 'Backend\FllowController', [

        'getIndex'      => 'fllow',
        'getCreate'     => 'fllow.getCreate',
        'postCreate'    => 'fllow.postCreate',
        'postDelete'    => 'fllow.postDelete',
        'getEdit'       => 'fllow.getEdit',
        'postEdit'      => 'fllow.postEdit',
        'postPublish'   => 'fllow.postPublish',
        'postVip'       => 'fllow.postVip',
    ]);

    Route::controller('service', 'Backend\ServiceController', [

        'getIndex'      => 'service',
        'getCreate'     => 'service.getCreate',
        'postCreate'    => 'service.postCreate',
        'postDelete'    => 'service.postDelete',
        'getEdit'       => 'service.getEdit',
        'postEdit'      => 'service.postEdit',
        'postPublish'   => 'service.postPublish',
        'postVip'       => 'service.postVip',
    ]);

    Route::controller('report', 'Backend\ReportController', [

        'getIndex'      => 'report',
        'getCreate'     => 'report.getCreate',
        'postCreate'    => 'report.postCreate',
        'postDelete'    => 'report.postDelete',
        'getEdit'       => 'report.getEdit',
        'postEdit'      => 'report.postEdit',
        'postPublish'   => 'report.postPublish',
    ]);

    Route::controller('category', 'Backend\CategoryController', [

        'getIndex'      => 'category',
        'getCreate'     => 'category.getCreate',
        'postCreate'    => 'category.postCreate',
        'postDelete'    => 'category.postDelete',
        'getEdit'       => 'category.getEdit',
        'postEdit'      => 'category.postEdit'
    ]);

    Route::controller('post', 'Backend\PostController', [

        'getIndex'      => 'post',
        'getCreate'     => 'post.getCreate',
        'postCreate'    => 'post.postCreate',
        'postDelete'    => 'post.postDelete',
        'getEdit'       => 'post.getEdit',
        'postEdit'      => 'post.postEdit'
    ]);

    Route::controller('product-category', 'Backend\ProductCategoryController', [

        'getIndex'      => 'productCategory',
        'getCreate'     => 'productCategory.getCreate',
        'postCreate'    => 'productCategory.postCreate',
        'postDelete'    => 'productCategory.postDelete',
        'getEdit'       => 'productCategory.getEdit',
        'postEdit'      => 'productCategory.postEdit'
    ]);

    Route::controller('product', 'Backend\ProductController', [

        'getIndex'      => 'product',
        'getCreate'     => 'product.getCreate',
        'postCreate'     => 'product.postCreate',
        'postDelete'    => 'product.postDelete',
        'getEdit'       => 'product.getEdit',
        'postEdit'      => 'product.postEdit',
        'postPublish'   => 'product.postPublish'
    ]);

    Route::get('product-relate/create', 'Backend\ProductRelateController@getCreate');
    Route::post('product-relate/create', 'Backend\ProductRelateController@postCreate');
    Route::post('product-relate/delete', 'Backend\ProductRelateController@postDelete');
    Route::get('product-relate/{id}', 'Backend\ProductRelateController@getEdit');
    Route::post('product-relate/{id}', 'Backend\ProductRelateController@postEdit');
    Route::controller('product-relate', 'Backend\ProductRelateController', [

        'getIndex'      => 'productRelate',
        'postDelete'    => 'productRelate.postDelete',
    ]);

    Route::controller('tag', 'Backend\TagController', [

        'getIndex'      => 'tag',
        'getCreate'     => 'tag.getCreate',
        'postCreate'    => 'tag.postCreate',
        'postDelete'    => 'tag.postDelete',
        'getEdit'       => 'tag.getEdit',
        'postEdit'      => 'tag.postEdit'
    ]);

    Route::controller('sale', 'Backend\SaleController', [

        'getIndex'      => 'sale',
        'postCreate'    => 'sale.postCreate',
        'postDelete'    => 'sale.postDelete',
        'getEdit'       => 'sale.getEdit',
        'postEdit'      => 'sale.postEdit',
    ]);

    Route::controller('madein', 'Backend\MadeinController', [

        'getIndex'      => 'madein',
        'postCreate'    => 'madein.postCreate',
        'postDelete'    => 'madein.postDelete',
        'getEdit'       => 'madein.getEdit',
        'postEdit'      => 'madein.postEdit'
    ]);

    Route::controller('manufacturer', 'Backend\ManufacturerController', [

        'getIndex'      => 'manufacturer',
        'postCreate'    => 'manufacturer.postCreate',
        'postDelete'    => 'manufacturer.postDelete',
        'getEdit'       => 'manufacturer.getEdit',
        'postEdit'      => 'manufacturer.postEdit'
    ]);

    Route::controller('order', 'Backend\OrderController', [

        'getIndex'      => 'order',
        'postCreate'    => 'order.postCreate',
        'postDelete'    => 'order.postDelete',
        'getEdit'       => 'order.getEdit',
        'postEdit'      => 'order.postEdit',
        'postFindBill'  => 'order.postFindBill'
    ]);

    Route::get('user/create', 'Backend\UserController@getCreate');
    Route::post('user/create', 'Backend\UserController@postCreate')->name('user.postCreate');
    Route::post('user/delete', 'Backend\UserController@postDelete')->name('user.postDelete');
    Route::get('user/{id}', 'Backend\UserController@getEdit');
    Route::post('user/{id}', 'Backend\UserController@postEdit');
    Route::controller('user', 'Backend\UserController', [
        'getIndex'      => 'user',
    ]);

    Route::controller('slider', 'Backend\SliderController', [

        'getIndex'      => 'slider',
        'postCreate'    => 'slider.postCreate',
        'postDelete'    => 'slider.postDelete',
        'getEdit'       => 'slider.getEdit',
        'postEdit'      => 'slider.postEdit'
    ]);

    Route::controller('support', 'Backend\SupportController', [

        'getIndex'      => 'support',
        'postCreate'    => 'support.postCreate',
        'postDelete'    => 'support.postDelete',
        'getEdit'       => 'support.getEdit',
        'postEdit'      => 'support.postEdit'
    ]);

    Route::controller('export-import', 'Backend\ExcelController', [

        'getIndex'      => 'excel',
        'postCreate'    => 'excel.postCreate',
        'postDelete'    => 'excel.postDelete',
        'getEdit'       => 'excel.getEdit',
        'postEdit'      => 'excel.postEdit'
    ]);

    Route::get('home-setting', 'Backend\SettingController@getHomeSetting')->name('setting.home');

    Route::post('home-setting', 'Backend\SettingController@postHomeSetting');

    Route::controller('setting', 'Backend\SettingController', [

        'getIndex'      => 'setting',
        'postCreate'    => 'setting.postCreate',
        'postDelete'    => 'setting.postDelete',
        'getEdit'       => 'setting.getEdit',
        'postEdit'      => 'setting.postEdit',
    ]);
});
Route::auth();

Route::get('dang-tin', 'Frontend\HouseController@getDangtin');
Route::get('lien-he', 'Frontend\HomeController@getLienHe');
Route::post('contact', 'Frontend\HomeController@postContact');

Route::get('/{categorySlug}/{itemSlug}', 'Frontend\ProductController@getIndex');
Route::controller('/{slug}', 'Frontend\CategoryController');
Route::controller('/', 'Frontend\HomeController');