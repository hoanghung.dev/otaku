<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Bill\BillRepository;
use App\Repositories\Order\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends Controller {

    public $orderOrder = ['col'=>'created_at', 'mode'=>'desc'];
    public $orderLimit = 10;

    public $billOrder = ['col'=>'created_at', 'mode'=>'desc'];
    public $billLimit = 100;

    protected $orderModel;
    protected $billModel;

    public function __construct(OrderRepository $order, BillRepository $bill)
    {
        $this->orderModel = $order;
        $this->billModel = $bill;
    }

    public function getIndex(Request $request) {
        if (!\Auth::user()->can('view.order')) abort(553);
        
        $select = ['id', 'name', 'address', 'date', 'receipt_method', 'status', 'total_price', 'tel', 'created_at'];
        $where = '';
        if($request->get('search_date') != 0) $where .= " created_at >= '" . date('Y-m-1', strtotime($request->get('search_date'))) .
            "' AND created_at <= '" . date('Y-m-31', strtotime($request->get('search_date'))) . "'";
        if($request->get('search_status') != '')  $where .= "status = '" . $request->get('search_status') . "'";
        if($request->get('keyword') != '') $where .= " name Like '%".$request->get('keyword')."%'";
        $listBill = $this->billModel->getAll($select ,$where , $this->orderOrder, $this->orderLimit);
        $data['listBill'] = $listBill;

        return view('backend.childs.order.list')->with($data);
    }

    public function postDelete(Request $request) {
        if (!\Auth::user()->can('delete.order')) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach($list_id as $id) {
                if($this->billModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted') , 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error , 'list_id' => $deleted]);
        } catch(\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }


    public function postFindBill(Request $request) {
        try {
            $id = $request->get('id');
            $data['bill'] = $this->billModel->getById($id);
            $select = ['price', 'product_id', 'value'];
            $data['listOrder'] = $this->orderModel->getByBillId($select, $data['bill']->id, $this->billOrder, 100);
            if($data['bill']) {
                return view('backend.childs.order.ajaxGetBill')->with($data);
            } else {
                return response()->json(['status' => 'error' , 'msg' => trans('form.edit_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }
}