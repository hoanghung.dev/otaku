<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\CategoryManufacturer\CategoryManufacturerRepository;
use App\Repositories\Manufacturer\ManufacturerRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public $categoryLimit = 100;
    public $categoryOrder = ['col' => 'order_no', 'mode' => 'asc'];

    protected $categoryModel;
    protected $categoryManufacturerModel;
    protected $manufacturerModel;

    public function __construct(CategoryRepository $category, CategoryManufacturerRepository $categoryManufacturer, ManufacturerRepository $manufacturer)
    {
        $this->categoryModel = $category;
        $this->categoryManufacturerModel = $categoryManufacturer;
        $this->manufacturerModel = $manufacturer;
    }

    public function getIndex()
    {
        if (!\Auth::user()->can('view.category')) abort(553);
        
        $select = ['id', 'order_no', 'name', 'parent_id'];
        $where = 'type = 2 AND parent_id = 0';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, 100);
        $data['listCategory'] = $listCategory;
        return view('backend.childs.category.list')->with($data);
    }

    public function getCreate()
    {
        if (!\Auth::user()->can('create.category')) abort(553);
        
        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 1';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        return view('backend.childs.category.create')->with($data);
    }

    public function postCreate(Request $request)
    {
        $data = $request->except('_token');
        if ($request->hasFile('image')) {
            $data['image'] = saveImage($request->file('image'), 'category');
        }

        $data['user_id'] = \Auth::user()->id;
        $data['slug'] = renderSlug(false, $data['slug'], 'category');
        $data['icon'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('icon') );
        $data['type'] = 2;

        if ($this->categoryModel->insert($data)) {
            \Session::flash('success', trans('form.created'));
        } else {
            \Session::flash('error', trans('form.create_error'));
        }
        return redirect()->route('category.getCreate');
    }

    public function getEdit($id)
    {
        if (!\Auth::user()->can('edit.category')) abort(553);
        
        $category = $this->categoryModel->getById($id);
        if (!$category) abort(404);

        $data['category'] = $category;

        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 2';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        return view('backend.childs.category.edit')->with($data);
    }

    public function postEdit(Request $request, $id)
    {
        $data = $request->except('_token');
        if ($request->hasFile('image')) {
            $data['image'] = saveImage($request->file('image'), 'category');
        }
        $data['slug'] = renderSlug($id, $data['slug'], 'category');
        $data['icon'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('icon') );

        if ($this->categoryModel->update($id, $data)) {
            \Session::flash('success', trans('form.edited'));
        } else {
            \Session::flash('success', trans('form.edit_error'));
        }
        return redirect()->route('category.getEdit', ['id'=>$id]);
    }

    public function postDelete(Request $request)
    {
        if (!\Auth::user()->can('delete.category')) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach ($list_id as $id) {
                if ($this->categoryModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted'), 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error, 'list_id' => $deleted]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }
}