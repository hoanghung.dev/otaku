<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FllowController extends Controller
{

    public $itemOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $itemLimit = 20;

    protected $itemModel;
    protected $module = [
        'name'      => 'fllow',
        'primaryKey'=> 'id',
        'modal'     => '\App\Models\Fllow',
        'form'      => [
            'list'  => [
                ['name'=>'user_id', 'label'=> 'User ID', 'type'=> 'name'],
                ['name'=>'min_price', 'label'=> 'min_price', 'type'=> 'text'],
                ['name'=>'max_price', 'label'=> 'max_price', 'type'=> 'text'],
                ['name'=>'min_acreage', 'label'=> 'min_acreage', 'type'=> 'text'],
                ['name'=>'max_acreage', 'label'=> 'max_acreage', 'type'=> 'text'],
                ['name'=>'created_at', 'label'=> 'Tạo lúc', 'type'=> 'created_at']
            ],
            'left'  => [
                ['name'=>'user_id', 'label'=>'user_id', 'type'=>'text', 'col'=>12, 'inner'=>'required'],
                ['name'=>'address', 'label'=>'address', 'type'=>'text', 'col'=>12],
                ['name'=>'category_id', 'label'=>'category_id', 'type'=>'text', 'col'=>12],
                ['name'=>'lat', 'label'=>'lat', 'type'=>'text', 'col'=>6],
                ['name'=>'long', 'label'=>'long', 'type'=>'text', 'col'=>6],
                ['name'=>'rad', 'label'=>'rad', 'type'=>'text', 'col'=>6],
                ['name'=>'type', 'label'=>'type', 'type'=>'text', 'col'=>6],
                ['name'=>'min_acreage', 'label'=>'min_acreage', 'type'=>'text', 'col'=>6],
                ['name'=>'max_acreage', 'label'=>'max_acreage', 'type'=>'text', 'col'=>6],
                ['name'=>'min_price', 'label'=>'min_price', 'type'=>'text', 'col'=>6],
                ['name'=>'max_price', 'label'=>'max_price', 'type'=>'text', 'col'=>6],
            ],
            'filter' => ['date', 'category']
        ]
    ];

    public function __construct()
    {
        $this->itemModel = new $this->module['modal'];
    }

    public function getIndex(Request $request)
    {
//        if (!\Auth::user()->can('view.' . $this->module['name'])) abort(553);
        #
        $where = $this->filter($request);
        #
        $listItem = $this->itemModel->whereRaw($where)->orderBy($this->module['primaryKey'], 'desc')->paginate($this->itemLimit);
        $data['listItem'] = $listItem;
        #
        $data['filter'] = $request->all();
        $data['module'] = $this->module;
        #
        return view('backend.childs.'.$this->module['name'].'.list')->with($data);
    }

    public function filter($request) {
        $where = '1=1 ';
        #
        if($request->has('date_start')) $where .= "AND created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('date_start'))) . "'";
        if($request->has('date_end')) $where .= "AND created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('date_end'))) . "'";
        #
        if ($request->get('category') != 0) $where .= 'AND category_id = ' . $request->get('category');
        if ($request->get('status') != '') $where .= "AND status = " . $request->get('status');
        if ($request->get('keyword') != '') $where .= "AND name LIKE '%" . $request->get('keyword') . "%'";
        return $where;
    }

    public function getCreate()
    {
//        if (!\Auth::user()->can('create.' . $this->module['name'])) abort(553);

        $data['module'] = $this->module;

        return view('backend.childs.'.$this->module['name'].'.create')->with($data);
    }

    public function postCreate(Request $request)
    {
        $data = $request->except('_token');
        $data['user_id'] = \Auth::user()->id;

        if ($request->hasFile('image')) {
            if (!file_exists(base_path() . '/public/filemanager/userfiles/'.$this->module['name'])) {
                mkdir(base_path() . '/public/filemanager/userfiles/'.$this->module['name'], 0755, true);
                mkdir(base_path() . '/public/filemanager/userfiles/_thumbs/'.$this->module['name'], 0755, true);
            }
            $data['image'] = saveAndResizeImage($request->file('image'), $this->module['name'], env('IMAGE_THUMB_WIDTH'), 'auto');
        }
        #
        $data['slug'] = renderSlug(false, $data['slug'], $this->module['name']);
        $data['status'] = $request->has('status') ? 1 : 0;
        #
        if ($this->itemModel->create($data)) {
            \Session::flash('success', trans('form.created'));
        } else {
            \Session::flash('error', trans('form.create_error'));
        }
        return redirect()->route( $this->module['name'].'.getCreate');
    }

    public function getEdit($id)
    {
//        if (!\Auth::user()->can('edit.'.$this->module['name'])) abort(553);
        
        $item = $this->itemModel->find($id);
        if (!$item) abort(404);
        #
        $data['item'] = $item;
        $data['module'] = $this->module;
        #
        return view('backend.childs.'.$this->module['name'].'.edit')->with($data);
    }

    public function postEdit(Request $request, $id)
    {
        $data = $request->except('_token');
        
        $item = $this->itemModel->find($id);
        if(!is_object($item)) abort(404);

        if ($request->hasFile('image')) {
            if (!file_exists(base_path() . '/public/filemanager/userfiles/'.$this->module['name'])) {
                mkdir(base_path() . '/public/filemanager/userfiles/'.$this->module['name'], 0755, true);
                mkdir(base_path() . '/public/filemanager/userfiles/_thumbs/'.$this->module['name'], 0755, true);
            }
            $data['image'] = saveAndResizeImage($request->file('image'), $this->module['name'], env('IMAGE_THUMB_WIDTH'), 'auto');
        }
        #
        $data['slug'] = renderSlug($id, $data['slug'], $this->module['name']);
        $data['status'] = $request->has('status') ? 1 : 0;
        #
        if ($item->update($data)) {
            \Session::flash('success', trans('form.edited'));
        } else {
            \Session::flash('success', trans('form.edit_error'));
        }
        return redirect()->route($this->module['name'].'.getEdit', ['id'=>$id]);
    }

    public function postDelete(Request $request)
    {
//        if (!\Auth::user()->can('delete.'.$this->module['name'])) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach ($list_id as $id) {
                $item = $this->itemModel->find($id);
                if(is_object($item)) {
                    if($item->delete()) {
                        $deleted[] = $id;
                    } else {
                        $error .= $id . '|';
                    }
                } else {
                    $error .= $id . '|';
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted'), 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error, 'list_id' => $deleted]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function postPublish(Request $request)
    {
        try {
            $id = $request->get('id');

            $item = $this->itemModel->find($id);
            if(is_object($item)) {
                if($item->status == 1) {
                    $item->status = 0;
                    $item->save();
                    return response()->json(['status' => 'success', 'msg' => trans('form.edited'), 'status_publish' => 0]);
                } else {
                    $item->status = 1;
                    $item->save();
                    return response()->json(['status' => 'success', 'msg' => trans('form.edited'), 'status_publish' => 1]);
                }
            }
            return response()->json(['status' => 'error', 'msg' => 'Action error']);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }
    
}