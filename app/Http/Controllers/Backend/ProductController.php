<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Madein;
use App\Models\Manufacturer;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Madein\MadeinRepository;
use App\Repositories\Manufacturer\ManufacturerRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Sale\SaleRepository;
use Illuminate\Http\Request;
use App\Models\Sale;

class ProductController extends Controller
{

    public $productOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $productLimit = 10;

    public $categoryOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $categoryLimit = 100;

    protected $productModel;
    protected $categoryModel;
    protected $saleModel;
    protected $manufacturerModel;
    protected $madeinModel;

    public function __construct(ProductRepository $product, CategoryRepository $category, SaleRepository $sale, ManufacturerRepository $manufacturerModel, MadeinRepository $madeinModel)
    {
        $this->productModel = $product;
        $this->categoryModel = $category;
        $this->saleModel = $sale;
        $this->manufacturerModel = $manufacturerModel;
        $this->madeinModel = $madeinModel;
    }

    public function getIndex(Request $request)
    {
        if (!\Auth::user()->can('view.product')) abort(553);
        
        $select = ['id', 'image', 'category_id', 'user_id', 'name', 'slug', 'status', 'created_at', 'seo_level', 'base_price', 'final_price', 'souther_price', 'code'];
        $where = '';
        if ($request->get('search_date') != 0) $where .= " created_at >= '" . date('Y-m-1', strtotime($request->get('search_date'))) .
            "' AND created_at <= '" . date('Y-m-31', strtotime($request->get('search_date'))) . "'";
        if ($request->get('search_category') != 0) $where .= 'category_id = ' . $request->get('search_category');
        if ($request->get('search_seo') != 0) $where .= 'seo_level = ' . $request->get('search_seo');
        if ($request->get('search_status') != '') $where .= "status = '" . $request->get('search_status') . "'";
        if ($request->get('keyword') != '') $where .= " name LIKE '%" . $request->get('keyword') . "%'";

        $listProduct = $this->productModel->getAll($select, $where, $this->productOrder, $this->productLimit);

        $data['listProduct'] = $listProduct;

        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 1 OR type = 3';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        $select = ['id', 'name'];
        $data['listManufacturer'] = $this->manufacturerModel->getAll($select, $this->productOrder, 100);

        $select = ['id', 'name'];
        $data['listMadein'] = $this->madeinModel->getAll($select, $this->productOrder, 100);

        $select = ['id', 'name'];
        $listSale = $this->saleModel->getAll($select, $this->productOrder, 10);
        $data['listSale'] = $listSale;

        return view('backend.childs.product.list')->with($data)->with('appends', $request->all());
    }

    public function getCreate()
    {
        if (!\Auth::user()->can('create.product')) abort(553);
        
        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 1 OR type = 3';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        $listSale = Sale::where('id', '!=', 0)->get();
        $data['listSale'] = $listSale;

        $listMadein = Madein::all();
        $data['listMadein'] = $listMadein;

        $listManufacturer = Manufacturer::all();
        $data['listManufacturer'] = $listManufacturer;

        return view('backend.childs.product.create')->with($data);
    }

    public function postCreate(Request $request)
    {
        $data = $request->except('_token', 'tags', 'category_orther_id', 'image', 'image_extra');
        $data['user_id'] = \Auth::user()->id;

        if ($request->hasFile('image')) {
            $data['image'] = saveAndResizeImage($request->file('image'), 'product', env('IMAGE_THUMB_WIDTH'), 'auto');
        }
        if ($request->hasFile('image_extra')) {
            foreach ($request->file('image_extra') as $key => $item) {
                $key += 1;
                $data['image_extra_' . $key] = saveAndResizeImage($item, 'product', env('IMAGE_MINI'), env('IMAGE_MINI'));
                resizeImage($data['image_extra_' . $key], env('IMAGE_THUMB_WIDTH'), 'auto');
            }
        }
        if ($request->hasFile('iframe')) {
            $data['iframe'] = saveImage($request->file('iframe'), 'docs');
        }

        #
        $data['slug'] = renderSlug(false, $data['slug'], 'product');
        $data['view_total'] = 0;
        $data['status'] = $request->has('status') ? 1 : 0;
        $data['tax_vat'] = $request->has('tax_vat') ? 1 : 0;
        if($request->has('video')) $data['video'] = isset(explode('?v=', $request->get('video'))[1]) ? explode('?v=', $request->get('video'))[1] : $request->get('video');
        else $data['video'] = '';
        #
        $data['category_orther_id'] = '';
        if ($request->has('category_orther_id')) {
            foreach ($request->get('category_orther_id') as $item) {
                $data['category_orther_id'] .= $item . ',';
            }
        }
        #
        $tags = explode(',', $request->get('tags'));
        if (count($tags) == 1 && $tags[0] == '') $result = $this->productModel->insert($data, false);
        else $result = $this->productModel->insert($data, $tags);
        if ($result !== false) {
            \Session::flash('success', trans('form.created'));
        } else {
            \Session::flash('error', trans('form.create_error'));
        }
        return redirect()->route('product.getCreate');
    }

    public function getEdit($id)
    {
        if (!\Auth::user()->can('edit.product')) abort(553);
        
        $product = $this->productModel->getById($id);
        if (!$product) abort(404);
        #
        $data['product'] = $product;
        $tags = array();
        foreach ($product->tags as $item) {
            $tags[] = $item->name;
        }
        #
        $data['category_orther_id'] = explode(',', $product->category_orther_id);
        #
        $tags = array();
        foreach ($product->tags as $item) {
            $tags[] = $item->name;
        }
        $data['tags'] = $tags;
        #
        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 1 OR type = 3';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;
        #
        $listSale = Sale::where('id', '!=', 0)->get();
        $data['listSale'] = $listSale;
        #
        $listMadein = Madein::all();
        $data['listMadein'] = $listMadein;
        #
        $listManufacturer = Manufacturer::all();
        $data['listManufacturer'] = $listManufacturer;
        #
        return view('backend.childs.product.edit')->with($data);
    }

    public function postEdit(Request $request, $id)
    {
        $data = $request->except('_token', 'category_orther_id', 'tags', 'image_extra');

        if ($request->hasFile('image')) {
            $data['image'] = saveAndResizeImage($request->file('image'), 'product', env('IMAGE_THUMB_WIDTH'), 'auto');
        }
        if ($request->hasFile('image_extra')) {
            $data['image_extra_1'] = $data['image_extra_2'] = $data['image_extra_3'] = $data['image_extra_4'] = '';
            foreach ($request->file('image_extra') as $key => $item) {
                $key += 1;
                $data['image_extra_' . $key] = saveAndResizeImage($item, 'product', env('IMAGE_MINI'), env('IMAGE_MINI'));
                resizeImage($data['image_extra_' . $key], env('IMAGE_THUMB_WIDTH'), 'auto');
            }
        }
        if ($request->hasFile('iframe')) {
            $data['iframe'] = saveImage($request->file('iframe'), 'docs');
        }

        $data['slug'] = renderSlug($id, $data['slug'], 'product');
        $data['status'] = $request->has('status') ? 1 : 0;
        $data['tax_vat'] = $request->has('tax_vat') ? 1 : 0;
        if($request->has('video')) $data['video'] = isset(explode('?v=', $request->get('video'))[1]) ? explode('?v=', $request->get('video'))[1] : $request->get('video');
        else $data['video'] = '';
        #
        $data['category_orther_id'] = '';
        if ($request->has('category_orther_id')) {
            foreach ($request->get('category_orther_id') as $item) {
                $data['category_orther_id'] .= $item . ',';
            }
        }

        $tags = explode(',', $request->get('tags'));
        if (count($tags) == 1 && $tags[0] == '') $result = $this->productModel->update($id, $data, false);
        else $result = $this->productModel->update($id, $data, $tags);

        if ($result !== false) {
            \Session::flash('success', trans('form.edited'));
        } else {
            \Session::flash('success', trans('form.edit_error'));
        }
        return redirect()->route('product.getEdit', ['id'=>$id]);
    }

    public function postDelete(Request $request)
    {
        if (!\Auth::user()->can('delete.product')) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach ($list_id as $id) {
                if ($this->productModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted'), 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error, 'list_id' => $deleted]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function postPublish(Request $request)
    {
        try {
            $id = $request->get('id');

            $status = $this->productModel->publish($id);
            if ($status != false) {
                return response()->json(['status' => 'success', 'msg' => trans('form.edited'), 'status_publish' => $status]);
            }
            return response()->json(['status' => 'error', 'msg' => 'Action error']);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }


    /*Order*/

    public function getEditOrder()
    {
        die('get order');
    }

    public function postEditOrder()
    {
        die('post order');
    }
}