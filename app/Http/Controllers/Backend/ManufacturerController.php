<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Manufacturer\ManufacturerRepository;
use App\Repositories\Madein\MadeinRepository;
use Illuminate\Http\Request;

class ManufacturerController extends Controller {

    public $manufacturer_limit = 100;
    public $manufacturer_order = ['col'=>'created_at', 'mode'=>'desc'];

    protected $manufacturerModel;
    protected $madeinModel;

    public function __construct(ManufacturerRepository $manufacturer, MadeinRepository $madeinModel)
    {
        $this->manufacturerModel = $manufacturer;
        $this->madeinModel = $madeinModel;
    }

    public function getIndex() {
        if (!\Auth::user()->can('view.manufacturer')) abort(553);

        $select = ['id', 'intro', 'name', 'slug', 'madein_id', 'size', 'guarantee'];
        $listManufacturer = $this->manufacturerModel->getAll($select,$this->manufacturer_order, $this->manufacturer_limit);
        $data['listManufacturer'] = $listManufacturer;

        $select = ['id', 'name' ];
        $data['listMadein'] = $this->madeinModel->getAll($select, $this->manufacturer_order, 100);
        
        return view('backend.childs.manufacturer.list')->with($data);
    }

    public function postCreate(Request $request) {
        try {
            $data = $request->except('_token', 'id');
            $data['image'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('image') );
            $data['user_id'] = 1;
            $data['status'] = 'publish';
            $data['slug'] = renderSlug(false, $data['slug'], 'manufacturer');

            $result = $this->manufacturerModel->insert($data);
            if($result !== false) {
                return response()->json(['status' => 'success' , 'msg' => trans('form.created') , 'id' => $result->id, 'madein' => $result->madein->name]);
            } else {
                return response()->json(['status' => 'error' , 'msg' => trans('form.create_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

    public function getEdit(Request $request) {
        if (!\Auth::user()->can('edit.manufacturer')) abort(553);
        
        try {
            $id = $request->get('id');
            $manufacturer = $this->manufacturerModel->getById($id);
            if($manufacturer) {
                return response()->json(['status' => 'success' , 'data' => $manufacturer ]);
            } else {
                return response()->json(['status' => 'error' , 'msg' => trans('form.edit_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

    public function postEdit(Request $request) {
        try {
            $id = $request->get('id');
            $data = $request->except('_token', 'id');
            $data['image'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('image') );
            $data['slug'] = renderSlug($id, $data['slug'], 'manufacturer');
            
            if($id != '') {
                if($this->manufacturerModel->update($id, $data)) {
                    $result = $this->manufacturerModel->getById($id);
                    return response()->json(['status' => 'success' , 'msg' => trans('form.edited'), 'slug' => $result->slug, 'madein' => $result->madein->name]);
                }
            }
            return response()->json(['status' => 'error' , 'msg' => trans('form.create_error')]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

    public function postDelete(Request $request) {
        if (!\Auth::user()->can('delete.manufacturer')) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach($list_id as $id) {
                if($this->manufacturerModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted') , 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error , 'list_id' => $deleted]);
        } catch(\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }
}