<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ProductRelate;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\ProductRelate\ProductRelateRepository;
use Illuminate\Http\Request;

class productRelateController extends Controller {

    public $productOrder = ['col'=>'created_at', 'mode'=>'desc'];
    public $productLimit = 10;

    public $categoryOrder = ['col'=>'created_at', 'mode'=>'desc'];
    public $categoryLimit = 100;
    
    protected $categoryModel;
    protected $productRelateModel;
    protected $productModel;

    public function __construct(ProductRepository $product, ProductRelateRepository $productRelateModel, CategoryRepository $category)
    {
        $this->productRelateModel = $productRelateModel;
        $this->categoryModel = $category;
        $this->productModel = $product;
    }

    public function getIndex(Request $request) {

        $listProduct = ProductRelate::paginate($this->productLimit);
        $data['listProduct'] = $listProduct;

        return view('backend.childs.productRelate.index')->with($data);
    }

    public function getCreate() {
        return view('backend.childs.productRelate.create');
    }

    public function postCreate(Request $request) {
        $productName = $request->get('name');
        $included = $request->get('included');
        $selling = $request->get('selling');
        $sale = $request->get('sale');

        $product = $this->productModel->getByName($productName);
        $data['product_id'] = $product->id;
        $data['category_included'] = $data['category_selling'] = $data['category_sale'] = '|';
        if($included != '') {
            foreach (explode(',', $included) as $item) {
                $category = $this->categoryModel->searchByName($item);
                if($category != null)
                $data['category_included'] .= $category->id . '|' ;
            }
        }

        if($selling != '') {
            foreach (explode(',', $selling) as $item) {
                $category = $this->categoryModel->searchByName($item);
                if($category != null)
                $data['category_selling'] .= $category->id . '|' ;
            }
        }

        if($sale != '') {
            foreach (explode(',', $sale) as $item) {
                $category = $this->categoryModel->searchByName($item);
                if($category != null)
                $data['category_sale'] .= $category->id . '|' ;
            }
        }

        if(ProductRelate::create($data)) {
            \Session::flash('success', 'Created!');
        } else {
            \Session::flash('success', 'Created!');
        }
        return redirect('dashboard/product-relate');
    }

    public function getEdit($id) {
        $productRelate = ProductRelate::find($id);
        if($productRelate == null) abort(404);
        return view('backend.childs.productRelate.edit')->with('productRelate', $productRelate);
    }

    public function postEdit($id, Request $request) {
        die('ok');
    }

    public function postDelete(Request $request) {
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach($list_id as $id) {
                if($this->productRelateModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted') , 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error , 'list_id' => $deleted]);
        } catch(\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }
}