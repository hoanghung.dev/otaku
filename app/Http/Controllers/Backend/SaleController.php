<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Sale\SaleRepository;
use App\Repositories\Manufacturer\ManufacturerRepository;
use Illuminate\Http\Request;

class SaleController extends Controller
{

    public $saleOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $saleLimit = 100;

    public $categoryOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $categoryLimit = 100;

    protected $saleModel;
    protected $categoryModel;
    protected $manufacturerModel;

    public function __construct(CategoryRepository $category, SaleRepository $sale, ManufacturerRepository $manufacturerModel)
    {
        $this->saleModel = $sale;
        $this->categoryModel = $category;
        $this->manufacturerModel = $manufacturerModel;
    }

    public function getIndex()
    {
        if (!\Auth::user()->can('view.sale')) abort(553);
        
        $select = ['id', 'name', 'type', 'intro', 'value', 'created_at'];
        $listSale = $this->saleModel->getAll($select, $this->saleOrder, $this->saleLimit);
        $data['listSale'] = $listSale;

        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 1';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->saleOrder, $this->categoryLimit);
        $data['listCategory'] = $listCategory;

        $select = ['id', 'name'];
        $data['listManufacturer'] = $this->manufacturerModel->getAll($select, $this->saleOrder, 100);

        return view('backend.childs.sale.list')->with($data);
    }

    public function postCreate(Request $request)
    {
        if (!\Auth::user()->can('create.sale')) abort(553);
        
        try {
            $data = $request->except('_token', 'id', 'category_id', 'manufacturer_id');
            $data['category_id'] = '';
            $category_id = str_replace('0,', '', $request->get('category_id'));
            if (count($category_id) > 0 && $category_id != '') {
                foreach ($category_id as $item) {
                    if ($item == $category_id[0]) {
                        $data['category_id'] .= $item;
                    } else {
                        $data['category_id'] .= ',' . $item;
                    }
                }
            }

            $data['manufacturer_id'] = '';
            $manufacturer_id = str_replace('0,', '', $request->get('manufacturer_id'));
            if (count($manufacturer_id) > 0 && $manufacturer_id != '') {
                foreach ($manufacturer_id as $item) {
                    if ($item == $manufacturer_id[0]) {
                        $data['manufacturer_id'] .= $item;
                    } else {
                        $data['manufacturer_id'] .= ',' . $item;
                    }
                }
            }

            $result = $this->saleModel->insert($data);
            if ($result !== false) {
                // UP date sale_id for list product
                $listProduct = null;

                if ($category_id != '' && $manufacturer_id != '') {
                    $listProduct = Product::whereIn('category_id', $category_id)->whereIn('manufacturer_id', $manufacturer_id)->get();
                } elseif ($category_id != '' && $manufacturer_id == '') {
                    $listProduct = Product::whereIn('category_id', $category_id)->get();
                } elseif ($category_id == '' && $manufacturer_id != '') {
                    $listProduct = Product::whereIn('manufacturer_id', $manufacturer_id)->get();
                }
                if (!empty($listProduct)) {
                    foreach ($listProduct as $product) {
                        $product->sale_id = $result->id;
                        $product->save();
                    }
                }

                return response()->json(['status' => 'success', 'msg' => trans('form.created'),
                    'id' => $result->id]);
            } else {
                return response()->json(['status' => 'error', 'msg' => trans('form.create_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function getEdit(Request $request)
    {
        if (!\Auth::user()->can('edit.sale')) abort(553);
        
        try {
            $id = $request->get('id');
            $sale = $this->saleModel->getById($id);
            if ($sale) {
                return response()->json(['status' => 'success', 'data' => $sale]);
            } else {
                return response()->json(['status' => 'error', 'msg' => trans('form.edit_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function postEdit(Request $request)
    {
        try {
            $id = $request->get('id');
            $data = $request->except('_token', 'id', 'category_id', 'manufacturer_id');
            $data['category_id'] = '';
            $category_id = str_replace('0,', '', $request->get('category_id'));
            if (count($category_id) > 0 && $category_id != '') {
                foreach ($category_id as $item) {
                    if ($item == $category_id[0]) {
                        $data['category_id'] .= $item;
                    } else {
                        $data['category_id'] .= ',' . $item;
                    }
                }
            }

            $data['manufacturer_id'] = '';
            $manufacturer_id = str_replace('0,', '', $request->get('manufacturer_id'));
            if (count($manufacturer_id) > 0 && $manufacturer_id != '') {
                foreach ($manufacturer_id as $item) {
                    if ($item == $manufacturer_id[0]) {
                        $data['manufacturer_id'] .= $item;
                    } else {
                        $data['manufacturer_id'] .= ',' . $item;
                    }
                }
            }

            if ($id != '') {
                // Reset sale_id for product
                if($data['category_id'] != ''){     // Delete products.sale_id in list category_id
                    foreach(explode(',', $data['category_id']) as $cat_id) {
                        $listProduct = Product::where('category_id', $cat_id)->get();
                        foreach($listProduct as $prd) {
                            $prd->sale_id = 0;
                            $prd->save();
                        }
                    }
                }
                if($data['manufacturer_id'] != ''){     // Delete products.sale_id in list manufacturer_id
                    foreach(explode(',', $data['manufacturer_id']) as $manuf_id) {
                        $listProduct = Product::where('manufacturer_id', $manuf_id)->get();
                        foreach($listProduct as $prd) {
                            $prd->sale_id = 0;
                            $prd->save();
                        }
                    }
                }

                // Update sale
                $result = $this->saleModel->update($id, $data);
                if ($result !== false) {
                    // UP date sale_id for list product
                    $listProduct = null;

                    if ($data['category_id'][0] != '0' && $data['manufacturer_id'][0] != '0') {
                        $listProduct = Product::whereIn('category_id', $request->get('category_id'))->whereIn('manufacturer_id', $request->get('manufacturer_id'))->get();
                    } elseif ($data['category_id'][0] != '0' && $data['manufacturer_id'][0] == '0') {
                        $listProduct = Product::whereIn('category_id', $request->get('category_id'))->get();
                    } elseif ($data['category_id'][0] == '0' && $data['manufacturer_id'][0] != '0') {
                        $listProduct = Product::whereIn('manufacturer_id', $request->get('manufacturer_id'))->get();
                    }
                    if (!empty($listProduct)) {
                        foreach ($listProduct as $product) {
                            $product->sale_id = $result->id;
                            $product->save();
                        }
                    }

                    return response()->json(['status' => 'success', 'msg' => trans('form.edited')]);
                }
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.create_error')]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function postDelete(Request $request)
    {
        if (!\Auth::user()->can('delete.sale')) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach ($list_id as $id) {
                if ($this->saleModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted'), 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error, 'list_id' => $deleted]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

}