<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class ExcelController extends Controller {

    public function getIndex() {
        if (!\Auth::user()->can('view.exel')) abort(553);
        
        return view('backend.childs.excel.index');
    }

    public function postIndex(Request $request) {
        $data = $request->except('_token');
        if(isset($data['site_offline'])) $data['site_offline'] = 1;
        if(isset($data['cache'])) $data['cache'] = 1;
        if(isset($data['send_mail'])) $data['send_mail'] = 1;
        $data['logo'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/filemanager/userfiles/','',$request->get('logo') );
        $data['favicon'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/filemanager/userfiles/','',$request->get('favicon') );
        $setting = Setting::first();
        foreach ($data as $key => $value) {
            $setting->$key = $value;
        }
        $setting->save();
        \Session::flash('success', 'Edit success!');
        return redirect()->route('setting');
    }
}