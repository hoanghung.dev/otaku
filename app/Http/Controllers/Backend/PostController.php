<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public $postOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $postLimit = 10;

    public $categoryOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $categoryLimit = 100;

    protected $postModel;
    protected $categoryModel;

    public function __construct(PostRepository $post, CategoryRepository $category)
    {
        $this->postModel = $post;
        $this->categoryModel = $category;
    }

    public function getIndex(Request $request)
    {
        if (!\Auth::user()->can('view.post')) abort(553);
        
        $select = ['id', 'image', 'category_id', 'user_id', 'name', 'slug', 'created_at', 'seo_level', 'status'];
        $where = '';
        if ($request->get('search_date') != 0) $where .= " created_at >= '" . date('Y-m-1', strtotime('20-' . $request->get('search_date'))) .
            "' AND created_at <= '" . date('Y-m-31', strtotime('20-' . $request->get('search_date'))) . "'";
        if ($request->get('search_category') != 0) $where .= 'category_id = ' . $request->get('search_category');
        if ($request->get('search_seo') != 0) $where .= 'seo_level = ' . $request->get('search_seo');
        if ($request->get('search_status') != '') $where .= "status = '" . $request->get('search_status') . "'";
        if ($request->get('keyword') != '') $where .= " name Like '%" . $request->get('keyword') . "%'";
        $listPost = $this->postModel->getAll($select, $where, $this->postOrder, $this->postLimit);
        $data['listPost'] = $listPost;

        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 2 OR type = 4 OR type = 5 ';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        return view('backend.childs.post.list')->with($data)->with('appends', $request->all());
    }

    public function getCreate()
    {
        if (!\Auth::user()->can('edit.post')) abort(553);
        
        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 2 OR type = 4 OR type = 5 ';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        return view('backend.childs.post.create')->with($data);
    }

    public function postCreate(Request $request)
    {
        $data = $request->except('_token', 'tags');
        if ($request->hasFile('image')) {
            $data['image'] = saveAndResizeImage($request->file('image'), 'post', env('IMAGE_THUMB_WIDTH'), 'auto');
        }
        $data['user_id'] = \Auth::user()->id;
        $data['slug'] = renderSlug(false, $data['slug'], 'post');
        $data['status'] = $request->has('status') ? 1 : 0;

        if($request->has('video')) $data['video'] = isset(explode('?v=', $request->get('video'))[1]) ? explode('?v=', $request->get('video'))[1] : $request->get('video');
        else $data['video'] = '';

        $tags = $request->has('tags') ? explode(',', $request->get('tags')) : false;

        $result = $this->postModel->insert($data, $tags);
        if ($result !== false) {
            \Session::flash('success', trans('form.created'));
        } else {
            \Session::flash('error', trans('form.create_error'));
        }
        return redirect()->route('post.getCreate');
    }

    public function getEdit($id)
    {
        if (!\Auth::user()->can('edit.post')) abort(553);
        
        $post = $this->postModel->getById($id);
        if (!$post) abort(404);

        $data['post'] = $post;
        $tags = array();
        foreach ($post->tags as $item) {
            $tags[] = $item->name;
        }
        $data['tags'] = $tags;

        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 2 OR type = 4 OR type = 5 ';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        return view('backend.childs.post.edit')->with($data);
    }

    public function postEdit(Request $request, $id)
    {
        $data = $request->except('_token', 'tags');

        if ($request->hasFile('image')) {
            $data['image'] = saveAndResizeImage($request->file('image'), 'post', env('IMAGE_THUMB_WIDTH'), 'auto');
        }

        $data['slug'] = renderSlug($id, $data['slug'], 'post');
        $data['status'] = $request->has('status') ? 1 : 0;
        if($request->has('video')) $data['video'] = isset(explode('?v=', $request->get('video'))[1]) ? explode('?v=', $request->get('video'))[1] : $request->get('video');
        else $data['video'] = '';

        $tags = explode(',', $request->get('tags'));
        if (count($tags) == 1 && $tags[0] == '') $result = $this->postModel->update($id, $data, false);
        else $result = $this->postModel->update($id, $data, $tags);

        if ($result !== false) {
            \Session::flash('success', trans('form.edited'));
        } else {
            \Session::flash('success', trans('form.edit_error'));
        }
        return redirect()->route('post.getEdit', ['id'=>$id]);
    }

    public function postDelete(Request $request)
    {
        if (!\Auth::user()->can('delete.post')) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach ($list_id as $id) {
                if ($this->postModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted'), 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error, 'list_id' => $deleted]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }
}