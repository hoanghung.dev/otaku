<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Product;
use App\Models\Tag;
use App\Repositories\Tag\TagRepository;
use Illuminate\Http\Request;

class TagController extends Controller {

    public $tag_limit = 100;
    public $tag_order = ['col'=>'count', 'mode'=>'desc'];

    protected $tagModel;

    public function __construct(TagRepository $tag)
    {
        $this->tagModel = $tag;
    }

    public function getIndex(Request $request) {
        if (!\Auth::user()->can('view.tag')) abort(553);
        
        $listTag = Tag::orderBy('count', 'desc')->paginate(15);
        $data['listTag'] = $listTag;

        return view('backend.childs.tag.list')->with($data)->with('appends',$request->all());
    }

    public function getCreate()
    {
        if (!\Auth::user()->can('edit.post')) abort(553);

        $select = ['id', 'name', 'parent_id'];
        $where = 'type = 2 OR type = 4 OR type = 5 ';
        $listCategory = $this->categoryModel->getAll($select, $where, $this->categoryOrder, false);
        $data['listCategory'] = $listCategory;

        return view('backend.childs.post.create')->with($data);
    }

    public function postCreate(Request $request) {
        try {
            $data = $request->except('_token', 'id');
            $data['image'] = '';
            $data['user_id'] = 1;
            $data['status'] = 'publish';

            $result = $this->tagModel->insert($data);
            if($result !== false) {
                return response()->json(['status' => 'success' , 'msg' => trans('form.created') , 'id' => $result->id]);
            } else {
                return response()->json(['status' => 'error' , 'msg' => trans('form.create_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

    public function getEdit($id)
    {
        if (!\Auth::user()->can('edit.tag')) abort(553);

        $tag = Tag::find($id);
        if(!is_object($tag)) abort(404);

        $listProduct = Product::withAnyTag([$tag->name])->get();
        $data['listProduct'] = $listProduct;

        $listPost = Post::withAnyTag([$tag->name])->get();
        $data['listPost'] = $listPost;

        return view('backend.childs.tag.edit')->with($data);
    }

    public function postEdit(Request $request, $id) {
        $data = $request->except('_token');

        $tag = Tag::find($id);
        
        if(isset($data['product'])) {
            foreach ($data['product'] as $itemId) {
                $item = Product::find($itemId);
                if(is_object($item)) $item->untag($tag->name);
            }
        }
        
        if(isset($data['post'])) {
            foreach ($data['post'] as $itemId) {
                $item = Post::find($itemId);
                if(is_object($item)) $item->untag($tag->name);
            }
        }
        
        \Session::flash('success', 'Xóa tag thành công!');
        
        return redirect()->route('tag.getEdit', ['id'=>$id]);
    }

    public function postDelete(Request $request) {
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach($list_id as $id) {
                if($this->tagModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted') , 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error , 'list_id' => $deleted]);
        } catch(\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }
}