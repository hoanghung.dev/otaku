<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:15
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\Category;
use App\Models\House;
use App\Models\Post;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;

class DashBoardController extends Controller
{

    public $productOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $productLimit = 5;

    public $categoryOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $categoryLimit = 100;

    protected $productModel;
    protected $categoryModel;

    public function __construct(ProductRepository $product, CategoryRepository $category)
    {
        $this->productModel = $product;
        $this->categoryModel = $category;
    }

    public function getIndex(Request $request)
    {

        // Lay khoang thoi gian can thong ke chia ra lam 10 doan thong ke.
        $start_day = $request->has('start_day') ? strtotime($request->get('start_day')) : strtotime('- 100 day');
        $end_day = $request->has('end_day') ? strtotime($request->get('end_day')) : time();
        $hieu_so = $end_day - $start_day;
        $ratio = ($hieu_so - $hieu_so % 10) / 10;

        $data = [];
        $i = 0;
        while ($i <= 10) {
            $thisTime = $start_day + $i * $ratio;
            $product = Product::where('created_at', '>=', date('Y-m-d H:i:s', $start_day))
                ->where('created_at', '<=', date('Y-m-d H:i:s', $thisTime))->get()->count();
            #
            $order = Bill::where('created_at', '>=', date('Y-m-d H:i:s', $start_day))
                ->where('created_at', '<=', date('Y-m-d H:i:s', $thisTime))->get()->count();
            #
            $pay = Bill::where('created_at', '>=', date('Y-m-d H:i:s', $start_day))
                ->where('created_at', '<=', date('Y-m-d H:i:s', $thisTime))->pluck('total_price')->sum();
            #
            $data[] = [
                'time' => $thisTime,
                'product' => $product,
                'order' => $order,
                'pay' => $pay
            ];
            $i++;
        }

        //  lay du lieu thong ke
        $data['data'] = $data;
        $data['start_day'] = $start_day;
        $data['end_day'] = $end_day;

        // Thong ke theo ngay/thang/nam
        $data['product']['today'] = Product::where('status', 1)->where('created_at', '>', date('Y-m-d 00:00:00', time()))->get()->count();
        $data['product']['week'] = Product::where('status', 1)->where('created_at', '>', date('Y-m-d 00:00:00', strtotime('Last Monday', time())))->get()->count();
        $data['product']['month'] = Product::where('status', 1)->where('created_at', '>', date('Y-m-1 00:00:00', time()))->get()->count();
        #
        $data['order']['today'] = Bill::where('created_at', '>', date('Y-m-d 00:00:00', time()))->get()->count();
        $data['order']['week'] = Bill::where('created_at', '>', date('Y-m-d 00:00:00', strtotime('Last Monday', time())))->get()->count();
        $data['order']['month'] = Bill::where('created_at', '>', date('Y-m-1 00:00:00', time()))->get()->count();
        #
        $data['pay']['today'] = Bill::where('created_at', '>', date('Y-m-d 00:00:00', time()))->pluck('total_price')->sum();
        $data['pay']['week'] = Bill::where('created_at', '>', date('Y-m-d 00:00:00', strtotime('Last Monday', time())))->pluck('total_price')->sum();
        $data['pay']['month'] = Bill::where('created_at', '>', date('Y-m-1 00:00:00', time()))->pluck('total_price')->sum();


        $select = ['id', 'image', 'category_id', 'user_id', 'name', 'slug', 'created_at', 'final_price', 'code'];
        $listProduct = $this->productModel->getAll($select, false, $this->productOrder, $this->productLimit);
        $data['listProduct'] = $listProduct;
        $data['listHouse'] = House::orderBy('id', 'desc')->limit(5);

        return view('backend.dashboard.index')->with($data);
    }

    public function postAutocomplete(Request $request)
    {
        $name = $request->get('name');
        $type = $request->get('type');

        $results = [];
        if ($type == 'product') {
            $results = Product::where('name', 'like', $name . '%')->limit(3)->get();
        } elseif ($type == 'category') {
            $results = Category::where('name', 'like', $name . '%')->limit(3)->get();
        } elseif ($type == 'post') {
            $results = Post::where('name', 'like', $name . '%')->limit(3)->get();
        }

        $data = [];
        foreach ($results as $item) {
            $data[] = $item->name;
        }
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    /**
     * @return int
     */
    public function deleteImage(Request $request)
    {
        $data = $request->except('_token');
        switch ($data['type']) {
            case 'product':
                if ($data['name'] == 'image_extra') {
                    $product = Product::find((int)$data['id']);
                    if (is_object($product)) {
                        $product->image_extra_1 = $product->image_extra_2 = $product->image_extra_3 = $product->image_extra_4 = '';
                        $product->save();
                        return response()->json(['status' => 'success']);
                    }
                } else {
                    $product = Product::find((int)$data['id']);
                    if (is_object($product)) {
                        $product->$data['name'] = '';
                        $product->save();
                        return response()->json(['status' => 'success']);
                    }
                }
                break;
        }
    }
}