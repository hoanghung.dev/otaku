<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Support;
use Illuminate\Http\Request;

class SupportController extends Controller {

    public function getIndex() {
        if (!\Auth::user()->can('view.support')) abort(553);

        $listSupport = Support::get();
        return view('backend.childs.support.index')->with('listSupport', $listSupport);
    }

    public function postIndex(Request $request) {
        if (!\Auth::user()->can('edit.support')) abort(553);

        $listSupport = Support::get();
        foreach ($listSupport as $Support) {
            $Support->name = $request->get('name' . $Support->id);
            $Support->email = $request->get('email' . $Support->id);
            $Support->tel = $request->get('tel' . $Support->id);

            if($request->get('image' . $Support->id) == 'NULL') $Support->image = '';
            else $Support->image = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('image' . $Support->id) );

            $Support->save();
        }

        if($request->has('image')) {
            $data['name'] = $request->get('name');
            $data['email'] = $request->get('email');
            $data['tel'] = $request->get('tel');
            $data['image'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('image') );
            Support::create($data);
        }

        \Session::flash('success', 'Success!');
        return redirect()->route('support');
    }

    public function postDelete(Request $request) {
        if (!\Auth::user()->can('delete.support')) abort(553);

        $id = $request->get('id');
        $Support = Support::find((int) $id);
        if($Support != null) {
            if($Support->delete()) {
                \Session::flash('success', 'Deleted!');
            }
        }
        \Session::flash('error', 'Error!');
        return response()->json(['']);
    }
}