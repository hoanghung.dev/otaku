<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\RoleUser;
use App\Models\User;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller {

    public $userOrder = ['col'=>'created_at', 'mode'=>'desc'];
    public $userLimit = 10;

    protected $userModel;

    public function __construct(UserRepository $user)
    {
        $this->userModel = $user;
    }

    public function getIndex(Request $request) {
        if(!\Auth::user()->can('view.user')) abort(553);

        $listUser = $this->getListUser($request);
        #
        $listUser = $listUser->paginate($this->userLimit);
        $data['listUser'] = $listUser;
        #
        $countTotal = $listUser->count();
        $data['countTotal'] = $countTotal;

        return view('backend.childs.user.list')->with($data)->with('appends', $request->all());
    }

    public function getListUser($request) {
        $listUser = User::whereRaw('1 = 1 ');

        if($request->has('date_start')) $listUser = $listUser->whereRaw("created_at >= '" . date('Y-m-d H:i:s',strtotime($request->get('date_start'))) ."'");
        if($request->has('date_end')) $listUser = $listUser->whereRaw("created_at <= '" . date('Y-m-d H:i:s',strtotime($request->get('date_end'))) ."'");

        if($request->get('search_status') != '')  $listUser = $listUser->where("status", $request->get('search_status'));

        if($request->get('keyword') != '') {
            if($request->get('search_type') == 'sex') {
                if($request->get('keyword') == 'Nam') $listUser = $listUser->where('sex', 1);
                else $listUser = $listUser->where('sex', 2);
            } else {
                $listUser = $listUser->where($request->get('search_type'), 'like', '%'.$request->get('keyword').'%');
            }
        }
        return $listUser;
    }

    public function postIndex(Request $request) {

    }

    public function getCreate() {
        if(!\Auth::user()->can('create.user')) abort(553);
        return view('backend.childs.user.create');
    }

    public function postCreate(Request $request) {
        if(!\Auth::user()->can('create.user')) abort(553);

        $data = $request->except('_token', 'repassword');
        if($request->get('password') != $request->get('repassword')) {
            \Session::flash('error', 'Mật khẩu không khớp!');
            return redirect('user');
        }

        $data['avatar'] = str_replace('http://' . $_SERVER['SERVER_NAME'] . '/public/filemanager/userfiles/', '', $request->get('avatar'));
        if($request->has('status')) $data['status'] = 1;
        $data['password'] = bcrypt($data['password']);

        $user = $this->userModel->insert($data);
        if($user != false) {
            // Doi quyen cho user
            $user->revokeAllRoles();
            $user->assignRole($request->get('role_user'));

            \Session::flash('success', 'Tạo thành công!');
            return redirect('user');
        }
        \Session::flash('error', 'Có lỗi xảy ra!');
        return redirect('user');
    }

    public function getEdit($id) {
        $data['user'] = $this->userModel->getById($id);
        if(!$data['user']) abort(404);

        return view('backend.childs.user.edit')->with($data);
    }

    public function postEdit($id, Request $request) {
        if(!\Auth::user()->can('edit.user')) abort(553);

        $data = $request->except('_token', 'password', 'repassword', 'pay_last', 'credits', 'role_user', 'status');
        $data['avatar'] = str_replace('http://' . $_SERVER['SERVER_NAME'] . '/public/filemanager/userfiles/', '', $request->get('avatar'));

        if($request->get('password') != $request->get('repassword')) {
            \Session::flash('error', 'Mật khẩu không khớp!');
            return redirect('user');
        }

        if($request->has('status')) $data['status'] = 'active';
        elseif (!$request->has('status') && User::find($id)->status == 'active') $data['status'] = 'deactive';

        if($request->has('password') && $request->get('password') != '') $data['password'] = bcrypt($request->get('password'));


        if($this->userModel->update($id, $data)) {
            // Doi quyen cho user
            $user = User::find($id);
            $user->revokeAllRoles();
            $user->assignRole($request->get('role_user'));

            \Session::flash('success', 'update thành công!');
            return redirect('user/' . $id);
        }
        \Session::flash('error', 'Có lỗi xảy ra!');
        return redirect('user/' . $id);
    }

    public function postDelete(Request $request) {

        if(!\Auth::user()->can('delete.user')) abort(553);

        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach($list_id as $id) {
                if($this->userModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted') , 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error , 'list_id' => $deleted]);
        } catch(\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

}