<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller {

    public function getIndex() {
        if (!\Auth::user()->can('view.slider')) abort(553);
        
        $listSlider = Slider::get();
        return view('backend.childs.slider.index')->with('listSlider', $listSlider);
    }

    public function postIndex(Request $request) {
        if (!\Auth::user()->can('edit.slider')) abort(553);

        $listSlider = Slider::get();
        foreach ($listSlider as $slider) {
            $slider->name = $request->get('name' . $slider->id);
            $slider->intro = $request->get('intro' . $slider->id);
            if($request->get('image' . $slider->id) == 'NULL') $slider->image = '';
            elseif ($request->get('image' . $slider->id) == '') {}
            else $slider->image = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('image' . $slider->id) );
            $slider->save();
        }

        if($request->has('image')) {
            $data['name'] = $request->get('name');
            $data['intro'] = $request->get('intro');
            $data['image'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('image') );
            Slider::create($data);
        }

        \Session::flash('success', 'Success!');
        return redirect()->route('slider');
    }

    public function postDelete(Request $request) {
        if (!\Auth::user()->can('delete.slider')) abort(553);
        
        $id = $request->get('id');
        $slider = Slider::find((int) $id);
        if($slider != null) {
            if($slider->delete()) {
                \Session::flash('success', 'Deleted!');
            }
        }
        \Session::flash('error', 'Error!');
        return response()->json(['']);
    }
}