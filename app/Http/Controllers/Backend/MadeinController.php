<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Madein\MadeinRepository;
use Illuminate\Http\Request;

class MadeinController extends Controller {

    public $madein_limit = 100;
    public $madein_order = ['col'=>'created_at', 'mode'=>'desc'];

    protected $madeinModel;

    public function __construct(MadeinRepository $madein)
    {
        $this->madeinModel = $madein;
    }

    public function getIndex() {
        if (!\Auth::user()->can('view.madein')) abort(553);
        
        $select = ['id', 'intro', 'name', 'created_at'];
        $listMadein = $this->madeinModel->getAll($select,$this->madein_order, $this->madein_limit);
        $data['listMadein'] = $listMadein;
        return view('backend.childs.madein.list')->with($data);
    }

    public function postCreate(Request $request) {
        try {
            $data = $request->except('_token', 'id');

            $result = $this->madeinModel->insert($data);
            if($result !== false) {
                return response()->json(['status' => 'success' , 'msg' => trans('form.created') , 'id' => $result->id, 'created_at'=>date('d/m/Y', strtotime($result->created_at))]);
            } else {
                return response()->json(['status' => 'error' , 'msg' => trans('form.create_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

    public function getEdit(Request $request) {
        if (!\Auth::user()->can('edit.madein')) abort(553);
        
        try {
            $id = $request->get('id');
            $madein = $this->madeinModel->getById($id);
            if($madein) {
                return response()->json(['status' => 'success' , 'data' => $madein ]);
            } else {
                return response()->json(['status' => 'error' , 'msg' => trans('form.edit_error')]);
            }
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

    public function postEdit(Request $request) {
        try {
            $id = $request->get('id');
            $data = $request->except('_token', 'id');
            if($id != '') {
                if($this->madeinModel->update($id, $data)) {
                    return response()->json(['status' => 'success' , 'msg' => trans('form.edited')]);
                }
            }
            return response()->json(['status' => 'error' , 'msg' => trans('form.create_error')]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }

    public function postDelete(Request $request) {
        if (!\Auth::user()->can('delete.madein')) abort(553);
        
        try {
            $error = '';
            $deleted = array();
            $list_id = $request->get('list_id');
            foreach($list_id as $id) {
                if($this->madeinModel->delete($id) === false) {
                    $error .= $id . '|';
                } else {
                    $deleted[] = $id;
                }
            }

            if ($error == '') {
                return response()->json(['status' => 'success', 'msg' => trans('form.deleted') , 'list_id' => $deleted]);
            }
            return response()->json(['status' => 'error', 'msg' => trans('form.delete_error') . ': ' . $error , 'list_id' => $deleted]);
        } catch(\Exception $ex) {
            return json_encode(['status' => 'error' , 'msg' => $ex->getMessage()]);
        }
    }
}