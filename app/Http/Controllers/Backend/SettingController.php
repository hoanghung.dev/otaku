<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\PageSetting;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller {

    public function getIndex() {
        if (!\Auth::user()->can('view.setting')) abort(553);
        
        $setting = Setting::first();
        return view('backend.childs.setting.index')->with('setting', $setting);
    }

    public function postIndex(Request $request) {
        if (!\Auth::user()->can('edit.setting')) abort(553);
        
        $data = $request->except('_token', 'sitemap');
        if(isset($data['site_offline'])) $data['site_offline'] = 1;
        if(isset($data['cache'])) $data['cache'] = 1;
        if(isset($data['send_mail'])) $data['send_mail'] = 1;
        $data['logo'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('logo') );
        $data['favicon'] = str_replace('http://'.$_SERVER['SERVER_NAME'].'/public/filemanager/userfiles/','',$request->get('favicon') );
        $setting = Setting::first();
        foreach ($data as $key => $value) {
            $setting->$key = $value;
        }
        # sitemap
        if($request->hasFile('sitemap')) {
            $file = $request->file('sitemap');
            $file->move(base_path() . '/', 'sitemap.xml');
        }

        $setting->save();
        \Session::flash('success', 'Edit success!');
        return redirect()->route('setting');
    }

    public function getHomeSetting() {
        if (!\Auth::user()->can('view.setting')) abort(553);

        $setting = PageSetting::where('page_name', 'home')->first();
        if(!is_object($setting)) abort(404);

        $data['settingHome'] = json_decode($setting->data);

        $listCategory = Category::select('id', 'name', 'parent_id')->get();
        $data['listCategory'] = $listCategory;
        
//        dump($data['setting']);

        return view('backend.childs.setting.home')->with($data);
    }

    public function postHomeSetting(Request $request) {
        if (!\Auth::user()->can('edit.setting')) abort(553);

        $setting = PageSetting::where('page_name', 'home')->first();
        if(!is_object($setting)) abort(404);

        $data = json_decode($setting->data);

        $data->listCategory = $request->has('listCategory') ? $request->get('listCategory') : [];

        $data->listBox = $request->has('listBox') ? $request->get('listBox') : [];

        $data->support->hotline1 = $request->has('hotline1') ? $request->get('hotline1') : '';
        $data->support->email1 = $request->has('email1') ? $request->get('email1') : '';
        $data->support->hotline2 = $request->has('hotline2') ? $request->get('hotline2') : '';
        $data->support->email2 = $request->has('email2') ? $request->get('email2') : '';
        $data->support->hotline3 = $request->has('hotline3') ? $request->get('hotline3') : '';
        $data->support->email3 = $request->has('email3') ? $request->get('email3') : '';
        $data->support->hotline4 = $request->has('hotline4') ? $request->get('hotline4') : '';
        $data->support->email4 = $request->has('email4') ? $request->get('email4') : '';

        $setting->data = json_encode($data);

        if($setting->save()) {
            \Session::flash('success', 'Edit success!');
        } else {
            \Session::flash('error', 'Edit error!');
        }

        return redirect()->route('setting.home');
    }
}