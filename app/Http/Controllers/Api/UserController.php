<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller {

    public function postRegister(Request $request) {

        $data = $request->all();

        $data['password'] = bcrypt($data['password']);

        $user = User::where('email', $data['email'])->first();
        if(is_object($user)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Email đã được sử dụng'
            ]);
        }

        $user = User::create($data);
        if($user != false) {
            // Doi quyen cho user
//            $user->assignRole($request->get('role_user'));

            return response()->json([
                'status'    => 'success',
                'msg'       => 'Tạo thành công!'
            ]);
        }
        return response()->json([
            'status'    => 'error',
            'msg'       => 'Tạo thất bại!'
        ]);
    }

    public function postLogin(Request $request)
    {
        $data = $request->all();

        if (\Auth::attempt(['email' => $data['email'], 'password' => $data['password']]))
        {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Đăng nhập thành công!',
                'data'      => \Auth::user()
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Sai email hoặc mật khẩu!'
        ]);
    }

    public function getDetail(Request $request) {
        $id = $request->get('id');

        $user = User::find($id);

        if(!is_object($user)) return response()->json([
            'status'    => 'error',
            'msg'       => 'Không tìm thấy'
        ]);

        return response()->json([
            'status'    => 'success',
            'data'      => $user
        ]);
    }

}