<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function getDetail(Request $request) {
        $id = $request->get('id');

        $item = Category::find($id);

        if(!is_object($item)) return response()->json([
            'status'    => 'error',
            'msg'       => 'Không tìm thấy'
        ]);

        return response()->json([
            'status'    => 'success',
            'data'      => $item
        ]);
    }

    public function postCreate(Request $request)
    {
        $data = $request->all();
        
        $result = Category::create($data);
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Tạo thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Tạo thất bại'
        ]);
    }

    public function postEdit(Request $request)
    {
        $data = $request->except('id');

        $item = Category::find($request->get('id'));

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        foreach ($data as $key => $value) {
            $item->$key = $value;
        }

        $result = $item->save();
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Cập nhật thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Cập nhật thất bại'
        ]);
    }

    public function deleteDelete(Request $request)
    {
        $id = $request->get('id');

        $item = Category::find($id);

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        $item->delete();

        return response()->json([
            'status'    => 'success',
            'msg'       => 'Xóa thành công'
        ]);
    }

    public function getSearch(Request $request) {
        $where = $this->filter($request);
        $limit = $request->get('limit', 10);

        $listItem = Category::whereRaw($where)->orderBy('id', 'desc')->limit($limit)->get();

        return response()->json([
            'status'    => 'success',
            'data'      => $listItem
        ]);
    }

    public function filter($request) {
        $where = '1=1 ';
        #
        if($request->has('date_start')) $where .= " AND created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('date_start'))) . "'";
        if($request->has('date_end')) $where .= " AND created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('date_end'))) . "'";
        #
        if ($request->has('name')) $where .= " AND name LIKE '%" . $request->get('name') . "%'";
        if ($request->has('status')) $where .= " AND status = " . $request->get('status');
        if ($request->has('user_id')) $where .= " AND user_id = " . $request->get('user_id');

        return $where;
    }

}