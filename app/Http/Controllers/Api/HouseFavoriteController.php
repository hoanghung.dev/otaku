<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\House;
use App\Models\Housefavorite;
use App\Models\User;
use Illuminate\Http\Request;

class HouseFavoriteController extends Controller
{

    public function postCreate(Request $request)
    {
        $data = $request->all();

        $result = Housefavorite::create($data);
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Tạo thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Tạo thất bại'
        ]);
    }

    public function postEdit(Request $request)
    {
        $data = $request->except('id');

        $item = Housefavorite::find($request->get('id'));

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        foreach ($data as $key => $value) {
            $item->$key = $value;
        }

        $result = $item->save();
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Cập nhật thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Cập nhật thất bại'
        ]);
    }

    public function deleteDelete(Request $request)
    {
        $id = $request->get('id');

        $item = Housefavorite::find($id);

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        $item->delete();

        return response()->json([
            'status'    => 'success',
            'msg'       => 'Xóa thành công'
        ]);
    }

    public function getSearch(Request $request) {
        $where = $this->filter($request);
        $limit = $request->get('limit', 10);

        $listHouseId = Housefavorite::whereRaw($where)->orderBy('id', 'desc')->pluck('house_id', 'id');
        $listHousefavoriteId = [];
        if(count($listHouseId) > 0) {
            foreach ($listHouseId as $k => $v) {
                $listHousefavoriteId[$v] = $k;
            }
        }

        $listItem = House::selectRaw('id as house_id, name, price, acreage, user_id, image_extra, created_at, category_id, address, type, vip')->whereIn('id', $listHouseId)->limit($limit)->get();
        if(count($listItem) > 0) {
            foreach ($listItem as $k => $v) {
                $listItem[$k]->id = $listHousefavoriteId[$v->house_id];
            }
        }

        return response()->json([
            'status'    => 'success',
            'data'      => $listItem
        ]);
    }

    public function filter($request) {
        $where = '1=1 ';
        #
        if($request->has('date_start')) $where .= "AND created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('date_start'))) . "'";
        if($request->has('date_end')) $where .= "AND created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('date_end'))) . "'";
        #
        if ($request->has('house_id')) $where .= "AND house_id = " . $request->get('house_id');
        if ($request->has('user_id')) $where .= "AND user_id = " . $request->get('user_id');

        return $where;
    }
}