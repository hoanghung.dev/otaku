<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\House;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    public function getDetail(Request $request) {
        $id = $request->get('id');

        $item = Notification::find($id);

        if(!is_object($item)) {
            $house = House::find($item->house_id);

            if(!is_object($house)) {
                return response()->json([
                    'status'    => 'error',
                    'msg'       => 'Không tìm thấy tin hoặc tin đã bị xóa'
                ]);
            }

            $item->status = 1;
            $item->save();

            return response()->json([
                'status'    => 'success',
                'data'      => $house
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Không tìm thấy'
        ]);
    }

    public function deleteDelete(Request $request)
    {
        $id = $request->get('id');

        $item = Notification::find($id);

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        $item->delete();

        return response()->json([
            'status'    => 'success',
            'msg'       => 'Xóa thành công'
        ]);
    }

    public function getSearch(Request $request) {
        $where = $this->filter($request);
        $limit = $request->get('limit', 10);

        $listItem = Notification::with('house')->whereRaw($where)->orderBy('id', 'desc')->limit($limit)->get();

        return response()->json([
            'status'    => 'success',
            'data'      => $listItem
        ]);
    }

    public function filter($request) {
        $where = '1=1 ';
        #
        if($request->has('date_start')) $where .= "AND created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('date_start'))) . "'";
        if($request->has('date_end')) $where .= "AND created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('date_end'))) . "'";
        #
        if ($request->has('house_id')) $where .= 'AND house_id = ' . $request->get('house_id');
        if ($request->has('user_id')) $where .= "AND user_id = " . $request->get('user_id');
        if ($request->has('type')) $where .= "AND type = " . $request->get('type');
        if ($request->has('status')) $where .= "AND status = " . $request->get('status');
        return $where;
    }
}