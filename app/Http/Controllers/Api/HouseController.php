<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\House;
use Illuminate\Http\Request;

class HouseController extends Controller
{

    public function getDetail(Request $request) {
        $id = $request->get('id');

        $item = House::find($id);

        if(!is_object($item)) return response()->json([
            'status'    => 'error',
            'msg'       => 'Không tìm thấy'
        ]);

        return response()->json([
            'status'    => 'success',
            'data'      => $item
        ]);
    }

    public function postCreate(Request $request)
    {
        $data = $request->all();

        $image_extra = [];
        for ($i = 1; $i <= 8; $i ++) {
            if ($request->hasFile('image_' . $i)) {
                $image_extra[] = saveAndResizeImage($request->file('image_' . $i), 'house', env('IMAGE_THUMB_WIDTH'), 'auto');
            }
        }
        $data['image_extra'] = json_encode($image_extra);
        
        $result = House::create($data);
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Tạo thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Tạo thất bại'
        ]);
    }

    public function postEdit(Request $request)
    {
        $data = $request->except('id');

        $image_extra = [];
        for ($i = 1; $i <= 8; $i ++) {
            if ($request->hasFile('image_' . $i)) {
                $image_extra[] = saveAndResizeImage($request->file('image_' . $i), 'house', env('IMAGE_THUMB_WIDTH'), 'auto');
            }
        }
        $data['image_extra'] = json_encode($image_extra);

        $item = House::find($request->get('id'));

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        foreach ($data as $key => $value) {
            $item->$key = $value;
        }

        $result = $item->save();
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Cập nhật thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Cập nhật thất bại'
        ]);
    }

    public function deleteDelete(Request $request)
    {
        $id = $request->get('id');

        $item = House::find($id);

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        $item->delete();

        return response()->json([
            'status'    => 'success',
            'msg'       => 'Xóa thành công'
        ]);
    }

    public function getSearch(Request $request) {
        $where = $this->filter($request);
        $limit = $request->get('limit', 10);

        $listItem = House::with('user')->whereRaw($where)->orderBy('updated_at', 'desc')->limit($limit)->get();

        // Tim kiem theo lat - long
        if ($request->has('lat') && $request->has('long')) {
            $rand_find = $request->get('rand', 1);
            #
            $modal = new GeoLocation();
            $edison_nj = $modal->fromDegrees($request->get('lat'), $request->get('long'));
            foreach ($listItem as $key => $item) {
                $brooklyn_ny = $modal->fromDegrees($item->lat, $item->long);
                $rand = $edison_nj->distanceTo($brooklyn_ny, 'kilometers');
                if ($rand > $rand_find) unset($listItem[$key]);
            }
        }

        return response()->json([
            'status'    => 'success',
            'data'      => $listItem
        ]);
    }

    public function filter($request) {
        $where = '1=1 ';
        #
        if($request->has('date_start')) $where .= " AND created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('date_start'))) . "'";
        if($request->has('date_end')) $where .= " AND created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('date_end'))) . "'";
        #
        if ($request->has('category_id')) $where .= ' AND category_id = ' . $request->get('category_id');
        if ($request->has('keyword')) $where .= " AND name LIKE '%" . $request->get('keyword') . "%'";
        if ($request->has('status')) $where .= " AND status = " . $request->get('status');
        if ($request->has('acreage')) $where .= " AND acreage = " . $request->get('acreage');
        if ($request->has('vip')) $where .= " AND vip = " . $request->get('vip');
        if ($request->has('user_id')) $where .= " AND user_id = " . $request->get('user_id');
        if ($request->has('category_id')) $where .= " AND category_id = " . $request->get('category_id');
        if ($request->has('contact_name')) $where .= " AND contact_name = '" . $request->get('contact_name') . "'";
        if ($request->has('contact_tel')) $where .= " AND contact_tel = '" . $request->get('contact_tel') . "'";
        if ($request->has('min_price')) $where .= " AND price >= " . $request->get('min_price');
        if ($request->has('max_price')) $where .= " AND price <= " . $request->get('max_price');
        if ($request->has('lat') && $request->has('long')) {
            $lat = $request->get('lat');
            $long = $request->get('long');
            $where .= " AND lat >= " . ($lat - 0.0094657) ;
        }
        return $where;
    }

    public function postPublish(Request $request)
    {
        try {
            $id = $request->get('id');

            $status = $this->productModel->publish($id);
            if ($status != false) {
                return response()->json(['status' => 'success', 'msg' => trans('form.edited'), 'status_publish' => $status]);
            }
            return response()->json(['status' => 'error', 'msg' => 'Action error']);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function postBlock(Request $request) {
        $id = $request->get('id');

        $item = House::find($id);

        if(!is_object($item)) return response()->json([
            'status'    => 'error',
            'msg'       => 'Không tìm thấy'
        ]);

        if($item->status == 2) {
            $item->status = 0;
        } else {
            $item->status = 2;
        }
        $item->save();

        return response()->json([
            'status'    => 'success',
            'msg'       => 'Block thành công'
        ]);
    }
}