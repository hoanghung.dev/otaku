<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function getSearch(Request $request) {
        $where = $this->filter($request);
        $limit = $request->get('limit', 10);

        $listItem = Service::whereRaw($where)->orderBy('id', 'asc')->limit($limit)->get();

        return response()->json([
            'status'    => 'success',
            'data'      => $listItem
        ]);
    }

    public function getDetail(Request $request) {
        $id = $request->get('id');

        $item = Service::find($id);

        if(!is_object($item)) return response()->json([
            'status'    => 'error',
            'msg'       => 'Không tìm thấy'
        ]);

        return response()->json([
            'status'    => 'success',
            'data'      => $item
        ]);
    }

    public function filter($request) {
        $where = '1=1 ';
        #
        if($request->has('date_start')) $where .= "AND created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('date_start'))) . "'";
        if($request->has('date_end')) $where .= "AND created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('date_end'))) . "'";
        #
        if ($request->has('name')) $where .= "AND name LIKE '%" . $request->get('name') . "%'";
        if ($request->has('sale')) $where .= "AND sale = '" . $request->get('sale') . "'";
        if ($request->has('status')) $where .= "AND status = '" . $request->get('status') . "'";
        if ($request->has('price')) $where .= "AND price >= " . $request->get('price');
        return $where;
    }
}