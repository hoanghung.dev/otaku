<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Fllow;
use Illuminate\Http\Request;

class FllowController extends Controller
{

    public function postCreate(Request $request)
    {
        $data = $request->all();

        $item = Fllow::where('user_id', $request->get('user_id'))->first();
        if(is_object($item)) {
            $this->postEdit($request);
        }

        $result = Fllow::create($data);
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Tạo thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Tạo thất bại'
        ]);
    }

    public function postEdit(Request $request)
    {
        $data = $request->except('user_id');

        $item = Fllow::where('user_id', $request->get('user_id'))->first();

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        foreach ($data as $key => $value) {
            $item->$key = $value;
        }

        $result = $item->save();
        if ($result !== false) {
            return response()->json([
                'status'    => 'success',
                'msg'       => 'Cập nhật thành công'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'msg'       => 'Cập nhật thất bại'
        ]);
    }

    public function deleteDelete(Request $request)
    {
        $id = $request->get('id');

        $item = Fllow::find($id);

        if(!is_object($item)) {
            return response()->json([
                'status'    => 'error',
                'msg'       => 'Không tìm thấy'
            ]);
        }

        $item->delete();

        return response()->json([
            'status'    => 'success',
            'msg'       => 'Xóa thành công'
        ]);
    }

    public function getDetail(Request $request) {
        $id = $request->get('id');

        $item = Fllow::find($id);

        if(!is_object($item)) return response()->json([
            'status'    => 'error',
            'msg'       => 'Không tìm thấy'
        ]);

        return response()->json([
            'status'    => 'success',
            'data'      => $item
        ]);
    }

    public function getSearch(Request $request) {
        $where = $this->filter($request);
        $limit = $request->get('limit', 10);

        $listItem = Fllow::whereRaw($where)->orderBy('id', 'desc')->limit($limit)->get();

        return response()->json([
            'status'    => 'success',
            'data'      => $listItem
        ]);
    }

    public function filter($request) {
        $where = '1=1 ';
        #
        if($request->has('date_start')) $where .= "AND created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('date_start'))) . "'";
        if($request->has('date_end')) $where .= "AND created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('date_end'))) . "'";
        #
        if ($request->has('category_id')) $where .= 'AND category_id = ' . $request->get('category_id');
        if ($request->has('keyword')) $where .= "AND name LIKE '%" . $request->get('keyword') . "%'";
        if ($request->has('acreage')) $where .= "AND acreage = " . $request->get('acreage');
        if ($request->has('vip')) $where .= "AND vip = " . $request->get('vip');
        if ($request->has('user_id')) $where .= "AND user_id = " . $request->get('user_id');
        if ($request->has('type')) $where .= "AND type = " . $request->get('type');
        if ($request->has('contact_name')) $where .= "AND contact_name = '" . $request->get('contact_name') . "'";
        if ($request->has('contact_tel')) $where .= "AND contact_tel = '" . $request->get('contact_tel') . "'";
        if ($request->has('price_start')) $where .= "AND price >= " . $request->get('price_start');
        if ($request->has('price_end')) $where .= "AND price <= " . $request->get('price_end');
        return $where;
    }

    public function postPublish(Request $request)
    {
        try {
            $id = $request->get('id');

            $status = $this->productModel->publish($id);
            if ($status != false) {
                return response()->json(['status' => 'success', 'msg' => trans('form.edited'), 'status_publish' => $status]);
            }
            return response()->json(['status' => 'error', 'msg' => 'Action error']);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }
}