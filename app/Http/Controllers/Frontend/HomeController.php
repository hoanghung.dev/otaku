<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\House;
use App\Models\Contact;
use App\Models\Setting;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public $postOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $postLimit = 10;

    public $categoryOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $categoryLimit = 100;

    protected $post_model;
    protected $setting;

    public function __construct(PostRepository $post, CategoryRepository $category)
    {
        $this->post_model = $post;
        $this->categoryModel = $category;
        $this->setting = Setting::first();
    }

    public function getIndex() {

        $listCategory = Category::where('status', 1)->where('type', 1)->orderBy('order_no', 'asc')->get();
        $data['listCategory'] = $listCategory;
        #
        $listHouse = [];
        foreach ($listCategory as $item) {
            $listHouse[$item->id] = House::where('status' , 1)->where('category_id', $item->id)->orderBy('id', 'desc')->limit(10)->get();
        }
        $data['listHouse'] = $listHouse;

        return view('frontend.home.index')->with($data);
    }

    public function postIndex(Request $request) {

        $data = $request->all();

        dd($data);
    }

    public function getLienHe() {
        return view('frontend.home.lienhe');
    }


    public function postContact(Request $request) {
        $data = $request->except('_token');

        Contact::create($data);

        $setting = Setting::first();

        $msg = 'Name: ' . $request->get('name') . ' | Email: ' . $request->get('email') . ' | Phone: ' . $request->get('tel') . ' | Message: ' . $request->get('message');
        $msg = wordwrap($msg, 1000);
        mail($setting->mail_from, $setting->mail_name, $msg);

        return response()->json([
            'status'    => 'success',
            'message'   => 'Tạo thành công!'
        ]);
    }

}