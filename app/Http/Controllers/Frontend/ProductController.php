<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Frontend;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\Order;
//use App\Repositories\Bill\BillRepository;
use App\Models\Post;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Tag;
use App\Models\User;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Sale\SaleRepository;
use App\Repositories\Manufacturer\ManufacturerRepository;
use App\Repositories\Madein\MadeinRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;
use Illuminate\Support\Facades\Mail;

class ProductController extends Controller
{

    public $productOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $productLimit = 10;

    public $postRelationLimit = 6;

    public $categoryOrder = ['col' => 'created_at', 'mode' => 'desc'];
    public $categoryLimit = 100;

    protected $productModel;
    protected $postModel;
    protected $categoryModel;
    protected $saleModel;
    protected $billModel;
    protected $orderModel;
    protected $manufacturerModel;
    protected $madeinModel;

    public function __construct(ProductRepository $product, CategoryRepository $category, ManufacturerRepository $manufacturer, MadeinRepository $madein, SaleRepository $sale, PostRepository $post)
    {
        $this->categoryModel = $category;
        $this->productModel = $product;
        $this->postModel = $post;
        $this->saleModel = $sale;
        $this->manufacturerModel = $manufacturer;
        $this->madeinModel = $madein;
    }

    public function getIndex($categorySlug, $itemSlug)
    {
        $category = $this->categoryModel->getBySlug($categorySlug);
        $data['category'] = $category;

        if (!$category) return abort(404);

        $pageOption = [
            'type' => 'product',
            'parentUrl' => $category->slug,
            'parentName' => $category->name,
        ];

        $post = $this->postModel->getBySlug($itemSlug);
        if (!is_object($post) || $post->status == 0) abort(404);
        $data['post'] = $post;
        $pageOption['pageName'] = $post->name;
        $pageOption['image'] = getUrlImageThumb($post->image, env('IMAGE_THUMB_WIDTH'), env('IMAGE_THUMB_HEIGHT'));
        $pageOption['link'] = \URL::to($category->slug . '/' . $post->slug);
        $pageOption['description'] = strip_tags(getIntro(str_replace('"', "'", $post->intro), 17));

        $select = ['name', 'slug', 'created_at', 'category_id', 'content', 'image'];
        $postRelate = $this->postModel->getRelation($post->id, $select, $this->postRelationLimit);
        $data['postRelate'] = $postRelate;
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.post.index')->with($data);
    }

    public function postAjaxFilter(Request $request)
    {
        if ($request->has('type') && $request->get('type') == 'delete') {
            unset($_SESSION[$request->get('data')]);
            return response()->json(['status' => 'success']);
        }

        $data = $request->get('data');
        $type = $request->get('type');
        if ($type == 'sx') {
            switch ($data) {
                case 'gtn':
                    $_SESSION['sx']['col'] = 'final_price';
                    $_SESSION['sx']['mode'] = 'ASC';
                    $_SESSION['sx']['type'] = 'gtn';
                    break;
                case 'cdt':
                    $_SESSION['sx']['col'] = 'final_price';
                    $_SESSION['sx']['mode'] = 'DESC';
                    $_SESSION['sx']['type'] = 'cdt';
                    break;
                case 'mc':
                    $_SESSION['sx']['col'] = 'id';
                    $_SESSION['sx']['mode'] = 'DESC';
                    $_SESSION['sx']['type'] = 'mc';
                    break;
                case 'az':
                    $_SESSION['sx']['col'] = 'name';
                    $_SESSION['sx']['mode'] = 'ASC';
                    $_SESSION['sx']['type'] = 'az';
                    break;
                case 'za':
                    $_SESSION['sx']['col'] = 'name';
                    $_SESSION['sx']['mode'] = 'DESC';
                    $_SESSION['sx']['type'] = 'za';
                    break;
            }
        } else {
            $_SESSION[$type] = $data;
        }
        return response()->json(['status' => 'success']);
    }

    public function postBuy(Request $request)
    {
        if ($request->has('id') && $request->has('value')) {
            $productId = $request->get('id');
            $productValue = $request->get('value');
            if (!isset($_SESSION['order'])) $_SESSION['order'] = array();
            if (!isset($_SESSION['order'][$productId])) {       // Neu san pham do chua dat hang
                $_SESSION['order'][$productId] = array('id' => $productId, 'value' => $productValue);
            } else {                                            // Neu da dat hang san pham do
                $_SESSION['order'][$productId] = array('id' => $productId, 'value' => $productValue + $_SESSION['order'][$productId]['value']);
            }
            $product = $this->productModel->getById($productId);
            return response()->json(['status' => 'success', 'catNumber' => count($_SESSION['order']), 'number' => $productValue, 'name' => $product->name, 'slug' => \URL::to($product->category->slug . '/' . $product->slug),
                'price' => number_format($product->final_price * $productValue, 0, ".", ","), 'image' => \URL::asset('uploads/' . $product->image)]);
        }
        return response()->json(['status' => 'error', 'msg' => 'Empty id or value']);
    }

    public function getCart()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Giỏ hàng',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);
        return view('frontend.childs.product.cart');
    }

    public function postCart(Request $request)
    {
        $data = $request->except('_token');

        $setting = Setting::first();
        $msg = 'Có đơn đặt hàng mới: Khách hàng: ' . $data['name'] . ' . Điện thoại: ' . $data['tel'] . ' . Tổng tiền: ' . number_format($data['total_price'], 0, '.', '.') . 'đ';
        $msg = wordwrap($msg, 1000);
//        mail($setting->mail_from, $setting->mail_name, $msg);
        Mail::send('frontend.email.order', ['title' => $setting->mail_name, 'msg' => $msg], function ($message) use ($setting) {

            $message->from('hoanghung.developer@gmail.com', 'thietbibanle.com');

            $message->to($setting->mail_from);

        });


        $bill = Bill::create($data);
        if ($bill !== false) {
            foreach ($_SESSION['order'] as $key => $item) {
                $product = $this->productModel->getById($key);
                $data = [
                    'bill_id' => $bill->id,
                    'price' => $key * $product->final_price,
                    'product_id' => $product->id,
                    'value' => $item['value']
                ];
                Order::create($data);
            }
        }
        unset($_SESSION['order']);

        \Session::flash('success', 'Đặt hàng thành công');
        return redirect('gio-hang');
    }

    public function getTest()
    {

//        $x = Tag::
//        $post = Post::withAnyTag(['Fsad'])->get();
//        dump($post[0]->name); die;

//        $user = User::find(8);
//        $role = Role::where('slug', 'admin')->first();
//        $user->assignRole($role);
//        die('ok');

//        $roleTeacher = Role::create([
//            'name'        => 'user',
//            'slug'        => 'user',
//            'description' => 'user'
//        ]);


        $permissionInternship = Permission::create([
            'name' => 'support',
            'slug' => [ // an array of permissions.
                'view' => true,
                'create' => true,
                'edit' => true,
                'delete' => true,
            ],
            'description' => 'support voi quantri'
        ]);

        $role = Role::where('slug', 'admin')->first();
        $role->assignPermission('support');
        die('product_category');
    }

    public function getSearch($name)
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Tìm kiếm',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        $select = ['id', 'intro', 'name', 'category_id', 'manufacturer_id', 'slug', 'base_price', 'final_price', 'image'];
        $listProduct = $this->productModel->searchName($select, $name, $this->productOrder, $this->productLimit);
        $data['listProduct'] = $listProduct;

        $select = ['id', 'name', 'slug', 'image'];
        $data['listManufacturer'] = $this->manufacturerModel->getAll($select, $this->productOrder, 100);

        $select = ['id', 'name'];
        $data['listMadein'] = $this->madeinModel->getAll($select, $this->productOrder, 100);
        $data['type'] = 'search';

        return view('frontend.childs.productCategory.index')->with($data);
    }

    public function postProductByManufacturer(Request $request)
    {
        $category_id = $request->get('category_id');
        $manufacturer_id = $request->get('manufacturer_id');
        $data['listProduct'] = Product::where('category_id', $category_id)->where('manufacturer_id', $manufacturer_id)->where('status', 'publish')->get();
        return view('frontend.childs.product.get_by_manufacturer')->with($data);
    }

    public function getCompare()
    {
        if (isset($_SESSION['compare'])) {
            $arrayId = [];
            foreach ($_SESSION['compare'] as $key => $item) {
                $arrayId[] = $key;
            }
        } else {
            abort(404);
        }

        $listProduct = Product::whereIn('id', $arrayId)->paginate($this->productLimit);
        $data['listProduct'] = $listProduct;

        $select = ['id', 'name', 'slug'];
        $data['listManufacturer'] = $this->manufacturerModel->getAll($select, $this->productOrder, 100);

        $select = ['id', 'name'];
        $data['listMadein'] = $this->madeinModel->getAll($select, $this->productOrder, 100);

        $pageOption = [
            'type' => 'page',
            'pageName' => 'So sánh',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.productCategory.index')->with($data);
    }

    public function getPrint(Request $request, $slug)
    {
        $product = Product::where('slug', $slug)->first();
        if (!is_object($product)) abort(404);

        $data['product'] = $product;

        return view('frontend.partials.print')->with($data);
    }

    public function postCompare(Request $request)
    {
        $productId = $request->get('id');
        if (!isset($_SESSION['compare'])) $_SESSION['compare'] = array();
        if (!isset($_SESSION['compare'][$productId])) {       // Neu san pham do chua dat hang
            $_SESSION['compare'][$productId] = array('id' => $productId);
        } else {                                            // Neu da dat hang san pham do
            $_SESSION['compare'][$productId] = array('id' => $productId);
        }
        $product = $this->productModel->getById($productId);
        return response()->json(['status' => 'success', 'name' => $product->name, 'slug' => \URL::to($product->category->slug . '/' . $product->slug),
            'price' => number_format($product->final_price, 0, ".", ","), 'image' => \URL::asset('uploads/' . $product->image)]);
    }

    public function thanhToan(Request $request)
    {
        $id = $request->get('id');
        $post = Post::find($id);
        if (!is_object($post)) return '';

        return view('frontend.childs.product.thanhtoan')->with('post', $post);
    }

    public function listThanhToan()
    {
        return view('frontend.childs.product.listthanhtoan');
    }
}