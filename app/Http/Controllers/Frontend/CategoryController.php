<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\Product;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Madein\MadeinRepository;
use App\Repositories\Manufacturer\ManufacturerRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Models\Setting;

class CategoryController extends Controller
{

    public $productLimit = 10;
    public $productOrder = ['col' => 'id', 'mode' => 'desc'];

    public $postLimit = 6;
    public $postOder = ['col' => 'created_at', 'mode' => 'desc'];

    protected $categoryModel;
    protected $productModel;
    protected $postModel;
    protected $manufacturerModel;
    protected $madeinModel;

    public function __construct(CategoryRepository $category, ProductRepository $product, ManufacturerRepository $manufacturer, MadeinRepository $madein, PostRepository $post)
    {
        $this->categoryModel = $category;
        $this->productModel = $product;
        $this->postModel = $post;
        $this->manufacturerModel = $manufacturer;
        $this->madeinModel = $madein;
        $this->setting = Setting::first();
    }

    public function getIndex($slug)
    {
        $category = $this->categoryModel->getBySlug($slug);
        if (!$category) abort(404);
        $data['category'] = $category;

        $pageOption = [
            'type' => 'page',
            'pageName' => $category->name,
        ];
        view()->share('pageOption', $pageOption);

        if (in_array($category->type, [2, 4, 5])) {
            $select = ['id', 'intro', 'name', 'slug', 'created_at','category_id', 'image'];
            $listPost = $this->postModel->getAll($select, 'category_id = ' . $category->id, $this->postOder, $this->postLimit);
            $data['listPost'] = $listPost;

            $select = ['id', 'intro', 'name', 'slug', 'created_at','category_id', 'image'];
            $listPostNew = $this->postModel->getAll($select, 'category_id != 39', $this->productOrder, 6);
            $data['listPostNew'] = $listPostNew;

            return view('frontend.childs.category.index')->with($data);
        } else {
            $where = $this->getWhere();

            $listCategoryId = [$category->id];
            $childs = $category->childs;
            if(count($childs) > 0) {
                foreach ($childs as $child) {
                    $listCategoryId[] = $child->id;
                }
            }

            $listProduct = Product::select('id', 'intro', 'name', 'category_id', 'manufacturer_id', 'slug', 'base_price', 'final_price', 'image')
                ->where('status', 1)->whereRaw($where)->whereIn('category_id', $listCategoryId);
            if(isset($_SESSION['sx'])) {
                switch ($_SESSION['sx']['type']) {
                    case 'gtn':
                        $listProduct= $listProduct->orderBy('final_price','asc')->paginate($this->productLimit);
                        break;
                    case 'cdt':
                        $listProduct= $listProduct->orderBy('final_price','desc')->paginate($this->productLimit);
                        break;
                    case 'mc':
                        $listProduct= $listProduct->orderBy('id','desc')->paginate($this->productLimit);
                        break;
                    case 'az':
                        $listProduct= $listProduct->orderBy('name','asc')->paginate($this->productLimit);
                        break;
                    default:
                        $listProduct= $listProduct->orderBy('name','desc')->paginate($this->productLimit);
                        break;
                }
            } else {
                $listProduct= $listProduct->orderBy('id','desc')->paginate($this->productLimit);
            }

            $data['listProduct'] = $listProduct;

            $select = ['id', 'name', 'slug', 'image'];
            $data['listManufacturer'] = $this->manufacturerModel->getAll($select, $this->productOrder, 100);

            $select = ['id', 'name'];
            $data['listMadein'] = $this->madeinModel->getAll($select, $this->productOrder, 100);

            return view('frontend.childs.productCategory.index')->with($data);
        }
    }

    public function getWhere()
    {
        $where = ' 1 = 1 ';

        if (isset($_SESSION['price'])) {
            switch ($_SESSION['price']) {
                case 'p1':
                    $where .= ' AND final_price <= 1000000';
                    break;
                case 'p13':
                    $where .= ' AND final_price >= 1000000 AND final_price <= 3000000';
                    break;
                case 'p35':
                    $where .= ' AND final_price >= 3000000 AND final_price <= 5000000';
                    break;
                case 'p57':
                    $where .= ' AND final_price >= 5000000 AND final_price <= 7000000';
                    break;
                case 'p710':
                    $where .= ' AND final_price >= 5000000 AND final_price <= 10000000';
                    break;
                default:
                    $where .= ' AND final_price >= 10000000';
            }
        }

        if (isset($_SESSION['core'])) {
            $where .= " AND core = '" . $_SESSION['core'] . "'";
        }

        if (isset($_SESSION['system'])) {
            $where .= " AND system = '" . $_SESSION['system'] . "'";
        }

        if (isset($_SESSION['ram'])) {
            $where .= " AND ram = '" . $_SESSION['ram'] . "'";
        }

        if (isset($_SESSION['chip'])) {
            $where .= " AND chip = '" . $_SESSION['chip'] . "'";
        }

        if (isset($_SESSION['hard'])) {
            $where .= " AND hard = '" . $_SESSION['hard'] . "'";
        }
        return $where;
    }

}