<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller {

    public $post_order = ['col'=>'created_at', 'mode'=>'desc'];
    public $post_limit = 10;

    public $category_order = ['col'=>'created_at', 'mode'=>'desc'];
    public $category_limit = 100;

    protected $post_model;
    protected $categoryModel;

    public function __construct(PostRepository $post, CategoryRepository $category)
    {
        $this->post_model = $post;
        $this->categoryModel = $category;
    }

    public function getIndex() {
        $select = ['id', 'image', 'category_id', 'user_id', 'name', 'slug', 'created_at', 'seo_level' ];
        $where = '';
        $list_post = $this->post_model->getAll($select, $where,$this->post_order, $this->post_limit);
        $data['list_post'] = $list_post;

        $select = ['id', 'name', 'parent_id'];
        $list_category = $this->categoryModel->getAll($select, $where, $this->category_order, $this->category_limit);
        $data['list_category'] = $list_category;

        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Giỏ hàng',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);
        
        return view('backend.childs.post.list')->with($data);
    }


}