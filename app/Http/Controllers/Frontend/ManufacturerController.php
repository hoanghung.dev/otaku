<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Manufacturer;
use App\Models\Product;
use App\Repositories\Manufacturer\ManufacturerRepository;
use App\Repositories\Madein\MadeinRepository;
use Illuminate\Http\Request;

class ManufacturerController extends Controller {

    public $manufacturerLimit = 100;
    public $manufacturerOrder = ['col'=>'id', 'mode'=>'desc'];

    protected $manufacturerModel;
    protected $madeinModel;

    public function __construct(ManufacturerRepository $manufacturer, MadeinRepository $madeinModel)
    {
        $this->manufacturerModel = $manufacturer;
        $this->madeinModel = $madeinModel;
    }

    public function getIndex($slug) {

        $manufacturer = Manufacturer::where('slug', $slug)->first();

        if($manufacturer == null) abort(404);

        $data['manufacturer'] = $manufacturer;
        $listProduct = Product::where('manufacturer_id', $manufacturer->id)->orderBy('id', 'desc')->paginate(10);
        $data['listProduct'] = $listProduct;

        $select = ['id', 'name', 'slug', 'image'];
        $data['listManufacturer'] = $this->manufacturerModel->getAll($select, $this->manufacturerOrder, 100);

        $select = ['id', 'name'];
        $data['listMadein'] = $this->madeinModel->getAll($select, $this->manufacturerOrder, 100);

        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Thương hiệu ' . $manufacturer->name,
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.productCategory.index')->with($data);
    }
}