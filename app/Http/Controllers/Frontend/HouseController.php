<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\House;
use Illuminate\Http\Request;

class HouseController extends Controller {

    public function getDangtin() {
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Đăng tin',
            'description'=> 'Đăng tin'
        ];
        view()->share('pageOption', $pageOption);

        return view('frontend.childs.house.dangtin');
    }

}