<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Housefavorite extends Model
{

    protected $table = 'house_favorite';

    protected $fillable = [
        'house_id', 'user_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function house()
    {
        return $this->hasOne(House::class, 'id', 'house_id');
    }
}
