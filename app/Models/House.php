<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{

    protected $table = 'house';

    protected $fillable = [
        'acreage', 'category_id', 'content', 'image', 'image_extra', 'intro', 'name', 'price', 'status', 'user_id', 'address', 'type', 'vip',
        'lat', 'long', 'contact_name', 'contact_tel'
    ];

    public function user() {
        return $this->hasMany(User::class, 'id', 'user_id');
    }
}
