<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    protected $table = 'notification';

    protected $fillable = [
        'house_id', 'user_id', 'type'
    ];

    public function house() {
        return $this->hasOne(House::class, 'id', 'house_id');
    }
}
