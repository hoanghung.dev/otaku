<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace App\Models;

use App\Models\Relations\ProductHasOneMadein;
use App\Models\Relations\ProductHasOneManufacturer;
use Illuminate\Database\Eloquent\Model;
use App\Models\Relations\ProductBelongToCategory;
use App\Models\Relations\ProductBelongToUser;

class Product extends Model {

    use \Conner\Tagging\Taggable;

    protected $table = 'products';

    use ProductBelongToCategory, ProductBelongToUser, ProductHasOneManufacturer, ProductHasOneMadein;

    protected $fillable = ['name', 'code', 'slug', 'user_id','base_price', 'final_price', 'souther_price', 'intro',
                'content', 'status', 'seo_title', 'seo_description', 'seo_keywords', 'seo_level',
                'image', 'image_extra_1', 'image_extra_2', 'image_extra_3', 'image_extra_4',
                'category_id', 'category_orther_id', 'view_total', 'material', 'size', 'capacity',
                'guarantee', 'madein_id', 'manufacturer_id', 'sale_id', 'sale_description', 'tax_vat',
                'specifications', 'iframe', 'video', 'image_extra_1', 'image_extra_2', 'image_extra_3', 'image_extra_4',
                'core', 'system', 'ram', 'chip', 'hard'
    ];
}