<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model {

    protected $table = 'sales';

    protected $fillable = [
        'name', 'intro', 'type', 'value', 'category_id', 'manufacturer_id'
    ];

}