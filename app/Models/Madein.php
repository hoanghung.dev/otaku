<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Madein extends Model
{

    protected $table = 'madein';

    protected $fillable = [
        'name' , 'intro'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
