<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{

    protected $table = 'slider';

    protected $fillable = [
        'name', 'intro', 'link', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
