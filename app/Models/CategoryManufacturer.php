<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryManufacturer extends Model
{

    protected $table = 'category_manufacturer';

    protected $fillable = [
        'category_id', 'manufacturer_id'
    ];

    protected $softDelete = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
