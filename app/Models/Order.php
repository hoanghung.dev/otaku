<?php

namespace App\Models;

use App\Models\Relations\OrderHasOneProduct;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    use OrderHasOneProduct;

    protected $table = 'orders';

    protected $fillable = [
        'bill_id' , 'product_id' , 'value' , 'price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
