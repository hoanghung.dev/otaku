<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Relations\CategoryBelongToUser;
use App\Models\Relations\CategoryHasManyPost;
use App\Models\Relations\CategoryHasManyProduct;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    protected $table = 'categories';

    use CategoryHasManyPost, CategoryHasManyProduct, CategoryBelongToUser;

    protected $fillable = [
        'name' , 'slug' , 'parent_id' , 'intro' , 'image' , 'user_id' , 'status', 'type', 'order_no', 'icon'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne($this, 'id', 'parent_id');
    }
}
