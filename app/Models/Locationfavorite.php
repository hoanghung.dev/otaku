<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locationfavorite extends Model
{

    protected $table = 'location_favorite';

    protected $fillable = [
        'user_id', 'lat', 'long'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
