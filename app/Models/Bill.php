<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Relations\BillHasManyOrder;

class Bill extends Model
{

    protected $table = 'bills';

    use BillHasManyOrder;

    protected $fillable = [
        'name' , 'address' , 'email' , 'note' , 'date' , 'status', 'coupon_code' , 'receipt_method', 'sex', 'tel' , 'total_price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
