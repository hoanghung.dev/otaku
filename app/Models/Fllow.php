<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fllow extends Model
{

    protected $table = 'fllow';

    protected $fillable = [
        'address', 'category_id', 'lat', 'long', 'max_acreage', 'max_price', 'min_acreage', 'min_price', 'type', 'user_id', 'rad'
    ];
}
