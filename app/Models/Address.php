<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $table = 'address';

    protected $fillable = [
        'content', 'image', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
