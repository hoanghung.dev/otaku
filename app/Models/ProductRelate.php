<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRelate extends Model {

    protected $table = 'product_relate';

    protected $fillable = [
        'category_included',
        'category_sale',
        'category_selling',
        'product_id'
    ];

}