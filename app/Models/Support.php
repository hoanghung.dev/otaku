<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{

    protected $table = 'support';

    protected $fillable = [
        'name', 'image', 'tel', 'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
