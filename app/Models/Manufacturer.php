<?php

namespace App\Models;

use App\Models\Relations\ManufacturerHasOneMadein;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{

    protected $table = 'manufacturers';
    
    use ManufacturerHasOneMadein;

    protected $fillable = [
        'name' , 'slug' , 'intro', 'image', 'guarantee', 'madein_id', 'size'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
