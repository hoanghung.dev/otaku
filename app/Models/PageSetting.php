<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 27/07/2016
 * Time: 20:14
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageSetting extends Model {

    protected $table = 'page_setting';

    protected $fillable = [
        'page_name' , 'data'
    ];
}