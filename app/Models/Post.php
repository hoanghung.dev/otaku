<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Relations\PostBelongToCategory;
use App\Models\Relations\PostBelongToUser;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model {

    use \Conner\Tagging\Taggable;

    protected $table = 'posts';

    use PostBelongToCategory, PostBelongToUser;

    use SoftDeletes;

    protected $fillable = [
        'name', 'slug', 'user_id', 'intro', 'content', 'status', 'seo_title', 'seo_description',
        'seo_keyword', 'image', 'category_id', 'video'
    ];

    protected $dates = ['deleted_at'];

    protected $softDelete = true;
}