<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:11
 */
namespace App\Models\Relations;

use App\Models\Post;

trait CategoryHasManyPost {

    public function posts() {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }
}