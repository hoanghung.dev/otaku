<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:11
 */
namespace App\Models\Relations;

use App\Models\Product;

trait CategoryHasManyProduct {

    public function products() {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }
}