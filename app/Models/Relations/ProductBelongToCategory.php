<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:29
 */
namespace App\Models\Relations;

use App\Models\Category;

trait ProductBelongToCategory {

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }
}