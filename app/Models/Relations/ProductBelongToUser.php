<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:31
 */
namespace App\Models\Relations;

use App\Models\User;

trait ProductBelongToUser {

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}