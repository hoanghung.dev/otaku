<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:18
 */
namespace App\Models\Relations;

use App\Models\Post;

trait UserHasManyPost {

    public function posts() {
        return $this->hasMany(Post::class, 'user_id');
    }
}