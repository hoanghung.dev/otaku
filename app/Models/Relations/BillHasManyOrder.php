<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:11
 */
namespace App\Models\Relations;

use App\Models\Order;

trait BillHasManyOrder {

    public function orders() {
        return $this->hasMany(Order::class, 'bill_id', 'id');
    }
}