<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:29
 */
namespace App\Models\Relations;

use App\Models\Madein;

trait ManufacturerHasOneMadein {

    public function madein() {
        return $this->hasOne(Madein::class, 'id', 'madein_id');
    }
}