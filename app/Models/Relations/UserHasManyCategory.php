<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:21
 */
namespace App\Models\Relations;

use App\Models\Category;

trait UserHasManyCategory {

    public function categories() {
        return $this->hasMany(Category::class, 'user_id');
    }
}