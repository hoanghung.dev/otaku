<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:29
 */
namespace App\Models\Relations;

use App\Models\Manufacturer;

trait ProductHasOneManufacturer {

    public function manufacturer() {
        return $this->hasOne(Manufacturer::class, 'id', 'manufacturer_id');
    }
}