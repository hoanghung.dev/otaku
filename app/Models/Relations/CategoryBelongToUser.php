<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:27
 */
namespace App\Models\Relations;

use App\Models\User;

trait CategoryBelongToUser {

    public function user() {
        return $this->belongTo(User::class, 'user_id');
    }
}