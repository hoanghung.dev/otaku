<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:29
 */
namespace App\Models\Relations;

use App\Models\Product;

trait OrderHasOneProduct {

    public function product() {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}