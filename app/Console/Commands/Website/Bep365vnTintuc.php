<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 08/09/2016
 * Time: 19:52
 */
namespace App\Console\Commands\Website;

use App\Console\Commands\Base;
use App\Models\Post;
use App\Models\User;

class Bep365vnTintuc extends Base {

    public
    function __construct($categoryId)
    {
        print "Crawler bep365.vn ...... \n";
//        for ($i = 1; $i < 10; $i++) {   // Chi crawl 9 trang dau
//            $this->getCategory('http://vnexpress.net/tin-tuc/oto-xe-may/page/' . $i . '.html', 17);
//        }
        $this->getCategory('http://bep365.vn/tin-tuc', $categoryId);
    }

    function getCategory($url, $cateId)
    {
        print "Crawl " . $url . " \n";
        $html = $this->cUrl($url);
        $html = str_get_html($html);
        if (!empty($html)) foreach ($html->find('.list_news ul li') as $article) {
            if ($article->find('a', 0)) {
                $link = 'http://bep365.vn/' . $article->find('a', 0)->href;
                $this->getDetail($link, $cateId);
            }
        } else print "Don't get html category \n";
        $html->clear();
    }

    function getDetail($url, $cateId)
    {

        print "Crawl " . $url . " \n";
        $dom = $this->cUrl($url);
        $html = str_get_html($dom);
        if (!empty($html)) {
            $data['category_id'] = $cateId;
            $data['user_id'] = User::first()->id;
            $data['status'] = 1;
            $data['name'] = $data['seo_title'] = $data['seo_description'] = $html->find("meta[property=og:title]", 0)->getAttribute('content');

            if($this->checkExist(str_slug($data['name'], '-'), 'post')) {
                print str_slug($data['name'], '-')." exist !\n";
            }

            $data['slug'] = renderSlug(false, str_slug($data['name'], '-'), 'post');
            $data['intro'] = $html->find('meta[name="description"]', 0)->getAttribute('content');
            $data['seo_keywords'] = $html->find("meta[name=keywords]", 0)->getAttribute('content');

            if (!empty($html->find('meta[property="og:image"]'))) {
                $image = $html->find('meta[property="og:image"]', 0)->getAttribute('content');
                $data['image'] = saveImage($image, 'post');
            }

            
            //$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
            $data['content'] = $html->find('#detail_news .article', 0)->innertext;
            $data['content'] = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $data['content']);
            $data['content'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['content']);
            $data['content'] = preg_replace('/<ins class="adsbygoogle".*?<\/ins>/s', '', $data['content']);
            $data['content'] = preg_replace('#<iframe(.*?)>(.*?)</iframe>#is', '', $data['content']);


            /*if(!empty($data['content'])) foreach(str_get_html($data['content'])->find("img") as $i=>$img) {
                $image = $img->src;
                $path = '/'.date('Ym').'/content/'; // Directory to upload files to.
                $directory = $this->uploadImageURL($image,$path,$data['slug'].'-'.$i);
                $data['content'] = str_replace($image, _ROOT_UPLOAD.$directory, $data['content']);
            }*/

            if (Post::create($data)) print "Insert '.$url.' done !\n"; else print "Insert '.$url.' unsuccess !\n";
            unset($data);
            $html->clear();
        } else print "Don't get html detail\n";
    }

}