<?php

namespace App\Console\Commands;

require_once 'simple_html_dom.php';

use App\Console\Commands\Website\Bep365vnTintuc;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CrawlPost extends Command
{

    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawldata:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl du lieu post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bep365vnTintuc = new Bep365vnTintuc(13);
    }
}
