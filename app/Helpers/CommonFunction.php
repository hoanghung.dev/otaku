<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 13/05/2016
 * Time: 15:13
 */

if (!function_exists('get_from_cache')) {
    function get_from_cache($key)
    {
        if (env('APP_ENV') == 'production') {
            if (\Cache::has($key)) {
                return \Cache::get($key);
            }
        }
        return false;
    }
}

if (!function_exists('cache_it')) {
    function cache_it($key, $value)
    {
        if (env('APP_ENV') == 'production') {
            return \Cache::put($key, $value, env('CACHE_TIME'));
        }
        return false;
    }
}

if (!function_exists('renderSlug')) {
    function renderSlug($id, $slug, $class)
    {
        switch ($class) {
            case 'manufacturer' :
                $classModel = new \App\Models\Manufacturer();
                break;
            case 'category' :
                $classModel = new \App\Models\Category();
                break;
            case 'post':
                $classModel = new \App\Models\Post();
                break;
            default:
                $classModel = new \App\Models\Product();
                break;
        }

        $item = $classModel::where('slug', '=', $slug);
        if ($id !== false) $item = $item->where('id', '!=', $id);
        $item = $item->get();

        if (count($item) != 0) {
            return $slug . '-' . rand(1, 100);
        }
        return $slug;
    }
}

function getIntro($text, $limit)
{

    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]);
    }
    return $text;
}

/*Image*/
function resizeImage($file, $width, $height)
{

//    $file = saveImage($file, $path);

    # Doi ten anh hung.jpg => hung_200x200.jpg
    $reFile = str_replace('.jpg', '_' . $width . 'x' . $height . '.jpg', str_replace('.png', '_' . $width . 'x' . $height . '.png', str_replace('.gif', '_' . $width . 'x' . $height . '.gif', $file)));

    if ($height == 'auto') {
        // Set chieu cao anh tu dong

        $widthImg = isset(getimagesize(\URL::asset('filemanager/userfiles/' . $file))[0]) ? getimagesize(\URL::asset('filemanager/userfiles/' . $file))[0] : env('IMAGE_THUMB_HEIGHT');
        $height = isset(getimagesize(\URL::asset('filemanager/userfiles/' . $file))[1]) ? getimagesize(\URL::asset('filemanager/userfiles/' . $file))[1] : env('IMAGE_THUMB_HEIGHT');
        $height = $height / ($widthImg / $width);
    }

    \Image::make(base_path() . '/public/filemanager/userfiles/' . $file)->resize($width, $height)->save(base_path() . '/public/filemanager/userfiles/' . '_thumbs/' . $reFile);
    return $file;
}

function saveAndResizeImage($file, $path, $width, $height)
{

    $file = saveImage($file, $path);

    # Doi ten anh hung.jpg => hung_200x200.jpg
    $reFile = str_replace('.jpg', '_' . $width . 'x' . $height . '.jpg', str_replace('.png', '_' . $width . 'x' . $height . '.png', str_replace('.gif', '_' . $width . 'x' . $height . '.gif', $file)));

    if ($height == 'auto') {
        // Set chieu cao anh tu dong

        $widthImg = isset(getimagesize(\URL::asset('filemanager/userfiles/' . $file))[0]) ? getimagesize(\URL::asset('filemanager/userfiles/' . $file))[0] : env('IMAGE_THUMB_HEIGHT');
        $height = isset(getimagesize(\URL::asset('filemanager/userfiles/' . $file))[1]) ? getimagesize(\URL::asset('filemanager/userfiles/' . $file))[1] : env('IMAGE_THUMB_HEIGHT');
        $height = $height / ($widthImg / $width);
    }

    \Image::make(base_path() . '/public/filemanager/userfiles/' . $file)->resize($width, $height)->save(base_path() . '/public/filemanager/userfiles/' . '_thumbs/' . $reFile);
    return $file;
}

function saveImage($file, $path)
{
    if (!is_string($file)) {
        $file_name = $file->getClientOriginalName();
        $file_name_insert = date('s_i_') . $file_name;
        $file->move(base_path() . '/public/filemanager/userfiles/' . $path, $file_name_insert);
        return $path . '/' . $file_name_insert;
    } else {
        $file_name_insert = date('s_i_') . rand(1, 100) . '_image_crawl.jpg';
        $data = file_get_contents($file);
        file_put_contents(base_path() . '/public/filemanager/userfiles/' . $path . '/' . $file_name_insert, $data);
        return $path . '/' . $file_name_insert;
    }
}

function showImage($src)
{
    if ($src != '') {
        ?>
        <img src="<?php echo getUrlImageThumb($src, env('IMAGE_THUMB_WIDTH'), 'auto');?>"
             class="image-max-width">
        <?php
    } else {
        ?>
        <img src="http://<?php echo $_SERVER['HTTP_HOST']?>/filemanager/userfiles/icon.ico"
             class="image-max-width">
        <?php
    }
}

function getUrlImageThumb($file, $width, $height)
{
    $filePath = str_replace('.jpg', '_' . $width . 'x' . $height . '.jpg', str_replace('.png', '_' . $width . 'x' . $height . '.png', str_replace('.gif', '_' . $width . 'x' . $height . '.gif', $file)));
    return \URL::asset('filemanager/userfiles/_thumbs/' . $filePath);
}

function coverToSlug($string)
{
    $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
        "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
        "ế", "ệ", "ể", "ễ",
        "ì", "í", "ị", "ỉ", "ĩ",
        "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
        "ờ", "ớ", "ợ", "ở", "ỡ",
        "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
        "ỳ", "ý", "ỵ", "ỷ", "ỹ",
        "đ",
        "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
        "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
        "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
        "Ì", "Í", "Ị", "Ỉ", "Ĩ",
        "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
        "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
        "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
        "Đ", " ", "'");

    /*Mảng chứa tất cả ký tự không dấu tương ứng với mảng $marTViet bên trên*/
    $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
        "a", "a", "a", "a", "a", "a",
        "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
        "i", "i", "i", "i", "i",
        "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
        "o", "o", "o", "o", "o",
        "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
        "y", "y", "y", "y", "y",
        "d",
        "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
        "A", "A", "A", "A", "A",
        "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
        "I", "I", "I", "I", "I",
        "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
        "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
        "Y", "Y", "Y", "Y", "Y",
        "D", "-", "-");
    return str_replace($marTViet, $marKoDau, $string);
}