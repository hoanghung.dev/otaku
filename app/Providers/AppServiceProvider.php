<?php

namespace App\Providers;

use App\Helpers\ImageHelper;
use App\Models\Address;
use App\Models\Category;
use App\Models\Post;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $menu = Category::where('type', 1)->where('parent_id', 0)->orderBy('order_no', 'asc')->get();
        view()->share('menu', $menu);
        
        $setting = Setting::first();
        view()->share('setting', $setting);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\User\UserRepository', 'App\Repositories\User\EloquentUser');
        $this->app->bind('App\Repositories\Category\CategoryRepository', 'App\Repositories\Category\EloquentCategory');
        $this->app->bind('App\Repositories\Post\PostRepository', 'App\Repositories\Post\EloquentPost');
        $this->app->bind('App\Repositories\Product\ProductRepository', 'App\Repositories\Product\EloquentProduct');
        $this->app->bind('App\Repositories\Tag\TagRepository', 'App\Repositories\Tag\EloquentTag');
        $this->app->bind('App\Repositories\Sale\SaleRepository', 'App\Repositories\Sale\EloquentSale');
        $this->app->bind('App\Repositories\Manufacturer\ManufacturerRepository', 'App\Repositories\Manufacturer\EloquentManufacturer');
        $this->app->bind('App\Repositories\Madein\MadeinRepository', 'App\Repositories\Madein\EloquentMadein');
        $this->app->bind('App\Repositories\Bill\BillRepository', 'App\Repositories\Bill\EloquentBill');
        $this->app->bind('App\Repositories\Order\OrderRepository', 'App\Repositories\Order\EloquentOrder');
        $this->app->bind('App\Repositories\CategoryManufacturer\CategoryManufacturerRepository', 'App\Repositories\CategoryManufacturer\EloquentCategoryManufacturer');
        $this->app->bind('App\Repositories\ProductRelate\ProductRelateRepository', 'App\Repositories\ProductRelate\EloquentProductRelate');
    }
}
