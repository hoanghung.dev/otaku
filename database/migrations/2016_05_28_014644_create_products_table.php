<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('code', 15);
            $table->integer('category_id');
            $table->string('category_orther_id');
            $table->integer('base_price');
            $table->integer('final_price');
            $table->integer('manufacturer_id');
            $table->integer('madein_id');
            $table->integer('guarantee');
            $table->integer('sale_id');
            $table->integer('tax_vat');
            $table->integer('capacity');
            $table->string('material');
            $table->string('size');
            $table->string('intro');
            $table->text('content');
            $table->string('image');
            $table->string('image_extra_1');
            $table->string('image_extra_2');
            $table->string('image_extra_3');
            $table->string('image_extra_4');
            $table->string('status', 10);
            $table->string('seo_title');
            $table->string('seo_description');
            $table->string('seo_keyword');
            $table->integer('seo_level');
            $table->integer('user_id');
            $table->integer('view_total');
            $table->dateTime('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
