<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('category_id');
            $table->string('intro');
            $table->string('content');
            $table->string('image');
            $table->string('status');
            $table->string('seo_title');
            $table->string('seo_description');
            $table->string('seo_keyword');
            $table->integer('seo_level');
            $table->integer('user_id');
            $table->integer('view_total');
            $table->dateTime('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
