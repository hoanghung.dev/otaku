<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 14:04
 */
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{

    public function run()
    {
        /* Seed article */
        /*echo "* START SEEDING 5 USER...\n";
        $user = factory(\App\Models\User::class, 5)->create()->each(function($user) {
            echo "* USER_ID = " . $user->id . " . START SEDDING 3 CATEGORY...\n";
            $category = factory(\App\Models\Category::class, 3)->create(["user_id"=>$user->id, "type"=>0])->each(function($category) use($user){
                echo " => CATEGORY_ID = " . $category->id . "\n";
                echo "     => SEEDING 30 POSTS";
                $category->posts()->saveMany($posts = factory(\App\Models\Post::class, 50)->create(["user_id"=>$user->id, "category_id"=>$category->id])->each(function(){
                    echo " . ";
                }));
                echo "DONE!\n";
            });
        });*/

        /* Seeder product */

        $category_product = array('Bếp nấu', 'Máy hút mùi', 'Lò nướng - Vi sóng', 'Máy rửa bát - Máy sấy', 'Chậu - Vòi rửa bát', 'Điện máy', 'Đồ gia dụng', 'Phụ kiện tủ bếp');
        $category_product_child = array('Cổ điển', 'Hiện đại', 'Hồng ngoại', 'Độc lập', 'Âm tủ');
        $code = array('6OP36X', '0H16X', 'RH946X', 'ORK36X', '6LPE6X', 'SOK36X', 'PL93EF', 'MKC36X', '6EFH6X', 'EF9WGS');

        echo "* START SEDDING CATEGORY...\n";
        foreach ($category_product as $item) {
            $category = factory(\App\Models\Category::class, 1)->create(["type" => 1, "name" => $item])->each(function ($category) use ($category_product_child, $code) {
                echo "     => SEEDING CATEGORY CHILDS\n";
                foreach ($category_product_child as $item2) {
                    $category_child = factory(\App\Models\Category::class, 1)->create(["type" => 1, "name" => $category->name . ' ' . $item2, 'parent_id' => $category->id])->each(function ($category_child) use ($category, $code) {
                        echo "          => SEEDING 10 PRODUCT\n";

                        $rand_key = array_rand($code, 2);

                        $category->products()->saveMany($products = factory(\App\Models\Product::class, 10)->create(["user_id" => rand(1, 5), "name" => $category->name . ' ' . $code[$rand_key[0]], "category_id" => $category_child->id, "category_orther_id" => '{"category_id":"' . $category->id . '"}'])->each(function () {
                        }));
                        echo "            DONE 10 PRODUCT!\n";
                    });
                    echo "     CATEGORY CHILD DONE \n";
                }
                echo "CATEGORY PARENT DONE \n";
            });
        }

        /*$category_product = array(243=>'Bếp nấu', 244=>'Máy hút mùi', 245=>'Lò nướng - Vi sóng', 246=>'Máy rửa bát - Máy sấy', 247=>'Chậu - Vòi rửa bát', 248=>'Điện máy', 249=>'Đồ gia dụng', 250=>'Phụ kiện tủ bếp');
        $category_product_child = array('Cổ điển', 'Hiện đại', 'Hồng ngoại', 'Độc lập', 'Âm tủ');
        echo "* START SEDDING PRODUCTS...\n";
        foreach($category_product as $key => $item) {
            $rand_key = array_rand($code,2);
            $products = factory(\App\Models\Product::class, 30)->create(["user_id" => rand(1,5), "name" => $item . ' ' . $code[$rand_key[0]], "category_orther_id" => '{"category_id":"'.$key.'"}'])->each(function () {});
        }*/
    }
}
