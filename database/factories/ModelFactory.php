<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*Article*/

$factory->define(\App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'address' => $faker->address,
        'tel' => $faker->phoneNumber,
        'fb_id' => str_random(10),
        'status' => $faker->randomElement(array('active', 'deactive')),
        'remember_token' => str_random(10)
    ];
});

$factory->define(\App\Models\Category::class, function(\Faker\Generator $faker) {
    return [
        /*'name' => $faker->jobTitle,*/
        'slug' => $faker->slug(10),
        'intro' => $faker->paragraph(20),
        'image' => $faker->randomElement(array('category.jpg', 'category2.jpg', 'category3.jpg', 'category4.jpg', 'category5.jpg', 'category6.jpg', 'category7.jpg')),
        /*'parent_id' => rand(1, 3),*/
        'status' => $faker->randomElement(array('publish')),
        'order' => rand(1,10)
    ];
});

$factory->define(\App\Models\Post::class, function(\Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug(10),
        'intro' => $faker->paragraph(20),
        'content' => $faker->paragraph(100),
        'image' => $faker->imageUrl(),
        'status' => $faker->randomElement(array('publish', 'draf', 'deleted')),
        'seo_title' => $faker->name(10),
        'seo_description' => $faker->name(10),
        'seo_keyword' => str_random(5) . ',' . str_random(5) . ',' .str_random(5),
        'seo_level' => rand(1,4),
        'view_total' => $faker->randomNumber(3)
    ];
});

/*Product*/

$factory->define(\App\Models\Product::class, function(\Faker\Generator $faker) {
    return [
        'slug' => $faker->slug(10),
        'intro' => $faker->paragraph(20),
        'content' => $faker->paragraph(50),
        'image' => $faker->randomElement(array('product.jpg', 'product2.jpg', 'product3.jpg', 'product4.jpg', 'product5.jpg', 'product6.jpg', 'product7.jpg')),
        'status' => $faker->randomElement(array('publish')),
        'base_price' => $faker->randomElement(array(15000000, 17000000, 16000000, 15600000, 16300000, 17300000, 15800000, 16100000, 16600000)),
        'final_price' => $faker->randomElement(array(14000000, 13000000, 14000000, 14600000, 12300000, 14300000, 13800000, 15100000, 13600000)),
        'code' => $faker->randomElement(array('6OP36X', '0H16X', 'RH946X', 'ORK36X', '6LPE6X', 'SOK36X', 'PL93EF', 'MKC36X', '6EFH6X', 'EF9WGS')),
        'manufacturer_id' => rand(1,5),
        'sale_id' => rand(1,3),
        'user_id'   => rand(1,5),
        'seo_title' => $faker->name(10),
        'seo_description' => $faker->name(10),
        'seo_keyword' => str_random(5) . ',' . str_random(5) . ',' .str_random(5),
        'seo_level' => rand(1,4),
        'tax_vat' => rand(0,1),
        'view_total' => $faker->randomNumber(3)
    ];
});

/*$factory->define(\App\Models\Product::class, function(\Faker\Generator $faker) {
    return [
        'slug' => $faker->slug(10),
        'intro' => $faker->paragraph(20),
        'content' => $faker->paragraph(50),
        'image' => $faker->randomElement(array('product.jpg', 'product2.jpg', 'product3.jpg', 'product4.jpg', 'product5.jpg', 'product6.jpg', 'product7.jpg')),
        'status' => $faker->randomElement(array('publish')),
        'category_id' => rand(251, 260),
        'base_price' => $faker->randomElement(array(15000000, 17000000, 16000000, 15600000, 16300000, 17300000, 15800000, 16100000, 16600000)),
        'final_price' => $faker->randomElement(array(14000000, 13000000, 14000000, 14600000, 12300000, 14300000, 13800000, 15100000, 13600000)),
        'code' => $faker->randomElement(array('6OP36X', '0H16X', 'RH946X', 'ORK36X', '6LPE6X', 'SOK36X', 'PL93EF', 'MKC36X', '6EFH6X', 'EF9WGS')),
        'manufacturer_id' => rand(1,8),
        'sale_id' => rand(1,3),
        'user_id'   => rand(1,5),
        'seo_title' => $faker->name(10),
        'seo_description' => $faker->name(10),
        'seo_keyword' => str_random(5) . ',' . str_random(5) . ',' .str_random(5),
        'seo_level' => rand(1,4),
        'tax_vat' => rand(0,1),
        'view_total' => $faker->randomNumber(3)
    ];
});*/