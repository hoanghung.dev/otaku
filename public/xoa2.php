<!DOCTYPE html>
<html lang="vi">
<head>
    <title>Bài thực hành số 1</title>
    <meta charset="utf 8"/>
    <link rel="style sheet" href="sty.css"/>
</head>
<body>
<div id="a">
    <h1>Tam tấu</h1>
</div>
<div>
    <h2>Tác giả: Trịnh Hoài Giang</h2>
</div>
<div>
    <p>
        Cũng là sông cả đó thôi <br/>
        Em và tam bạc với tôi đọc hành <br/>
        Dòng sông vận khúc ruột mình <br/>
        Hạt phù sa cũng nghĩa tình nước non <br/>
    </p>
</div>
<div>
    <p>
        Lắng nghe một tiếng chon von <br/>
        Đò ai sóng vỗ đập mòn bóng đêm <br/>
        Em nâng ngọn bấc trong tim <br/>
        Đôi tay gò cố gọi tìm đò ơi <br/>
    </P>
</div>
<hr/>
<div>
    <p>
        Cũng là sóng cả đó thôi <br/>
        Em và Tam bạc, và tôi, đồng hành <br/>
        Dòng sông vận khúc một mình <br/>
        Đò ơi! Có nghĩa, có tình thì thưa.
    </p>
</div>
</body>
</html>