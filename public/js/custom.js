/**
 * Created by hoanghung on 16/05/2016.
 */
// Reset button
var domain = location.hostname;
var href = window.location.href;

function resetForm(element) {
    $(element).find('input').val('');
    $(element).find('textarea').val('');
    $(element).find('.deleteimage').click();
    $(element).find('select').find('option:first').attr('selected', 'selected');
    $(element).find('select').find('option:first').siblings('option').each(function () {
        $(this).removeAttr('selected');
    });
    $('#tags').tagEditor({initialTags: []});
}


$(document).ready(function(){
    // Auto render slug
    $('input[name=name]').keyup(function(){

        var str = $(this).val();
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");
        str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:| |\&|~/g,"-");
        str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
        str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
        $('input[name=slug]').val(str);
    });
    
});

/*filemanager*/
var urlobj;

function BrowseServer(obj)
{
    var domain = location.hostname;
    urlobj = obj;
    OpenServerBrowser(
        'http://'+domain+'/public/filemanager/show',
        screen.width * 0.7,
        screen.height * 0.7 ) ;
}

function OpenServerBrowser( url, width, height )
{
    var iLeft = (screen.width - width) / 2 ;
    var iTop = (screen.height - height) / 2 ;
    var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes" ;
    sOptions += ",width=" + width ;
    sOptions += ",height=" + height ;
    sOptions += ",left=" + iLeft ;
    sOptions += ",top=" + iTop ;
    var oWindow = window.open( url, "BrowseWindow", sOptions ) ;
}

function SetUrl( url, width, height, alt )
{
    document.getElementById(urlobj).value = url ;
    oWindow = null;
}