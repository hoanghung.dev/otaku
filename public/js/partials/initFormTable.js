/**
 * Created by hoanghung on 11/06/2016.
 */
$('.checkbox-master').click(function () {

    if ($(this).is(':checked')) {
        $('table input[type=checkbox]').attr('checked', 'checked');
    } else {
        $('table input[type=checkbox]').removeAttr('checked');
    }
});