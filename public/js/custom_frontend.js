/**
 * Created by hoanghung on 16/05/2016.
 */
var domain = location.hostname;
var href = window.location.href;

// Reset button
function resetForm(element) {
    $(element).find('input').val('');
    $(element).find('textarea').val('');
    $(element).find('select').find('option:first').attr('selected', 'selected');
    $(element).find('select').find('option:first').siblings('option').each(function () {
        $(this).removeAttr('selected');
    });
}

$(".rectangle").click(function () {
    $('.menu-left').toggleClass('active_menu');
    $('body').toggleClass('active_main');
});

$('.keyword').keydown(function (e) {
    if (e.keyCode == 13) {
        $('.search').click();
    }
});

$('.search').click(function () {
    var keyword = $('.keyword').val();
    if (keyword.length > 0) {
        var domain = location.hostname;
        location.href = 'http://' + domain + '/tim-kiem/' + keyword;

    } else {
        alert('Vui lòng nhập từ khóa để tìm kiếm!');
    }
});

$('.menu-left ul li').each(function () {
    $(this).click(function () {
        $(this).find('ul').slideToggle();
    });
});