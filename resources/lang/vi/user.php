<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 24/07/2016
 * Time: 13:30
 */

return [
    'name'      => 'Tên',
    'sex'       => 'Giới tính',
    'birthday'  => 'Ngày sinh',
    'role_user' => 'Quyền',
    'tel'       => 'SĐT',
    'password'  => 'Mật khẩu',
    'address'   => 'Địa chỉ'
];