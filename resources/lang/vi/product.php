<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 28/05/2016
 * Time: 15:56
 */
return [
    'view_more'     => 'Xem thêm',
    'compare'       => 'So sánh',
    'map'           => 'Map',
    'product_category'=> 'Danh mục SP',
    'category_orther'=> 'Danh mục khác',

    'list_product'  => 'Danh sách sản phẩm',
    'new_product'   => 'Sản phẩm mới',
    'product'       => 'Sản phẩm',
    'name'          => 'Tên',
    'code'          => 'Mã',
    'sale'          => 'Sale',
    'sale_description'=> 'Sale description',
    'tax_vat'       => 'thuế VAT',
    'product_info'  => 'Thông tin sản phẩm',
    'base_price'    => 'Giá gốc',
    'final_price'   => 'Giá cuối',
    'souther_price' => 'Giá miền nam',
    'guarantee'     => 'Bảo hành',
    'madein'        => 'Made in',
    'capacity'      => 'Công suất',
    'material'      => 'Chất liệu',
    'size'          => 'Kick thước',
    'seo_description'=> 'Seo description',
    'manufacturers' => 'Hãng sản xuất',
    'manufacturer'  => 'Hãng sản xuất',
    'total_price'   => 'Tổng giá',
    'receipt_method'=> 'Receipt Method',
    'date'          => 'Date',
    'status'        => 'Trạng thái',
    'address'       => 'Địa chỉ',
    'intro'         => 'Mô tả',
    'slider'        => 'Slides',
    'order'         => 'Order',
    'order_info'    => 'Order Infomation',
    'new_orders'    => 'Đơn hàng',

    'id'            => 'Prodcut id',
    'customer_info' => 'Thông tin khách hàng',
    'list_bill'     => 'Danh sách hóa đơn',
    'sex'           => 'Giới tính',
    'tel'           => 'Điện thoại',
    'email'         => 'Email',
    'price'         => 'Giá',
    'value'         => 'Số lượng',
    'no_category'   => 'Không danh mục',
    'no_manufacturer'=> 'Không hãng',
    'type'          => 'Loại',
    'list_sale'     => 'Danh sách sale',
    'list_madein'   => 'Danh sách hãng sản xuất',
    'list_manufacturer'=> 'List Manufacturer',

    'revenue'       => 'Lợi nhuận',

    'product_relate'=> 'Sản phẩm LQ',
    'category_included'=> 'Danh mục liên quan',
    'category_selling'=> 'Danh mục giảm giá',
    'category_sale'  => 'Danh mục bán chạy',

    'list_slider'   => 'Danh sách slider',
    'export'        => 'Export sản phẩm',
];