<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 10/06/2016
 * Time: 09:20
 */
return [
    'status'    => 'Trạng thái',
    'publish'   => 'Duyệt',
    'draf'      => 'Xóa',
    'export'    => 'Export bài viết',
];