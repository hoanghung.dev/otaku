<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 21:48
 */
return [
    'dashboard' => 'Dashboard',
    'site_name' => 'Tên trang',
    'site_offline'  => 'Site offline',
    'cache'     => 'Cache',
    'cache_time'=> 'Cache time',
    'site_offline_message'=> 'Site offline message',
    'meta_description'=> 'Meta description',
    'meta_keywords'=> 'Meta keywords',
    'robots'    => 'Robots',
    'language'  => 'Ngôn ngữ',
    'send_mail' => 'Gửi mail',
    'mail_from' => 'Mail from',
    'mail_name' => 'Mail name',

    'add'       => 'Thêm mới',
    'add_something'=> 'Add New :something',
    'view_all'  => 'Xem thêm',

    'post'      => 'Bài viết',
    'list_post' => 'Danh sách bài viết',
    'post_info' => 'Thông tin bài viết',

    'no_parent'  => 'Không cha',
    'category'  => 'Danh mục',
    'category_info'=> 'Thông tin danh mục',
    'list_category'  => 'Danh sách danh mục',

    'tags'      => 'Tags',

    'tag'       => 'Tag',

    'user'      => 'User',

    'count'     => 'Count',

    'actions'   => 'Hành động',
    'status'    => 'Trạng thái',

    'list_user' => 'Danh sách người dùng',

    'ajax_create_error' => 'Ajax create error',
    'ajax_edit_error' => 'Ajax edit error',
    'ajax_delete_error' => 'Ajax delete error',

    'please_select_category' => 'Please select category!',
    'please_select_post'     => 'Please select post!',

    'view'      => 'Xem',
    'view_more' => 'Xem thêm',

    'settings'  => 'Settings',
    'logout'    => 'Logout',
    'hotline'   => 'Hotline',
    'copyright' => 'Copyright',
    
];