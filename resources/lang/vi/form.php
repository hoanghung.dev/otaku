<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 23:16
 */
return [
    'name'      => 'Tên',
    'name_des'      => 'The name is how it appears on your site.',
    'slug'      => 'Slug',
    'slug_des'  => 'The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.',
    'description'=> 'Mô tả',
    'description_des' => 'The description is not prominent by default; however, some themes may show it.',
    'content'   => 'Nội dung',
    'publish'   => 'Duyệt',
    'category_parent'  => 'Mục cha',
    'category_des' => 'Categories,a unlike tags, can have a hierarchy. You might have a Jazz category, and under that have children categories for Bebop and Big Band. Totally optional.',
    'seo'       => 'SEO',
    'seo_title' => 'Seo title',
    'seo_description' => 'Seo description',
    'seo_keyword' => 'Seo keyword',
    'author'    => 'Tác giả',

    'btn_save'  => 'Lưu',
    'btn_create'  => 'Thêm',
    'btn_edit'  => 'Sửa',
    'btn_reset' => 'Reset',
    'btn_export'=> 'Export',
    'btn_import'=> 'Import',

    'created'   => 'Đã thêm',
    'created_at'   => 'Tạo lúc',
    'create_error'   => 'Create error',

    'edited'    => 'Đã sửa',
    'edit_error'    => 'Lỗi sửa',

    'delete'    => 'Xóa',
    'deleted'   => 'Đã xóa',
    'delete_error'=> 'Delete error',

    'image'     => 'Ảnh',
    'view'      => 'Xem',

    'all_date'  => 'All date',
    'all_seo'   => 'All SEO',
    'all_category'=> 'Tất cả danh mục',

    'btn_add'   => 'Add',
    'btn_cancel'=> 'Cancel',

    'tel'       => 'Tel',
    'role_user' => 'Quyền',
    'username'  => 'Username',
    'password'  => 'Password',
    'avatar'    => 'Avatar',
    'email'     => 'Email',

    'user_profile'=> 'User Profile',
    
    'export_import'=> 'Export & Import'
];