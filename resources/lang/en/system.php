<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 21:48
 */
return [
    'dashboard' => 'Dashboard',
    'site_name' => 'Site name',
    'site_offline'  => 'Site offline',
    'cache'     => 'Cache',
    'cache_time'=> 'Cache time',
    'site_offline_message'=> 'Site offline message',
    'meta_description'=> 'Meta description',
    'meta_keywords'=> 'Meta keywords',
    'robots'    => 'Robots',
    'language'  => 'Language',
    'send_mail' => 'Send mail',
    'mail_from' => 'Mail from',
    'mail_name' => 'Mail name',

    'btn_add'       => 'Add New',
    'btn_cancel'    => 'Cancel',
    'add_something'       => 'Add New :something',
    'view_all'  => 'View all',

    'post'      => 'Post',
    'list_post' => 'List Post',
    'main_info' => 'Main Infomation',

    'no_parent'  => 'No parent',
    'category'  => 'Category',
    'category_info'=> 'Category Infomation',
    'list_category'  => 'List Category',

    'tags'      => 'Tags',

    'tag'       => 'Tag',

    'user'      => 'User',

    'count'     => 'Count',

    'list'      => 'List',
    
    'name'      => 'Name',
    'slug'      => 'Slug',
    
    'actions'   => 'Actions',
    'status'    => 'Status',
    
    'create'    => 'Create',
    'edit'      => 'Edit',

    'list_user' => 'List user',

    'ajax_create_error' => 'Ajax create error',
    'ajax_edit_error' => 'Ajax edit error',
    'ajax_delete_error' => 'Ajax delete error',

    'please_select_category' => 'Please select category!',
    'please_select_post'     => 'Please select post!',

    'view'      => 'View',
    'view_more' => 'View more',

    'settings'  => 'Settings',
    'logout'    => 'Logout',
    'hotline'   => 'Hotline',
    'copyright' => 'Copyright',
    
];