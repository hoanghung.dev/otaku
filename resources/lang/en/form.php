<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 23:16
 */
return [
    'name'      => 'Name',
    'name_des'      => 'The name is how it appears on your site.',
    'slug'      => 'Slug',
    'slug_des'  => 'The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.',
    'description'=> 'Description',
    'description_des' => 'The description is not prominent by default; however, some themes may show it.',
    'content'   => 'Content',
    'publish'   => 'Publish',
    'category_parent'  => 'Parent Category',
    'category_des' => 'Categories, unlike tags, can have a hierarchy. You might have a Jazz category, and under that have children categories for Bebop and Big Band. Totally optional.',
    'seo'       => 'SEO',
    'seo_title' => 'Seo title',
    'seo_description' => 'Seo description',
    'seo_keywords' => 'Seo keywords',
    'author'    => 'Author',

    'btn_save'  => 'Save',
    'btn_create'  => 'Create',
    'btn_edit'  => 'Update',
    'btn_reset' => 'Reset',

    'created'   => 'Created!',
    'created_at'   => 'Created at',
    'create_error'   => 'Create error',

    'edited'    => 'Edited',
    'edit_error'    => 'Edit error',

    'delete'    => 'Delete',
    'deleted'   => 'Deleted',
    'delete_error'=> 'Delete error',

    'image'     => ' Image',
    'view'      => 'View',

    'all_date'  => 'All date',
    'all_seo'   => 'All SEO',
    'all_category'=> 'All category',

    'btn_add'   => 'Add',
    'btn_cancel'=> 'Cancel',

    'tel'       => 'Tel',
    'role_user' => 'Role',
    'username'  => 'Username',
    'password'  => 'Password',
    'avatar'    => 'Avatar',
    'email'     => 'Email',

    'user_profile'=> 'User Profile',
    
];