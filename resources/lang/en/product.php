<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 28/05/2016
 * Time: 15:56
 */
return [
    'view_more'     => 'View more',
    'compare'       => 'Compare',
    'map'           => 'Map',
    'product_category'=> 'Product Category',
    'category_orther'=> 'Category orther',

    'list_product'  => 'List Product',
    'new_product'   => 'New product',
    'product'       => 'Product',
    'name'          => 'Name',
    'code'          => 'Code',
    'sale'          => 'Sale',
    'sale_description'=> 'Sale description',
    'tax_vat'       => 'Tax VAT',
    'product_info'  => 'Product Infomation',
    'base_price'    => 'Base price',
    'final_price'   => 'Final price',
    'souther_price' => 'Souther price',
    'guarantee'     => 'Guarantee',
    'madein'        => 'Made in',
    'capacity'      => 'Capacity',
    'material'      => 'Material',
    'size'          => 'Size',
    'seo_description'=> 'Seo description',
    'manufacturers' => 'Manufacturers',
    'manufacturer'  => 'Manufacturer',
    'total_price'   => 'Total price',
    'receipt_method'=> 'Receipt Method',
    'date'          => 'Date',
    'status'        => 'Status',
    'address'       => 'Address',

    'order'         => 'Order',
    'order_info'    => 'Order Infomation',
    'new_orders'    => 'New Orders',

    'id'            => 'Prodcut id',
    'customer_info' => 'Customer Infomation',
    'list_bill'     => 'List bill',
    'sex'           => 'Sex',
    'tel'           => 'Tel',
    'email'         => 'Email',
    'price'         => 'Price',
    'value'         => 'Value',
    'no_category'   => 'No category',
    'no_manufacturer'=> 'No manufacturer',
    'type'          => 'Type',
    'list_sale'     => 'List sale',
    'list_madein'   => 'List Made in',
    'list_manufacturer'=> 'List Manufacturer',

    'revenue'       => 'Revenue',

    'product_relate'=> 'Product Relate',
    'category_included'=> 'Category Included',
    'category_selling'=> 'Category Selling',
    'category_sale'  => 'Category Sale',
    
    'slider'        => 'Slider',
    'export_import' => 'Export/Import',
    
    'seo_keywords'  => 'Seo keywords',
];