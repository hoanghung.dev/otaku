@if(!isset($pageOption))
    <?php
    $pageOption = [
            'type'      => 'page',
            'parentUrl' => $setting->site_name,
            'parentName' => 'Trang chủ',
            'pageName'  => 'Không tìm thấy trang',
            'link'      => "http://".$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
    ];
    ?>
@endif

<div class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ URL::to('/') }}">Trang chủ</a></li>
            @if($pageOption['type'] != 'page')
                <li><a href="{{ URL::to($pageOption['parentUrl']) }}">{{ $pageOption['parentName'] }}</a></li>
            @endif
            <li class="active">{{ $pageOption['pageName'] }}</li>
        </ol><!-- /.breadcrumb -->

    </div><!-- /.container -->
</div>