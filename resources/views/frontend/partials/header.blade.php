<div id="header-wrapper">
    <div id="header">
        <div id="header-inner">
            <nav class="navbar navbar-default">
                <div class="header-secondary">
                    <div class="container clearfix">
                        Select Language
                        <ul class="languages">
                            <li>
                                <a href="{{ URL::to('/') }}#"
                                   class="language-active">
                                    <img class="iclflag" src="{{ URL::asset('images/vi.png') }}" alt="en"
                                         title="VietNam">
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/') }}#">
                                    <img class="language-flag" src="{{ URL::asset('images/en.png') }}" alt="fr"
                                         title="English">
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/') }}#">
                                    <img class="language-flag" src="{{ URL::asset('images/de.png') }}" alt="de"
                                         title="Japan">
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/') }}#">

                                    <img class="language-flag" src="{{ URL::asset('images/it.png') }}" alt="it"
                                         title="Korea">
                                </a>
                            </li>
                        </ul><!-- /.languages -->

                        <ul class="header-submenu pull-right">
                            <li><a href="{{ URL::to('register') }}"><i
                                            class="fa fa-users color-gray-dark"></i> <span
                                            class="hidden-xs">Đăng ký</span></a>
                            </li>
                            <li><a href="{{ URL::to('login') }}"><i
                                            class="fa fa-sign-in color-gray-dark"></i> <span
                                            class="hidden-xs">Đăng nhập</span></a></li>
                        </ul>
                    </div><!-- /.container -->
                </div><!-- /.header-secondary -->

                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#navbar-main">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand"
                           href="{{ URL::to('/') }}">
                                <span class="logo-styled">
                                    <span class="logo-title">
                                        <img src="{{ URL::asset('images/icon.ico') }}" alt="{{ $setting->site_name }}"
                                             style="width:50px;"> OTAKU
                                    </span><!-- /.logo-title -->
                                    <span class="logo-subtitle hidden-sm">Tìm nhà <br> Quanh đây</span>
                                </span><!-- /.logo-styled -->
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="navbar-main">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="menuparent hidden-sm">
                                <a href="{{ URL::to('/') }}">Trang chủ</a>
                            </li>


                            <li class="menuparent">
                                <a href="{{ URL::to('tin-tuc') }}">Tin tức</a>
                            </li>
                            <li class="menuparent megamenu columns4">
                                <a href="{{ URL::to('dang-tin') }}">Đăng tin</a>
                            </li>

                            <li class="menuparent megamenu columns3">
                                <a href="{{ URL::to('tai-ung-dung') }}">Đăng ký nhận tin</a>
                            </li>

                            <li class="menuparent">
                                <a href="{{ URL::to('gioi-thieu') }}">Giới thiệu</a>
                            </li>
                            <li class="menuparent">
                                <a href="{{ URL::to('lien-he') }}">Liên hệ</a>
                            </li>
                        </ul><!-- /.nav -->                        </div><!-- /.navbar-collapse -->
                </div>
            </nav>
        </div><!-- /#header-inner -->
    </div><!-- /#header -->
</div>

