<div id="footer-wrapper">
    <div id="footer">
        <div id="footer-inner">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="widget">
                                <h3>Giới thiệu</h3>

                                <div class="list-group clean">
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">Terms &amp; Conditions</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">Advertisment</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">Press Information</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">Our Team</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="widget">
                                <h3>Hỗ trợ</h3>

                                <div class="list-group clean">
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">Tutorials</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">FAQ</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">Handbook</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item">API</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="widget">
                                <h3>Danh mục</h3>
                                <?php $listItem = \App\Models\Category::where('status', 1)->where('type', 1)->orderBy('order_no', 'asc')->get();?>
                                @if(count($listItem) > 0)
                                    <div class="list-group clean">
                                        @foreach($listItem as $item)
                                            <a href="{{ URL::to($item->slug) }}"
                                               class="list-group-item">{{ $item->name }}</a>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="widget">
                                <h3>Ứng dụng</h3>

                                <div class="list-group clean">
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item"><img
                                                src="{{ URL::asset('filemanager/userfiles/data/en.png') }}" alt="en"
                                                title="English"> Adroid</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item"><img
                                                src="{{ URL::asset('filemanager/userfiles/data/de(1).png') }}"
                                                alt="en"
                                                title="German"> IOS</a>
                                    <a href="{{ URL::to('/') }}#"
                                       class="list-group-item"><img
                                                src="{{ URL::asset('filemanager/userfiles/data/it.png') }}" alt="en"
                                                title="Italian"> Windows phone</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="block-content fullwidth block-content-small-padding background-primary-dark">
                        <div class="block-content-inner">
                            <div class="social-stripe center">
                                <a href="{{ URL::to('/') }}#"
                                   class="facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="dribbble">
                                    <i class="fa fa-dribbble"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="linkedin">
                                    <i class="fa fa-linkedin"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="pinterest">
                                    <i class="fa fa-pinterest"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="dropbox">
                                    <i class="fa fa-dropbox"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="linkedin">
                                    <i class="fa fa-linkedin"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="foursquare">
                                    <i class="fa fa-foursquare"></i>
                                </a>

                                <a href="{{ URL::to('/') }}#"
                                   class="instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </div><!-- /.social-stripe -->
                        </div><!-- /.block-content-inner -->
                    </div><!-- /.block-content -->                                </div><!-- /.container -->
            </div><!-- /.footer-top -->

            <div class="footer-bottom">
                <div class="container">
                    <nav class="clearfix">
                        <ul class="nav navbar-nav footer-nav">
                            <li><a href="#">Quy định bảo mật</a>
                            </li>
                            <li><a href="{{ URL::to('/') }}">Thỏa thuận sử dụng</a>
                            </li>
                            <li><a href="#">Hợp tác</a>
                            </li>
                        </ul><!-- /.nav -->
                    </nav>

                    <div class="copyright">
                        Designed by <a href="http://otaku.vn">Otaku.vn</a> / Assembled in <a
                                href="http://onered.vn/">Onered Mates</a>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.footer-bottom -->
        </div><!-- /#footer-inner -->
    </div><!-- /#footer -->
</div>