<meta charset="utf-8">
<title>@if(isset($pageOption)){{ $pageOption['pageName'] }}@else{!! $setting->site_name !!}@endif</title>
<!-- Meta -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta content="@if(isset($pageOption)){{ $pageOption['pageName'] }}@else{!! $setting->site_name !!}@endif"
      name="title">

<meta content="@if(isset($pageOption['keyword'])){{ $pageOption['keyword'] }}@else{{ $setting->site_name}}@endif"
      name="keywords">

<meta content="@if(isset($pageOption['description']) && $pageOption['description'] != ''){!! $pageOption['description'] !!}@else{!! $setting->meta_description !!}@endif"
      name="description">

<meta content="{{ $setting->robots }}" name="robots">

<link rel="alternate" hreflang="vi" href="{{ URL::to('/') }}" />
<meta property="og:title" content="@if(isset($pageOption)){{ $pageOption['pageName'] }}@else{!! $setting->site_name !!}@endif" />
<meta property="og:description" content="@if(isset($pageOption['description'])){{ $pageOption['description'] }}@else{!! $setting->meta_description !!}@endif" />
<meta property="og:image" content="@if(isset($pageOption['image'])){{ $pageOption['image'] }}@else{{ URL::asset('filemanager/userfiles/' . $setting->logo) }}@endif" />
<meta property="og:url" content="@if(isset($pageOption['link'])){{ $pageOption['link'] }}@else{{ URL::to('/') }}@endif" />
<link rel="canonical" href="{{ URL::to('/') }}"/>
<meta property="og:type" content="website" />
<meta name="author" content="{{ $_SERVER['HTTP_HOST'] }}" />
<meta property="og:site_name" content="https://www.facebook.com/thietbibanle68" />
<meta property="fb:admins" content="hoanghung"/>
<meta property="fb:app_id" content="0987519120"/>


<link rel="shortcut icon" href="{{ URL::asset('filemanager/userfiles/'. $setting->favicon) }}" type="image/x-icon">



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">.gm-style-pbc {
        transition: opacity ease-in-out;
        background-color: rgba(0, 0, 0, 0.45);
        text-align: center
    }

    .gm-style-pbt {
        font-size: 22px;
        color: white;
        font-family: Roboto, Arial, sans-serif;
        position: relative;
        margin: 0;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
    }</style>
<link type="text/css" rel="stylesheet" href="./data_files/css">
<style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
        font-size: 10px
    }</style>
<style type="text/css">@media print {
        .gm-style .gmnoprint, .gmnoprint {
            display: none
        }
    }

    @media screen {
        .gm-style .gmnoscreen, .gmnoscreen {
            display: none
        }
    }</style>
<style type="text/css">.gm-style {
        font-family: Roboto, Arial, sans-serif;
        font-size: 11px;
        font-weight: 400;
        text-decoration: none
    }

    .google-map-filter .form-group > div.chosen-container:nth-child(3) {
        display: none;
    }

    .gm-style img {
        max-width: none
    }</style>

<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="./data_files/css(1)">

<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/chosen.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/ion.rangeSlider.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/ion.rangeSlider.skinNice.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/directory.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/bootstrap.vertical-tabs.min.css') }}">
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/common.js') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/map.js') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/util.js') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/marker.js') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/overlay.js') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/onion.js') }}"></script>
<script type="text/javascript" charset="UTF-8"
        src="{{ URL::asset('filemanager/userfiles/data/ViewportInfoService.GetViewportInfo') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/stats.js') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('frontend/js/controls.js') }}"></script>
<script type="text/javascript" charset="UTF-8"
        src="{{ URL::asset('filemanager/userfiles/data/AuthenticationService.Authenticate') }}"></script>
<script type="text/javascript" charset="UTF-8" src="{{ URL::asset('filemanager/userfiles/data/vt') }}"></script>
<script type="text/javascript" charset="UTF-8"
        src="{{ URL::asset('filemanager/userfiles/data/QuotaService.RecordEvent') }}"></script>

<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.js') }}"></script>


@yield('custom_header')

<link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/custom.css') }}">
