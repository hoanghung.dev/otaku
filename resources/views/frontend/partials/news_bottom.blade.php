<div class="box_tin_tuc_khuyen_mai">
    <span>Tin tức</span>
    <ul>
        <?php $listPost = \App\Models\Post::where('status', 1)->whereIn('category_id', [13,14])->orderBy('id', 'desc')->limit(8)->get();?>
        @if($listPost != null)
            @foreach($listPost as $item)
                <li>
                    @if(is_object($item->category))
                        <a href="{{ URL::to($item->category->slug.'/'.$item->slug) }}"
                           title="{{ $item->name }}"><span> <img
                                        src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                        alt="{{ $item->name }}"
                                        title="{{ $item->name }}"></span>
                            <h2>{{ $item->name }}</h2>
                            <b>{{ date('d/m/Y', strtotime($item->created_at)) }}</b> </a>
                    @endif
                </li>
            @endforeach
        @endif
    </ul>
</div>
<style>
    .hcp_ul_ctn_3 {
        width: 33.333333333333%;
    }
</style>
<script>
    function danhgiatrang(data_type, data_id, type) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if (xmlhttp.responseText !== "")
                    alert(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", "danhgiatrang.php?data_type=" + data_type + "&data_id=" + data_id + "&type=" + type, true);
        xmlhttp.send();
    }
    var jrating = jQuery.noConflict();
    jrating(document).ready(function () {
        jrating(".danhgia_thich").click(function () {
            var data_type = jrating(this).attr("data-type");
            var data_id = jrating(this).attr("data-id");
            danhgiatrang(data_type, data_id, "like");
        });
        jrating(".danhgia_khongthich").click(function () {
            var data_type = jrating(this).attr("data-type");
            var data_id = jrating(this).attr("data-id");
            danhgiatrang(data_type, data_id, "dislike");
        });
    });
</script>