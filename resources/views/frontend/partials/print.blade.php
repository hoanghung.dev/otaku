<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Báo giá {{ $product->name }}</title>
    <meta http-equiv="Expires" content="Tue, 18 Sep 2012 09:50:33 GMT">
    <meta name="revisit-after" content="1 days">
    <meta name="robots" content="{{ $setting->robots }}">
</head>
<body onload="window.print()">
<style>
    p {
        margin: 0;
        padding: 0
    }

    .manufacturer img {
        max-width: 150px;
    }
</style>
<table border="0" cellpadding="0" style="border-collapse: collapse" width="100%">
    <tbody>
    <tr>
        <td>
            <?php $imageHelper = new \App\Helpers\ImageHelper(); ?>
            <table border="0" cellpadding="0" style="border-collapse: collapse" width="100%">
                <tbody>
                <tr>
                    <td width="22%" align="left" style="border-bottom:#CCCCCC 1px solid"><img
                                src="{{ URL::asset('filemanager/userfiles/' . $setting->logo) }}" width="283"
                                height="186"></td>
                    <td width="78%" align="left" style=" padding-left:24pt; border-bottom:#CCCCCC 1px solid"><b
                                style="font-size:16pt">CÔNG TY CỔ PHẦN GIẢI PHÁP CÔNG NGHỆ WSOFT</b>
                        <p style="font-size:13pt">Địa chỉ: Số 32 Đặng Tiến Đông, Đống Đa, Hà Nội</p>
                        <p style="font-size:13pt">Điện thoại: 0976 366 655</p>
                        <p style="font-size:13pt">Email: sales@wsoft.vn</p></td>
                </tr>
                </tbody>
            </table>
            <br>
            <table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" id="table6">
                <tbody>
                <tr>
                    <td><p align="center"><span style="font-size:24pt;font-family:Arial,Verdana; font-weight:bold"><b>BẢNG BÁO GIÁ SẢN PHẨM </b></span><b><br>
                                <span style="font-size:16pt;font-family:Arial,Verdana">(Tại thời điểm {{ date('H:m:i', time()) }}
                                    Ngày: {{ date('d', time()) }} tháng {{ date('m', time()) }}
                                    năm {{ date('Y', time()) }})</span></b>
                        </p></td>
                </tr>
                </tbody>
            </table>
            <br>
            <p><span style="font-size:14pt;font-family:Arial,Verdana">Kính gửi quý khách báo giá sản phẩm </span></p>
            <p style="color:#FF0000; padding-bottom:15px"><span style="font-size:14pt;font-family:Arial,Verdana; ">Công ty cổ phần giải pháp công nghệ WSOFT là một doanh nghiệp trên  15  năm chuyên cung cấp thiết bị mã số mã vạch và  giải pháp .Với nhiều  sản phẩm chất lượng,mẫu mã phong phú và một đội ngũ nhân viên chuyên nghiệp có kinh nghiệm lâu năm tại Việt Nam.
  Chúng tôi xin trân trọng gửi tới Quý khách hàng bảng báo giá đặc biệt các loại thiết bị mới nhất hiện nay</span>
            </p>
            <table border="1" cellpadding="5" style="border-collapse: collapse" width="100%" id="table8">
                <tbody>
                <tr>

                    <td align="center"><span style="font-size:14pt;font-family:Arial,Verdana"><b>TT</b></span></td>
                    <td align="center"><span style="font-size:14pt;font-family:Arial,Verdana"><b>Chi tiết thiết bị và dịch vụ</b></span>
                    </td>
                    <td align="center"><span style="font-size:14pt;font-family:Arial,Verdana"><b>SL</b></span></td>
                    <td align="center"><span style="font-size:14pt;font-family:Arial,Verdana"><b>Đơn giá VNĐ</b></span>
                    </td>
                    <td align="center"><span
                                style="font-size:14pt;font-family:Arial,Verdana"><b>Thành tiền VNĐ</b></span></td>
                    <td align="center"><span style="font-size:14pt;font-family:Arial,Verdana"><b>Bảo hành</b></span>
                    </td>
                </tr>


                <tr>

                    <td align="center"><span style="font-size:12pt;"><b></b></span></td>
                    <td align="center" style=" width: 65%;"><span
                                style="font-size:16pt;font-weight:bold;">{{ $product->name }}</span></td>
                    <td align="center"><span style="font-size:14pt;">1</span></td>
                    <td align="right"><span style="font-size:14pt;">
                            @if($product->final_price == 0)
                                Liên hệ
                            @else
                                {{ number_format($product->final_price,0,'.','.') }}
                                <i>đ</i>
                            @endif
                        </span></td>
                    <td align="right"><span style="font-size:14pt;">
                            @if($product->final_price == 0)
                                Liên hệ
                            @else
                                {{ number_format($product->final_price,0,'.','.') }}
                                <i>đ</i>
                            @endif
                        </span></td>
                    <td align="right"><span style="font-size:14pt;">{{ $product->guarantee }} Tháng</span>
                    </td>

                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td align="left" style="width:400"><span style="font-size:14pt"><p>
	{!! $product->intro !!}</p></span>
                    </td>
                    <td align="center" colspan="4">
                        <img src="{{ $imageHelper->getUrlImageThumb($product->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                             alt="{{ $product->name }}" width="120" height="120"></td>
                </tr>


                </tbody>
            </table>
            <div align="center" style="font-size:18pt; color:#000080; margin-top:15px">Nhà phân phối các sản phẩm công
                nghệ chính hãng
            </div>
                <div class="manufacturer">
            <?php $manufacturers = \App\Models\Manufacturer::get();?>
            @if(is_object($manufacturers))
                @foreach($manufacturers as $item)
                    <img src="{{ URL::asset('filemanager/userfiles/' . $item->image) }}">
                @endforeach
            @endif
                </div>
            <span style="font-size:14pt;">  Ghi chú: 				<br>
      1.Báo giá có giá trị trong vong 15 ngày	<br>
      2.Tất cả các thiết bị đều mới 100% có phiếu bảo hành hoặc tem của nhà sản xuất <br>
	  3.Tân Phát bảo hành miễn phí các sản phẩm bán ra, cho mượn thiết bị trong quá trinh chờ bảo hành.	<br>
      4.Quý khách vui lòng thanh toán 100% bằng CK ngay sau khi ký đơn hàng<br></span>
            <div align="left" style="font-size:14pt; color:#FF0000"> Đơn vị thụ hưởng :Công ty cổ phần giải pháp công nghệ WSOFT.<br>
            </div>
            <div align="center" style="font-size:18pt; color:#0000ff">Chúng tôi tự hào là nhà cung cấp thiết bị cho
                nhiều hệ thống cửa hàng, siêu thị tại Hà Nội: Siêu thị BigC,Siêu thị Hiway,Siêu thị Ebest,chuỗi cửa hàng
                của Giovanni,thời trang Ivy,Thời trang Fiona…..
            </div>
            <div align="center" style="font-size:18pt; color:#000000;">Xin trân thành cảm ơn sự quan tâm ủng hộ của Quý
                Khách hàng
            </div>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>