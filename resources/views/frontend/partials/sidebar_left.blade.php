<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ URL::to('dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('post') }}"><i class="fa fa-file-o fa-fw"></i> {{ trans('system.post') }}</a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="flot.html">{{ trans('system.add') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('post') }}">{{ trans('system.view_all') }}</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('category') }}"><i class="fa fa-folder-open fa-fw"></i> {{ trans('system.category') }}</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-tags fa-fw"></i> {{ trans('system.tag') }}</a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="flot.html">{{ trans('system.add') }}</a>
                    </li>
                    <li>
                        <a href="morris.html">{{ trans('system.view_all') }}</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> {{ trans('system.user') }}</a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="flot.html">{{ trans('system.add') }}</a>
                    </li>
                    <li>
                        <a href="morris.html">{{ trans('system.view_all') }}</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->