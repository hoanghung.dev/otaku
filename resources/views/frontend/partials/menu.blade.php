

<div class="cd-dropdown-wrapper" style="z-index:99"><a class="cd-dropdown-trigger"
                                                       href="#">Danh mục sản
        phẩm</a>
    <nav class="cd-dropdown">
        <ul class="cd-dropdown-content">
            @foreach($menu as $item)
                <li class="has-children"><a href="{{ URL::to($item->slug) }}">{{ $item->name }}</a>
                    <?php $childs = $item->childs;?>
                    @if(count($childs) > 0)
                        <ul class="cd-secondary-dropdown is-hidden">
                            @foreach($childs as $child)
                                <li class="has-children">
                                    <ul class="is-hidden">
                                        <li class="see-all"><a
                                                    href="{{ URL::to($child->slug) }}">{{ $child->name }}</a></li>
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
        <!-- .cd-dropdown-content -->
    </nav>
    <!-- .cd-dropdown -->
</div>

<div id="search-main">
    <div class="nav-search menu_mobile">
        <div class="toggle">
            <a class="rectangle" href="javascript:void(0);"><img
                        src="{{ URL::asset('images/menu-icon.png') }}" alt=""></a>
        </div>
        <div class="search">
            <div>
                <div class="timkiem_tanphat3"><input type="text" name="keyword" id="keyword" size="20" class="keyword2"
                                                     placeholder="Nội dung tìm kiếm ..."></div>
                <input type="button" class="timkiem_tanphat4" value="Tìm kiếm" name="search" id="search"
                       onclick="return create_seo_search2();">
            </div>
        </div>
    </div>
    <div class="menu-left">
        <div>
            <ul class="">
                @foreach($menu as $item)
                    <li class="">
                        <?php $childs = $item->childs;?>
                        @if(count($childs) > 0)
                            <a href="#">{{ $item->name }}</a>
                        @else
                            <a href="{{ URL::to($item->slug) }}">{{ $item->name }}</a>
                        @endif
                        @if(count($childs) > 0)
                            <ul class="">
                                @foreach($childs as $child)
                                    <li class="">
                                        <a
                                                href="{{ URL::to($child->slug) }}">{{ $child->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>

    </div>

</div>