<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.bxslider.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.isotope.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/ion.rangeSlider.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.flot.canvas.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.flot.resize.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/jquery.flot.time.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/carousel.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/collapse.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/dropdown.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/tab.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/transition.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/graph.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('frontend/js/director.js') }}"></script>
<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('google-map-div'), {
            center: {lat: 21.0434623947563, lng: 105.793354706937},
            zoom: 15
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                map.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBoF2YXlGtPUnvwDaLx7ZVS6fKv_cGDCjg&callback=initMap"
        async defer>
</script>
@yield('custom_footer')