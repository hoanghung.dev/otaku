@extends('frontend.layouts.master')
@section('main_content')

    <div class="container">
        <div class="block-content fullwidth">
            <div class="block-content-inner">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="sidebar">
                            <div class="widget widget-boxed">
                                <h3>Bài mới</h3>

                                <div class="content-rows-small">
                                    <div class="content-row-small">
                                        <div class="content-row-small-body">
                                            <h4><a href="#">Pellentesque mi eros, fermentum vitae fermentum et</a></h4>
                                            <div class="content-row-small-meta">By John Doe / 02 May, 2014</div>
                                            <!-- /.content-row-small-meta -->
                                        </div><!-- /.content-row-body -->
                                    </div><!-- /.content-row-small -->

                                    <div class="content-row-small">
                                        <div class="content-row-small-body">
                                            <h4><a href="#">Pellentesque mi eros, fermentum vitae fermentum et</a></h4>
                                            <div class="content-row-small-meta">By John Doe / 02 May, 2014</div>
                                            <!-- /.content-row-small-meta -->
                                        </div><!-- /.content-row-body -->
                                    </div><!-- /.content-row-small -->
                                </div><!-- /.content-rows-small -->
                            </div><!-- /.widget -->

                            <div class="widget widget-boxed widget-boxed-dark">
                                <h3>Danh mục</h3>

                                <div class="list-group">
                                    <a href="#" class="list-group-item"><i class="fa fa-angle-right"></i> Audi <span
                                                class="badge">1254</span></a>
                                    <a href="#" class="list-group-item"><i class="fa fa-angle-right"></i> Citroen <span
                                                class="badge">35</span></a>
                                    <a href="#" class="list-group-item"><i class="fa fa-angle-right"></i> Opel <span
                                                class="badge">149</span></a>
                                    <a href="#" class="list-group-item"><i class="fa fa-angle-right"></i> Seat <span
                                                class="badge">35</span></a>
                                    <a href="#" class="list-group-item"><i class="fa fa-angle-right"></i> Toyota <span
                                                class="badge">35</span></a>
                                    <a href="#" class="list-group-item"><i class="fa fa-angle-right"></i> Volkswagen
                                        <span class="badge">351</span></a>
                                </div><!-- /.list-group -->
                            </div><!-- /.widget -->
                        </div><!-- /.sidebar -->                </div>

                    <div class="col-sm-9">
                        <div class="page-header center page-header-no-rules">
                            @if(isset($category))
                                <h1>{{$category->name}}</h1>
                                <p>{!! $category->intro !!}</p>
                            @else
                                <h1>{{$keyword}}</h1>
                            @endif
                        </div>
                        <div class="portfolio row">
                            @if(count($listPost) > 0)
                                @foreach($listPost as $item)
                                    <div class="col-sm-4">
                                        <div class="portfolio-item">
                                            <a href="{{$item->category->slug.'/'.$item->slug}}">
                                                <img src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}" alt="{{$item->name}}">
                                                <span><i class="fa fa-link"></i></span>
                                                <h2>{{$item->name}}</h2>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div><!-- /.block-content-inner -->
        </div><!-- /.block-content -->
    </div>

@stop
