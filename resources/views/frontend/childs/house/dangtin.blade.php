@extends('frontend.layouts.master')

@section('main_content')

    <div class="container">
        <div class="block-content">
            <div class="block-content-inner">
                <div class="page-header center">
                    <h1>Đăng tin</h1>
                </div><!-- /.page-header -->

                <div class="row mb40">

                    <div class="col-sm-8">
                        <iframe class="contact-map" width="727" height="425"
                                src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=5th+Avenue,+New+York,+NY,+United+States&amp;aq=0&amp;oq=5th+&amp;sll=37.0625,-95.677068&amp;sspn=70.689889,135.263672&amp;ie=UTF8&amp;hq=&amp;hnear=5th+Ave,+New+York&amp;t=m&amp;z=13&amp;ll=40.649866,-74.005367&amp;output=embed"></iframe>
                    </div>
                </div><!-- /.row -->

                <form method="post" action="?">
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label>Tiêu đề</label>
                            <input type="text" name="name" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Giá</label>
                            <input type="text" name="price" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Diện tích</label>
                            <input type="text" name="city" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Địa chỉ</label>
                            <input type="text" name="address" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Chọn ảnh</label>
                            @include('backend.partials.image_thumb_default', ['name' => 'image_extra'])
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Nội dung</label>
                            <textarea class="form-control" name="content"></textarea>
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Điện thoại liên hệ</label>
                            <input type="text" name="tel" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Người liên hệ</label>
                            <input type="text" name="name" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-xs-12">
                            <label>Chọn loại tin</label>
                            <?php $listItem = \App\Models\Category::where('status', 1)->where('type', 0)->get();?>
                            @if(count($listItem) > 0)
                                <select name="category_id" class="form-control">
                                    @foreach($listItem as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div><!-- /.form-group -->

                    </div><!-- /.row -->


                    <button class="btn btn-secondary" type="button">Đăng tin</button>
                </form>
            </div><!-- /.block-content-inner -->
        </div><!-- /.block-content -->
    </div>

@stop


@section('custom_header')
    <style>
        .block-content {
            width: 727px;
            margin: auto;
        }
    </style>
@stop
