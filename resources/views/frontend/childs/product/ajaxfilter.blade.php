@if($listProduct)
    @foreach($listProduct as $product)
        <p class="priced ">
                <div class="item-content-product">
                    <div class="border-product">
                        <div class="box-product">
                            <div class="image-new">
                                <i class="icon-new"></i>
                            </div>
                            <a href="{{ URL::to($product->category->slug . '/' . $product->slug) }}"><img class="img-content-product lazy" alt="{{ $product->name }}"
                                 src="{{ URL::asset('filemanager/userfiles/' . $product->image) }}"/></a>
                        </div>
        <p class="short-name">
            <span> {{ $product->manufacturer->name }}  </span>
        </p>
        <p class="priceline "><span
                    class="ms-number-money2">{!! number_format($product->base_price,0,",",".") !!}</span> ₫</p>
        <p class="priced "><span class="ms-number-money2">{{ number_format($product->final_price,0,",",".") }}</span> ₫
        </p>
        <p class="full-name"><span class=""><a href="{{ URL::to($product->category->slug . '/' . $product->slug) }}">{{ $product->name }}</a></span></p>

        <p class="icon">
        </p>
        <div class="bginfo">
            <div class="box-attribute">
                {{ $product->intro }}
            </div>
            <div class="box-detail">
                <span class="btnviewdetail"><a href="{{ URL::to($product->category->slug . '/' . $product->slug) }}">Chi tiết</a></span>
                <span class="btnsosanh add-compare add-compare-22" data-id="{{ $product->id }}">So sánh</span>
            </div>
        </div>
        </div>
        </div>
    @endforeach
    {!! $listProduct->links() !!}
@endif
