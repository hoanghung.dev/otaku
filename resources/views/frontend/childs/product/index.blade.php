@extends('frontend.layouts.master')

@section('main_content')

    <div class="boxphai">

        <div class="san_pham">
            <div class="anh_san_pham"><a href="#" rel="nofollow">
                    <img class="image-prd" alt="{{ $product->name }}" title="{{ $product->name }}"
                         src="{{ URL::to('filemanager/userfiles/' . $product->image) }}"></a>
                <ul class="img-relate">
                    @if($product->image != '')
                        <li>
                            <img src="{{ getUrlImageThumb($product->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                 data-action="{{ $product->image }}">
                        </li>
                    @endif

                    @if($product->image_extra_1 != '')
                        <li>
                            <img src="{{ getUrlImageThumb($product->image_extra_1, env('IMAGE_MINI'), env('IMAGE_MINI')) }}"
                                 data-action="{{ $product->image_extra_1 }}">
                        </li>
                    @endif

                    @if($product->image_extra_2 != '')
                        <li>

                            <img src="{{ getUrlImageThumb($product->image_extra_2, env('IMAGE_MINI'), env('IMAGE_MINI')) }}"
                                 data-action="{{ $product->image_extra_2 }}">
                        </li>
                    @endif

                    @if($product->image_extra_3 != '')
                        <li>

                            <img src="{{ getUrlImageThumb($product->image_extra_3, env('IMAGE_MINI'), env('IMAGE_MINI')) }}"
                                 data-action="{{ $product->image_extra_3 }}">

                        </li>
                    @endif

                    @if($product->image_extra_4 != '')
                        <li>
                            <img src="{{ getUrlImageThumb($product->image_extra_4, env('IMAGE_MINI'), env('IMAGE_MINI')) }}"
                                 data-action="{{ $product->image_extra_4 }}">
                        </li>
                    @endif
                </ul>
            </div>
            <div class="thong_tin_san_pham">
                <ul>
                    <li><h2 itemprop="name">{{ $product->name }} </h2></li>
                    <li>
                            <span class="thuong_hieu"><a
                                        href="{{ URL::to('thuong-hieu/' . $product->manufacturer->slug ) }}"
                                        title="{{ $product->manufacturer->name }}">{{ $product->manufacturer->name }}</a></span>
                        <span class="ma_hang">{{ $product->code }}</span>
                        <span class="xuatxu">{{ $product->madein->name }}</span>
                    </li>
                    <li class="products_price">
                        @if($product->final_price != 0)
                            <span class="price_1"><b>{{ number_format($product->final_price, 0 ,'.', '.') }}
                                    <i>đ</i></b></span><span
                                    class="price_2"></span> <span
                                    class="vat">{{ number_format($product->base_price, 0 ,'.', '.') }}<i>đ</i></span>
                        @else
                            Liên hệ hotline để đặt hàng: {{ $setting->hotline }}
                        @endif
                    </li>
                    <li>
                        <a href="{{ URL::to('print/' . $product->slug) }}" target="_blank" class="inbaogia"
                           title="In báo giá">In báo
                            giá</a>
                        <button onclick="buy('{{ $product->id }}',1)" class="gio_hang" id="btnBuy" title="Mua hàng"
                                rel="nofollow">Mua hàng
                        </button>
                        @if($product->iframe != '')
                            <button class="gio_hang" id="btnBuy" title="Mua hàng" data-toggle="modal"
                                    data-target="#myModal"
                                    rel="nofollow">Xem catalog
                            </button>
                        @endif
                    </li>
                    <li class="nd_thanh_toan">
                        <button data-toggle="modal" data-target="#myModal2" rel="nofollow">Xem chi tiết</button>
                    </li>
                    <li class="hotline_cart">Gọi mua hàng: {{ $setting->hotline }}</li>
                    <li>Sản xuất: {{ $product->madein->name }}</li>
                    <li>Bảo hành: {{ $product->guarantee }} tháng</li>
                    <li class="products_shortdescription"><p>
                            <span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">{{ $product->intro }}
                                    .</span></span>
                        </p></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="noi_dung_produc">
        <div class="title">Miêu tả sản phẩm <a href="#" title="{{ $product->name }}"
                                               rel="nofollow">{{ $product->name }}</a>
        </div>
        <div class="products_description"><p>{!! $product->content !!}</p></div>
        @if($product->video != '')
            <div class="title">Video</div>
            <div class="thong_so">
                <p>
                    <iframe frameborder="0" height="315" scrolling="no"
                            src="https://www.youtube.com/embed/{{ $product->video }}" width="560"></iframe>
                </p>
            </div>
        @endif

        @if($product->specifications != '')
            <div class="title">Thông số kỹ thuật</div>
            <div class="thong_so">
                {!! $product->specifications !!}
            </div>
        @endif
    </div>

    <div class="spvx"><span class="lgicon"></span>
        @if(count($productRelate) > 0)
            <span class="tspvx">Sản phẩm liên quan</span>
            <ul>
                @foreach($productRelate as $item)
                    <li>
                        <div class="ct_bit1"><a href="{{ URL::to($item->category->slug . '/' . $item->slug) }}"
                                                title="{{ $item->name }}"> <img
                                        alt="{{ $item->name }}"
                                        src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                        class="imghometp"> </a>
                        </div>
                        <div class="ct_bit2">
                            <h2><a title="Đầu đọc mã vạch Zebex Z-3220"
                                   href="{{ URL::to($item->category->slug . '/' . $item->slug) }}">
                                    {{ $item->name }}</a></h2>
                            <div class="ma_thuong"><span class="thuong_hieu"><a
                                            href="{{ URL::to('thuong-hieu/' . $item->manufacturer->slug) }}"
                                            title="Zebex">{{ $item->manufacturer->name }}</a></span>
                            </div>
                        </div>
                        <div class="ct_bit3">
                            @if($item->final_price == 0)
                                <span class="gia">Liên hệ</span>
                            @else
                                <span class="gia">{{ number_format($item->final_price,0,",",".") }} <i>đ</i></span>
                                <span class="gia2">{!! number_format($item->base_price,0,",",".") !!} <i>đ</i></span>
                            @endif
                            <span class="chuthich"></span></div>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Catalog</h4>
                </div>
                <div class="modal-body">
                    @if($product->iframe != '')
                        <img src="{{ URL::asset('filemanager/userfiles/' . $setting->logo) }}">
                        <object data="{{ URL::asset('filemanager/userfiles/' . $product->iframe) }}"
                                type="application/pdf">
                            <embed src="{{ URL::asset('filemanager/userfiles/' . $product->iframe) }}"
                                   type="application/pdf"/>
                        </object>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Các hình thức thanh toán</h4>
                </div>
                <div class="modal-body thanh-toan">
                    <?php $listThanhToan = \App\Models\Post::where('status', 1)->where('category_id', 39)->get();?>
                    @if(is_object($listThanhToan))
                        <ul>
                            @foreach($listThanhToan as $item)
                                <li>
                                    <a href="#" data-action="{{ $item->id }}">
                                        <img src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>

            </div>
        </div>
    </div>

@stop

@section('custom_header')
    <link href="{{ URL::asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('frontend/css/style4.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('frontend/css/product.css') }}" rel="stylesheet" type="text/css">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>

    <script>
        // Buy product
        function buy(id, value) {
            if (value < 1) {
                swal({title: "Lỗi!", text: "Số lượng sản phẩm sai!", type: "error", confirmButtonText: "OK"});
            } else {
                $.ajax({
                    url: '{{ URL::to('dat-hang') }}',
                    type: 'POST',
                    data: {id: id, value: value, _token: '{{ csrf_token() }}'},
                    success: function (resp) {
                        if (resp.status == 'success') {
                            $('.gio-hang').html(parseInt($('.gio-hang').html()) + 1);
                            swal({
                                title: "Thành công",
                                text: "Đặt mua thành công!",
                                type: "success",
                                confirmButtonText: "OK"
                            });
                        } else {
                            swal({title: "Lỗi", text: resp.msg, type: "error", confirmButtonText: "OK"});
                        }
                    },
                    error: function (resp) {
                        swal({title: "Lỗi!", text: "Lỗi đặt mua sản phẩm!", type: "error", confirmButtonText: "OK"});
                    }
                });
            }
        }

        // Anh mo ta
        $('ul.img-relate img').each(function () {
            $(this).click(function () {
                var src = $(this).data('action');
                $('.image-prd').attr('src', 'http://' + document.domain + '/filemanager/userfiles/' + src);
            });
        });

        $('.thanh-toan').on('click', 'a', function () {
            var id = $(this).data('action');
            $.ajax({
                url: '{{ URL::to('chi-tiet-thanh-toan') }}',
                type: 'GET',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (resp) {
                    $('.modal-body.thanh-toan').html(resp + '<button class="thanh-toan-back">Quay lại</button>');
                },
                error: function (resp) {
                    alert('error');
                }
            });
        });

        $('.thanh-toan').on('click', 'button.thanh-toan-back', function () {

            $.ajax({
                url: '{{ URL::to('thong-tin-thanh-toan') }}',
                type: 'GET',
                data: {_token: '{{ csrf_token() }}'},
                success: function (resp) {
                    $('.modal-body.thanh-toan').html(resp);
                },
                error: function (resp) {
                    alert('error');
                }
            });
        });

    </script>
@stop