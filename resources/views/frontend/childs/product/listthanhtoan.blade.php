<?php $listThanhToan = \App\Models\Post::where('status', 1)->where('category_id', 39)->get();?>
@if(is_object($listThanhToan))
    <ul>
        @foreach($listThanhToan as $item)
            <li>
                <a href="#" data-action="{{ $item->id }}">
                    <img src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}">
                </a>
            </li>
        @endforeach
    </ul>
@endif