@if($listProduct)
    @foreach($listProduct as $product)
        <a href="{{ URL::to($product->category->slug . '/' . $product->slug) }}">
            <div class="item-content-product">
                <div class="border-product">
                    <div class="box-product">
                        <div class="image-new">
                            <i class="icon-new"></i>
                        </div>
                        <img class="img-content-product lazy" alt="{{ $product->name }}"
                             src="{{ URL::asset('filemanager/userfiles/'. $product->image) }}"/>
                    </div>
                    <p class="short-name">
                        <span> {{ $product->manufacturer->name }}  </span>
                    </p>

                    <p class="priceline "><span
                                class="ms-number-money2">{!! number_format($product->base_price,0,",",".") !!}</span>
                        ₫</p>

                    <p class="price "><span
                                class="ms-number-money2">{{ number_format($product->final_price,0,",",".") }}</span>
                        ₫</p>

                    <p class="full-name"><span class="">{{ $product->name }}</span></p>

                    <p class="icon">
                    </p>

                    <div class="bginfo">
                        <div class="box-attribute">
                            {{ $product->intro }}
                        </div>
                        <div class="box-detail">
                            <span class="btnviewdetail">{{ trans('product.view_more') }}</span>
                                                        <span class="btnsosanh add-compare add-compare-22"
                                                              data-value="pIhjz3LFyzQ="
                                                              data-index="22">{{ trans('product.compare') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    @endforeach
@endif