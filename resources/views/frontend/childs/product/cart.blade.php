@extends('frontend.layouts.master')

@section('main_content')
    <form action="" method="post">
        {{ csrf_field() }}
        <section>
            <div id="orderForm" class="wrapper">
                <h2>THÔNG TIN ĐẶT HÀNG</h2>
                @if(session('success')) <span class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
                <div class="shipping">
                    <label>Chọn hình thức mua hàng:</label>

                    <div class="method delivery" style="">
                        <input checked="" name="receipt_method" type="radio" value="Giao hàng tận nơi">
                        <label for="Delivery">
                            <strong>GIAO TẬN NƠI</strong>
                            <span>Xem hàng tại nhà không thích không mua.</span>
                        </label>
                    </div>
                    <div class="method instore" style="">
                        <input id="InStore" name="receipt_method" type="radio" value="Đặt tại showroom"/>
                        <label for="InStore">
                            <strong>ĐẶT TẠI SHOWROOM</strong>
                            <span>Giữ hàng trước, đến siêu thị xem và mua hàng.</span>
                        </label>
                    </div>
                    <div class="opts">
                        <div class="deliveryinfo ">
                            <div class="row">
                                <div class="col-xs-12">
                                    <label class="block">Thời gian giao hàng:</label>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="form-group ">
                                            <div class="input-group ms-box-date">
                                                <input name="date" type="datetime-local" class="form-control"
                                                       required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <label class="block">Địa chỉ:</label>
                                </div>
                                <div class="col-xs-12">
                                    <textarea class="form-control" name="address" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="customer">
                    <div class="gender">
                        <label>Nhập thông tin của bạn:</label>

                        <div class="checkbox">
                            <label>
                                <input type="radio" checked="checked" value="nam" name="sex"/> Anh </label>
                            <label>
                                <input type="radio" value="nữ" name="sex"/> Chị </label>

                        </div>
                    </div>
                    <div class="info">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group form-group-lg">
                                    <input type="text" name="name" class="form-control" placeholder="Họ tên" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group form-group-lg">
                                    <input type="text" name="tel" class="form-control is-number" placeholder="Điện thoại" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group form-group-lg">
                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="calclist">
                    @if(isset($_SESSION['order']) && count($_SESSION['order']) != 0)
                        <?php $total_price = 0;?>
                        @foreach($_SESSION['order'] as $key => $item)
                            <?php $product = \App\Models\Product::find($key);?>
                            @if($product != null)
                                <?php
                                $price = $product->final_price * $item['value'];
                                $total_price += $price;
                                ?>
                                <li>
                                    <div class="table  card-1">
                                        <div class="price">
                                            <div class="pinfo">
                                                <img src="{{ getUrlImageThumb($product->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                                     width="95" alt="{{ $product->name }}">
                                                <a href="{{ URL::to($product->category->slug .'/'. $product->slug) }}">
                                                    <h1>{{ $product->name }}</h1></a>
                                                <input type="hidden" class="product-1" name="ProductId"
                                                       value="Wf/8HDTX6+U="/>

                                                <p>
                                                    <strong><span
                                                                class="ms-number-money2">{!! number_format($product->final_price,0,",",".") !!}</span><span> ₫</span></strong>
                                                </p>

                                                <p>
                                                    <label class="infopromotion"></label>
                                                </p>

                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                        <div class="group-quantity">
                                            <div class="box-quantity">
                                                <p>Số lượng</p> {{ $item['value'] }}
                                            </div>
                                            <div class="total">
                                                <p>Thành tiền</p>

                                                <p><strong><span class="ms-number-money2 t-price t-price-1">{!! number_format($price,0,",",".") !!} </span><span> ₫</span></strong>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="remove-gh">
                            <span class="remove-item-card card-remove-s" data-item="1" data-value="Wf/8HDTX6+U="><i
                                        class="fa fa-times-circle"></i></span>
                                        </div>
                                    </div>

                                </li>
                            @endif
                        @endforeach
                    <li>
                        <input class="frmctrl" name="coupon_code" placeholder="Mã phiếu giảm giá (nếu có)" type="text"
                               value="">
                    </li>
                    <li>
                        <span><b>Tổng cộng</b></span>
                        <strong><span itemid='1700000.00' class="ms-number-money2 total-price">{!! number_format($total_price,0,",",".") !!}</span>
                            <span> ₫</span></strong>
                        <input type="hidden" name="total_price" value="{{ $total_price }}">
                        <div class="clr"></div>
                    </li>
                    <li id="deliveryPayMsg">
                        (*) Giá trên không bao gồm phí giao hàng, nhân viên sẽ gọi điện để tư vấn phí giao hàng cho quý
                        khách.
                    </li>
                    @else
                        <li>Không có sản phẩm trong giỏ hàng</li>
                    @endif
                </ul>
                <div class="note">
                <textarea class="frmctrl" cols="20" id="OrderNote" name="note"
                          placeholder="Ghi chú thêm của bạn nếu có" rows="2"></textarea>
                </div>
                <button type="submit" class="button blue bnt-save-card" data-taget="0">XÁC NHẬN ĐẶT HÀNG</button>
      {{--          <div class="clr"></div>
                <div class="call">
                    Tổng đài hỗ trợ: <strong>0902.195.911</strong> (gọi miễn phí) - <strong>0903.295.199</strong>
                </div>--}}
            </div>
        </section>
    </form>
@stop

@section('custom_header')
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style7_cart.css') }}">

    <style>
        .opts:before {
            display: none;
        }
    </style>
@stop