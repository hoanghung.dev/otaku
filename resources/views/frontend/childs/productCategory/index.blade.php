@extends('frontend.layouts.master')

@section('main_content')



    <div class="boxphai">

        <div class="provider_cate">
            @if($listManufacturer)
                @foreach($listManufacturer as $item)
                    <a class="provider_cate_bit" href="{{ URL::to('thuong-hieu/' . $item->slug) }}">
                        <img src="{{ URL::asset('filemanager/userfiles/' . $item->image) }}" height="30"
                             alt="{{ $item->name }}">
                    </a>
                @endforeach
            @endif
        </div>

        <ul class="hcp_ul_ct_3">
            @if($listProduct)
                @foreach($listProduct as $item)<p class="priced ">
                <li>
                    <div class="ct_bit1"><a href="{{ URL::to($item->category->slug . '/' . $item->slug) }}"
                                            title="{{ $item->name }}"> <img
                                    alt="{{ $item->name }}"
                                    src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                    class="imghometp"> </a>
                    </div>
                    <div class="ct_bit2">
                        <h2><a title="Đầu đọc mã vạch Zebex Z-3220"
                               href="{{ URL::to($item->category->slug . '/' . $item->slug) }}">
                                {{ $item->name }}</a></h2>
                        <div class="ma_thuong"><span class="thuong_hieu"><a
                                        href="{{ URL::to('thuong-hieu/' . $item->manufacturer->slug) }}"
                                        title="Zebex">{{ $item->manufacturer->name }}</a></span>
                        </div>
                    </div>
                    <div class="ct_bit3">
                        @if($item->final_price == 0)
                            <span class="gia">Liên hệ</span>
                        @else
                            <span class="gia">{{ number_format($item->final_price,0,",",".") }} <i>đ</i></span>
                            <span class="gia2">{!! number_format($item->base_price,0,",",".") !!} <i>đ</i></span>
                        @endif
                        <?php if($item->sale_id != 0){ ?>
                            <span class="giamgia">
                                <?php if(isset($item->sale)){?> {{ $item->sale->type }} <?php }?>
                            </span>
                        <?php } ?>
                    </div>
                </li>
                @endforeach
            @endif
        </ul>


        <div class="p_trang">
            {!! $listProduct->links() !!}
        </div>

        @include('frontend.partials.news_bottom')

    </div>

@stop

@section('custom_header')
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('frontend/css/style3.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('frontend/css/timkiem.css') }}">
@stop

@section('custom_footer')
    <script>
        $('.filter-price').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'price' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });

        $('.filter-core').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'core' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });

        $('.filter-system').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'system' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });

        $('.filter-ram').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'ram' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });

        $('.filter-chip').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'chip' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });

        $('.filter-hard').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'hard' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });

        $('.filter-sx').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'sx' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });

        $('.fa.fa-times').each(function () {
            $(this).click(function () {
                var data = $(this).data('action');
                $.ajax({
                    url: '{{ URL::to('product-filter') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: { data: data, type: 'delete' , _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            location.reload();
                        } else {
                            swal({title: 'Lỗi!', text: 'Lỗi tìm kiếm sản phẩm', type: 'error'});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            });
        });
    </script>
@stop