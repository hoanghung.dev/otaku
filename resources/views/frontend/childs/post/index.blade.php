@extends('frontend.layouts.master')

@section('main_content')

    <div id="main">
        <div id="main-inner">

            @include('frontend.partials.breadcrumb')

            <div class="banner hidden-xs">
                <img src="{{ URL::asset('images/banner.jpg') }}" alt="{{ $post->name }}">

                <div class="dark-background"></div>

                <div class="hero-text center">
                    <div class="small">Best Meal in the City</div>
                    <div class="large">Tasty Restaurant Ltd.</div>
                    <div class="small">New York City, NY, Beverly Hills 90210</div>
                </div><!-- /.hero-text -->

                <div class="container relative">
                    <div class="small-photo">
                        <a href="#">
                            <img src="{{ getUrlImageThumb($post->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                 alt="{{ $post->name }}">
                        </a>
                    </div><!-- /.small-photo -->
                </div>
            </div><!-- /.banner -->

            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-content">
                            <div class="block-content-inner">
                                <div class="primary-content">
                                    <h2 class="widgetized-title">{{ $post->name }}</h2>

                                    <p>
                                        {!! $post->content !!}
                                    </p>

                                    <h3 class="widgetized-title mt40">Bài liên quan</h3>

                                    <div class="reviews">
                                        <div class="review">
                                            <div class="review-image">
                                                <a href="#">
                                                    <img src="../../assets/img/tmp/reviews/01.jpg" alt="">
                                                </a>
                                            </div><!-- /.review-image -->

                                            <div class="review-title">
                                                <h2><a href="#">Etiam malesuada consectetur hendrerit</a></h2>

                                                <div class="rating pull-left">
                                                    <a href="#">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half-empty"></i>
                                                    </a>
                                                </div><!-- /.rating -->

                                                <span class="review-count">by <a href="#">John Doe</a></span>
                                            </div><!-- /.review-title -->
                                        </div><!-- /.review -->

                                        <div class="review">
                                            <div class="review-image">
                                                <a href="#">
                                                    <img src="../../assets/img/tmp/reviews/02.jpg" alt="">
                                                </a>
                                            </div><!-- /.review-image -->

                                            <div class="review-title">
                                                <h2><a href="#">Ut dictum hendrerit diam, vitae rhoncus</a></h2>

                                                <div class="rating pull-left">
                                                    <a href="#">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half-empty"></i>
                                                    </a>
                                                </div><!-- /.rating -->

                                                <span class="review-count">by <a href="#">John Doe</a></span>
                                            </div><!-- /.review-title -->
                                        </div><!-- /.review -->

                                        <div class="review">
                                            <div class="review-image">
                                                <a href="#">
                                                    <img src="../../assets/img/tmp/reviews/03.jpg" alt="">
                                                </a>
                                            </div><!-- /.review-image -->

                                            <div class="review-title">
                                                <h2><a href="#">Maecenas nisl augue, auctor</a></h2>

                                                <div class="rating pull-left">
                                                    <a href="#">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half-empty"></i>
                                                    </a>
                                                </div><!-- /.rating -->

                                                <span class="review-count">by <a href="#">John Doe</a></span>
                                            </div><!-- /.review-title -->
                                        </div><!-- /.review -->
                                    </div>

                                    <h3 class="widgetized-title mt40">5 Comments</h3>

                                    <div class="comments">
                                        <div class="comment">
                                            <div class="comment-body">
                                                <div class="comment-picture">
                                                    <div class="comment-picture-inner">
                                                        <a href="#">
                                                            <img src="../../assets/img/tmp/authors/1.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div><!-- /.comment-picture -->

                                                <div class="comment-content">
                                                    <div class="comment-meta">
                                                        <h3 class="comment-title"><a href="#">Britney Doe</a></h3>
                                                        <span class="comment-date">2 days ago</span>
                                                        <a href="#" class="comment-reply"><i
                                                                    class="fa fa-angle-double-left"></i> Reply</a>
                                                    </div><!-- /.comment-meta -->

                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                                        diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                                                        aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                                                        nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                                        aliquip ex ea commodo consequat.
                                                    </p>
                                                </div><!-- /.comment-content -->
                                            </div><!-- /.comment-body -->

                                            <div class="comment">
                                                <div class="comment-body">
                                                    <div class="comment-picture">
                                                        <div class="comment-picture-inner">
                                                            <a href="#">
                                                                <img src="../../assets/img/tmp/authors/2.jpg" alt="">
                                                            </a>
                                                        </div>
                                                    </div><!-- /.comment-picture -->

                                                    <div class="comment-content">
                                                        <div class="comment-meta">
                                                            <h3 class="comment-title"><a href="#">Britney Doe</a></h3>
                                                            <span class="comment-date">2 days ago</span>
                                                            <a href="#" class="comment-reply"><i
                                                                        class="fa fa-angle-double-left"></i> Reply</a>
                                                        </div><!-- /.comment-meta -->

                                                        <p>
                                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                                            sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                            magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
                                                            quis nostrud exerci tation ullamcorper suscipit lobortis
                                                            nisl ut aliquip ex ea commodo consequat.
                                                        </p>
                                                    </div><!-- /.comment-content -->
                                                </div><!-- /.comment-body -->

                                                <div class="comment">
                                                    <div class="comment-body">
                                                        <div class="comment-picture">
                                                            <div class="comment-picture-inner">
                                                                <a href="#">
                                                                    <img src="../../assets/img/tmp/authors/3.jpg"
                                                                         alt="">
                                                                </a>
                                                            </div>
                                                        </div><!-- /.comment-picture -->

                                                        <div class="comment-content">
                                                            <div class="comment-meta">
                                                                <h3 class="comment-title"><a href="#">Britney Doe</a>
                                                                </h3>
                                                                <span class="comment-date">2 days ago</span>
                                                                <a href="#" class="comment-reply"><i
                                                                            class="fa fa-angle-double-left"></i>
                                                                    Reply</a>
                                                            </div><!-- /.comment-meta -->

                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                                elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                                                                dolore magna aliquam erat volutpat. Ut wisi enim ad
                                                                minim veniam, quis nostrud exerci tation ullamcorper
                                                                suscipit lobortis nisl ut aliquip ex ea commodo
                                                                consequat.
                                                            </p>
                                                        </div><!-- /.comment-content -->
                                                    </div><!-- /.comment-body -->
                                                </div><!-- /.comment -->
                                            </div><!-- /.comment -->
                                        </div><!-- /.comment -->

                                        <div class="comment">
                                            <div class="comment-body">
                                                <div class="comment-picture">
                                                    <div class="comment-picture-inner">
                                                        <a href="#">
                                                            <img src="../../assets/img/tmp/authors/1.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div><!-- /.comment-picture -->

                                                <div class="comment-content">
                                                    <div class="comment-meta">
                                                        <h3 class="comment-title"><a href="#">Britney Doe</a></h3>
                                                        <span class="comment-date">2 days ago</span>
                                                        <a href="#" class="comment-reply"><i
                                                                    class="fa fa-angle-double-left"></i> Reply</a>
                                                    </div><!-- /.comment-meta -->

                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                                        diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                                                        aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                                                        nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                                        aliquip ex ea commodo consequat.
                                                    </p>
                                                </div><!-- /.comment-content -->
                                            </div><!-- /.comment-body -->
                                        </div><!-- /.comment -->

                                        <div class="comment">
                                            <div class="comment-body">
                                                <div class="comment-picture">
                                                    <div class="comment-picture-inner">
                                                        <a href="#">
                                                            <img src="../../assets/img/tmp/authors/2.jpg" alt="">
                                                        </a>
                                                    </div>
                                                </div><!-- /.comment-picture -->

                                                <div class="comment-content">
                                                    <div class="comment-meta">
                                                        <h3 class="comment-title"><a href="#">Britney Doe</a></h3>
                                                        <span class="comment-date">2 days ago</span>
                                                        <a href="#" class="comment-reply"><i
                                                                    class="fa fa-angle-double-left"></i> Reply</a>
                                                    </div><!-- /.comment-meta -->

                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                                        diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                                                        aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                                                        nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                                                        aliquip ex ea commodo consequat.
                                                    </p>
                                                </div><!-- /.comment-content -->
                                            </div><!-- /.comment-body -->
                                        </div><!-- /.comment -->
                                    </div><!-- /.comments -->

                                    <h3 class="widgetized-title">Leave Comment</h3>

                                    <form method="post" action="?" class="ng-pristine ng-valid">
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <input type="text" class="form-control" placeholder="Your name"
                                                       required="required">
                                            </div><!-- /.form-group -->

                                            <div class="form-group col-sm-6">
                                                <input type="email" class="form-control" placeholder="E-mail"
                                                       required="required">
                                            </div><!-- /.form-group -->
                                        </div><!-- /.row -->

                                        <textarea placeholder="Message" required="required" class="form-control"
                                                  rows="4"></textarea>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="sidebar">
                            <div class="block-content">
                                <div class="block-content-inner">
                                    <div class="widget widget-boxed widget-boxed-secondary">
                                        <form method="post">
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control">
                                            </div><!-- /.form-group -->


                                            <div class="form-group">
                                                <label>Type</label>

                                                <select class="form-control" style="display: none;">
                                                    <option value="">Automotive</option>
                                                    <option value="">Coffee</option>
                                                    <option value="">Food</option>
                                                    <option value="">Museum</option>
                                                    <option value="">Restaurant</option>
                                                    <option value="">Transportation</option>
                                                    <option value="">Wine</option>
                                                </select>
                                                <div class="chosen-container chosen-container-single"
                                                     style="width: 233px;" title=""><a class="chosen-single"
                                                                                       tabindex="-1"><span>Automotive</span>
                                                        <div><b></b></div>
                                                    </a>
                                                    <div class="chosen-drop">
                                                        <div class="chosen-search"><input type="text"
                                                                                          autocomplete="off"></div>
                                                        <ul class="chosen-results"></ul>
                                                    </div>
                                                </div>
                                            </div><!-- /.form-group -->


                                            <div class="form-group">
                                                <label>Price From</label>

                                                <select class="form-control" style="display: none;">
                                                    <option value="">2000 €</option>
                                                    <option value="">3000 €</option>
                                                    <option value="">5000 €</option>
                                                </select>
                                                <div class="chosen-container chosen-container-single chosen-container-single-nosearch"
                                                     style="width: 233px;" title=""><a class="chosen-single"
                                                                                       tabindex="-1"><span>2000 €</span>
                                                        <div><b></b></div>
                                                    </a>
                                                    <div class="chosen-drop">
                                                        <div class="chosen-search"><input type="text" autocomplete="off"
                                                                                          readonly=""></div>
                                                        <ul class="chosen-results"></ul>
                                                    </div>
                                                </div>
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <label>Price To</label>

                                                <select class="form-control" style="display: none;">
                                                    <option value="">2000 €</option>
                                                    <option value="">3000 €</option>
                                                    <option value="">5000 €</option>
                                                </select>
                                                <div class="chosen-container chosen-container-single chosen-container-single-nosearch"
                                                     style="width: 233px;" title=""><a class="chosen-single"
                                                                                       tabindex="-1"><span>2000 €</span>
                                                        <div><b></b></div>
                                                    </a>
                                                    <div class="chosen-drop">
                                                        <div class="chosen-search"><input type="text" autocomplete="off"
                                                                                          readonly=""></div>
                                                        <ul class="chosen-results"></ul>
                                                    </div>
                                                </div>
                                            </div><!-- /.form-group -->

                                            <input type="submit" class="btn btn-block btn-secondary" value="Filter">
                                        </form>
                                    </div><!-- /.widget -->
                                    <div class="widget widget-boxed widget-boxed-dark">
                                        <h3>Opening Hours</h3>

                                        <div class="opening-hours">
                                            <div class="day clearfix">
                                                <span class="name">Monday</span><span
                                                        class="hours">07:00 AM - 07:00 PM</span>
                                            </div><!-- /.day -->

                                            <div class="day clearfix">
                                                <span class="name">Tuesday</span><span
                                                        class="hours">07:00 AM - 07:00 PM</span>
                                            </div><!-- /.day -->

                                            <div class="day clearfix">
                                                <span class="name">Wednesday</span><span class="hours">07:00 AM - 07:00 PM</span>
                                            </div><!-- /.day -->

                                            <div class="day clearfix">
                                                <span class="name">Thursday</span><span class="hours">07:00 AM - 07:00 PM</span>
                                            </div><!-- /.day -->

                                            <div class="day clearfix">
                                                <span class="name">Friday</span><span
                                                        class="hours">07:00 AM - 07:00 PM</span>
                                            </div><!-- /.day -->

                                            <div class="day clearfix">
                                                <span class="name">Saturday</span><span class="hours">07:00 AM - 02:00 PM</span>
                                            </div><!-- /.day -->

                                            <div class="day clearfix">
                                                <span class="name">Sunday</span><span class="hours"><i
                                                            class="icon icon-normal-door-out"></i> Closed</span>
                                            </div><!-- /.day -->
                                        </div><!-- /.opening-hours -->
                                    </div><!-- /.widget -->                    </div><!-- /.block-content-inner -->
                            </div><!-- /.block-content -->
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /#main-inner -->
    </div>

@stop