<html>
<head>

    @include('frontend.partials.header_script')

</head>

<body class="breadcrumb-white footer-top-dark">

<div id="page">

    @include('frontend.partials.header')

    @if($_SERVER['REQUEST_URI'] != '/')
        @include('frontend.partials.breadcrumb')
    @endif

    @yield('main_content')

</div>

@include('frontend.partials.footer')

@include('frontend.partials.footer_script')


</body>
</html>