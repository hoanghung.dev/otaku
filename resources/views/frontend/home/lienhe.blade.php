@extends('frontend.layouts.master')
@section('main_content')

    <div class="container">
        <div class="block-content">
            <div class="block-content-inner">
                <div class="page-header center">
                    <h1>Contact Us</h1>
                </div><!-- /.page-header -->

                <div class="row mb40">
                    <div class="col-sm-4">
                        <h4>Contact Information</h4>

                        <p>
                            <strong>Your Company, Inc.</strong>
                        </p>

                        <p class="address">
                            <i class="icon icon-normal-pointer-pin"></i>
            <span>795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107</span>
                        </p>

                        <p>
                            <strong><i class="icon icon-normal-phone"></i> Phone:</strong> (123) 456-7890<br>
                            <strong><i class="icon icon-normal-mail"></i> E-mail:</strong> mail@yourcompany.com
                        </p>
                    </div>

                    <div class="col-sm-8">
                        <iframe class="contact-map" width="425" height="425"
                                src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=5th+Avenue,+New+York,+NY,+United+States&amp;aq=0&amp;oq=5th+&amp;sll=37.0625,-95.677068&amp;sspn=70.689889,135.263672&amp;ie=UTF8&amp;hq=&amp;hnear=5th+Ave,+New+York&amp;t=m&amp;z=13&amp;ll=40.649866,-74.005367&amp;output=embed"></iframe>
                    </div>
                </div><!-- /.row -->

                <form method="POST" action="" id="contact-form">
                    <div class="row">
                        <div class="form-group col-sm-4 col-md-4">
                            <label>Họ tên</label>
                            <input type="text" name="name" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-sm-4 col-md-4">
                            <label>Email</label>
                            <input type="text" name="mail" class="form-control">
                        </div><!-- /.form-group -->

                        <div class="form-group col-sm-4 col-md-4">
                            <label>Số điện thoại</label>
                            <input type="text" name="tel" class="form-control">
                        </div><!-- /.form-group -->
                    </div><!-- /.row -->
                    <div class="textarea form-group">
                        <label>Lời nhắn</label>
                        <textarea class="form-control" rows="4" name="message"></textarea>
                    </div>
                    <button class="btn btn-secondary" type="button">Contact</button>
                    <script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>
                    <link href="{{ URL::asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css">
                    <script>
                        $('#contact-form').submit(function (e) {
                            e.preventDefault();
                            var name = $('input[name=name]').val();
                            var email = $('input[name=email]').val();
                            var tel = $('input[name=tel]').val();
                            var message = $('textarea[name=message]').val();

                            $.ajax({
                                url: '{{ URL::to('contact') }}',
                                type: 'POST',
                                data: {
                                    name: name,
                                    email: email,
                                    tel: tel,
                                    message: message,
                                    _token: '{{ csrf_token() }}'
                                },
                                success: function () {
                                    swal({title: 'Gửi thành công!', text: 'Chúng tôi sẽ liên hệ với bạn sớm nhất.', type: 'success'});
                                    $('#contact-form input').val('');
                                    $('#contact-form textarea').val('');
                                }
                            });
                        });
                    </script>
                </form>
            </div><!-- /.block-content-inner -->
        </div><!-- /.block-content -->
    </div>
@stop
