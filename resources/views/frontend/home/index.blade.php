@extends('frontend.layouts.master')

@section('main_content')

    <div id="main-wrapper">
        <div id="main">
            <div id="main-inner">
                <div class="google-map-wrapper">
                    <div id="google-map-div" style="position: relative; overflow: hidden;">

                    </div>

                    <div class="container">
                        <div class="google-map-filter col-xs-12 col-sm-4 col-md-3 pull-right">
                            <form method="post"
                                  action="{{ URL::to('/') }}?"
                                  class="clearfix">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Keyword search" name="keyword">
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <select name="category_id" data-placeholder="Chọn loại nhà đất" class="form-control"
                                            style="display: none;">
                                        <option value="#">Chọn loại nhà đất</option>
                                        <option value="#">Czech Republic</option>
                                        <option value="#">Germany</option>
                                        <option value="#">France</option>
                                        <option value="#">Hungary</option>
                                        <option value="#">Italy</option>
                                        <option value="#">Poland</option>
                                        <option value="#">Slovak Republic</option>
                                        <option value="#">Ukraine</option>
                                    </select>
                                    <div class="chosen-container chosen-container-single" style="width: 245px;"
                                         title=""><a class="chosen-single chosen-default" tabindex="-1"><span>Chọn loại nhà đất</span>
                                            <div><b></b></div>
                                        </a>
                                        <div class="chosen-drop">
                                            <div class="chosen-search"><input type="text" autocomplete="off"></div>
                                            <ul class="chosen-results"></ul>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <select name="city" data-placeholder="Chọn tỉnh thành phố" class="form-control"
                                            style="display: none;">
                                        <option value="#">Chọn tỉnh thành phố</option>
                                        <option value="#">Brooklyn</option>
                                        <option value="#">Bronx</option>
                                        <option value="#">Manhattan</option>
                                        <option value="#">Village</option>
                                        <option value="#">Queens</option>
                                    </select>
                                    <div class="chosen-container chosen-container-single" style="width: 245px;"
                                         title=""><a class="chosen-single chosen-default" tabindex="-1"><span>Chọn tỉnh thành phố</span>
                                            <div><b></b></div>
                                        </a>
                                        <div class="chosen-drop">
                                            <div class="chosen-search"><input type="text" autocomplete="off"></div>
                                            <ul class="chosen-results"></ul>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <select name="address" data-placeholder="Chọn quận huyện" class="form-control"
                                            style="display: none;">
                                        <option value="#">Chọn quận huyện</option>
                                        <option value="#">Berlin</option>
                                        <option value="#">Canberra</option>
                                        <option value="#">London</option>
                                        <option value="#">New York</option>
                                        <option value="#">Paris</option>
                                        <option value="#">Prague</option>
                                    </select>
                                    <div class="chosen-container chosen-container-single" style="width: 245px;"
                                         title=""><a class="chosen-single chosen-default" tabindex="-1"><span>Chọn quận huyện</span>
                                            <div><b></b></div>
                                        </a>
                                        <div class="chosen-drop">
                                            <div class="chosen-search"><input type="text" autocomplete="off"></div>
                                            <ul class="chosen-results"></ul>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <select name="acreage" data-placeholder="Chọn diện tích" class="form-control"
                                            style="display: none;">
                                        <option value="#">Chọn diện tích</option>
                                        <option value="#">Autoservice</option>
                                        <option value="#">Bank</option>
                                        <option value="#">Bar</option>
                                        <option value="#">Castle</option>
                                        <option value="#">Hospital</option>
                                        <option value="#">Museum</option>
                                        <option value="#">Restaurant</option>
                                    </select>
                                    <div class="chosen-container chosen-container-single" style="width: 245px;"
                                         title=""><a class="chosen-single chosen-default" tabindex="-1"><span>Chọn diện tích</span>
                                            <div><b></b></div>
                                        </a>
                                        <div class="chosen-drop">
                                            <div class="chosen-search"><input type="text" autocomplete="off"></div>
                                            <ul class="chosen-results"></ul>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6">
                                            <input type="text" class="form-control" placeholder="Giá từ"
                                                   name="price_start">
                                        </div>

                                        <div class="col-xs-6 col-sm-6">
                                            <input type="text" class="form-control" placeholder="Giá đến"
                                                   name="price_end">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" value="Search" class="btn btn-terciary btn-block">
                                </div><!-- /.form-group -->
                            </form>
                        </div><!-- /.google-map-filter -->
                    </div><!-- /.container --></div>

                <?php
                $listItem = \App\Models\House::where('status', 1)->where('vip', 1)->orderBy('id', 'desc')->limit(12)->get();
                ?>
                @if(count($listItem) >0)
                    <div class="secondary-images hidden-xs">
                        <div class="row">
                            @foreach($listItem as $item)
                                <div class="secondary-image col-sm-1">
                                    @if($item->vip == 1)
                                        <img src="{{ URL::asset('images/icon_hot.png') }}" class="icon_hot">
                                    @endif
                                    <a href="{{ URL::to('bai-viet/'. $item->id) }}">
                                        <?php showImage($item->image);?>
                                        <span></span>
                                    </a>
                                    <p class="price">{{ $item->price }} triệu</p>
                                </div><!-- /.secondary-image -->
                            @endforeach
                        </div><!-- /.row -->
                    </div>
                @endif


                <div class="container">
                    <div class="block-content block-content-medium-padding">
                        <div class="block-content-inner">
                            <h1 class="center mb40 widgetized-title">Bài đăng mới</h1>

                            @if(count($listCategory) > 0)
                                <ul class="portfolio-isotope-filter clearfix">
                                    @foreach($listCategory as $key => $item)
                                        <li class="@if($key == 0){{ 'selected' }}@endif"><a
                                                    data-filter=".div{{ $item->id }}"><span>{{ $item->name }}</span></a>
                                        </li>
                                    @endforeach
                                    {{-- <li><a data-filter=".ogep"><span>Ở ghép</span></a></li>
                                     <li><a data-filter=".chungcu"><span>Chung cư</span></a></li>
                                     <li><a data-filter=".bietthu"><span>Biệt thự</span></a></li>
                                     <li><a data-filter=".khoxuong"><span>Kho xưởng</span></a></li>
                                     <li><a data-filter=".cuahang"><span>Cửa hàng</span></a></li>
                                     <li><a data-filter=".vanphong"><span>Văn phòng</span></a></li>--}}
                                </ul>
                            @endif


                            <div class="portfolio portfolio-isotope isotope"
                                 style="position: relative; overflow: hidden; height: 890px;">
                                @if(count($listCategory) > 0)
                                    @foreach($listCategory as $key => $cat)
                                        <?php $listItem = \App\Models\House::where('status', 1)->where('category_id', $cat->id)->limit(10)->get();?>
                                        @if(count($listItem) > 0)
                                            @foreach($listItem as $item)
                                                <div class="col-sm-3 portfolio-item-isotope div{{ $cat->id }} isotope-item"
                                                     style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">

                                                    <div class="box background-white">
                                                        @if($item->vip == 1)
                                                            <img src="{{ URL::asset('images/icon_hot.png') }}"
                                                                 class="icon_hot">
                                                        @endif
                                                        <div class="box-picture">
                                                            <a href="{{ URL::to('bai-viet/' . $item->id) }}">
                                                                <?php showImage($item->image);?>
                                                                <span></span>
                                                            </a>
                                                        </div><!-- /.box-picture -->

                                                        <div class="box-body">
                                                            <h2 class="box-title-plain">
                                                                <a href="{{ URL::to('bai-viet/' . $item->id) }}">{{ $item->name }}</a>
                                                            </h2><!-- /.box-title-plain -->

                                                            <div class="box-content">
                                                                <div class="box-meta clearfix">
                                                                    <p>Giá : {{ $item->price }} triệu</p>
                                                                    <p>Diện tích : {{ $item->acreage }} m2</p>
                                                                    <p>Địa chỉ : {{ $item->address }}</p>
                                                                </div>
                                                            </div><!-- /.box-content -->
                                                        </div><!-- /.box-body -->
                                                    </div><!-- /.box -->
                                                </div>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </div><!-- /.portfolio -->


                        </div><!-- /.block-content-inner -->
                    </div><!-- /.block-content -->
                </div><!-- /.container -->


                <div class="container">
                    <div class="block-content background-white fullwidth">
                        <div class="block-content-inner">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h3 class="widgetized-title">Tin dành cho bạn</h3>

                                    <script>
                                        $('.vertical-tabs .tabs-left li').each(function () {
                                            $(this).click(function () {
                                                var target = $(this).data('target');
                                                alert(target);
                                                $(target).addClass('active');
                                                $(target).siblings().removeClass('active');
                                                $(this).addClass('active');
                                                $(this).removeClass('active');
                                            });
                                        });
                                    </script>
                                    <div class="vertical-tabs clearfix">
                                        <div class="vertical-tab-left col-xs-4 col-sm-3"> <!-- required for floating -->
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs tabs-left">
                                                @if(count($listCategory) > 0)
                                                    @foreach($listCategory as $key => $item)
                                                        <li class="@if($key == 0){{ 'active' }}@endif category-red"
                                                            data-target=".div{{ $item->id }}">
                                                            <a href="{{ URL::to('/') }}#div{{ $item->id }}"
                                                               data-toggle="tab">
                                                                <img src="{{ URL::asset('filemanager/userfiles/' . $item->icon) }}"
                                                                     alt="">
                                                                <div class="title">
                                                                    <strong>{{ $item->name }}</strong>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div><!-- /.vertical-tab-left -->

                                        <div class="vertical-tab-right col-xs-8 col-sm-9">
                                            <div class="tab-content">
                                                @foreach($listCategory as $key => $cat)
                                                    <div class="tab-pane @if($key == 0){{ 'active' }}@endif"
                                                         id="div{{ $cat->id }}">
                                                        <div class="reviews">
                                                            @if(isset($listHouse[$cat->id]) && count($listHouse[$cat->id]) > 0)
                                                                @foreach($listHouse[$cat->id] as $item)
                                                                    <div class="review">
                                                                        @if($item->vip == 1)
                                                                            <img src="{{ URL::asset('images/icon_hot.png') }}"
                                                                                 class="icon_hot">
                                                                        @endif
                                                                        <div class="review-image">
                                                                            <a href="{{ URL::to('bai-viet/'.$item->id) }}">
                                                                                <?php showImage($item->image);?>
                                                                            </a>
                                                                        </div><!-- /.review-image -->

                                                                        <div class="review-title">
                                                                            <h2>
                                                                                <a href="{{ URL::to('bai-viet/'.$item->id) }}">{{ $item->name }}</a>
                                                                            </h2>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>Giá :</td>
                                                                                    <td>{{ $item->price }} triệu</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Diện tích :</td>
                                                                                    <td>{{ $item->acreage }} m2</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Địa chỉ :</td>
                                                                                    <td>{{ $item->address }}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div><!-- /.review-title -->
                                                                    </div>
                                                                    <hr>
                                                                @endforeach
                                                            @endif

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a>
                                                        </div>
                                                    </div><!-- /.tab-pane -->
                                                @endforeach

                                                <div class="tab-pane" id="castles">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/05.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        sodales lorem eget</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">67 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/10.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Vestibulum
                                                                        interdum velit mauris</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">69 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Praesent
                                                                        hendrerit faucibus nulla</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">62 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/02.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Praesent
                                                                        hendrerit faucibus nulla</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">71 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/07.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Vivamus
                                                                        pretium vitae libero at ultrices</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">65 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/05.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Praesent
                                                                        hendrerit faucibus nulla</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">58 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->

                                                <div class="tab-pane" id="church">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        sodales lorem eget</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">50 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/02.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Mauris
                                                                        fermentum rhoncus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">63 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/07.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        sodales lorem eget</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">99 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/02.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        malesuada consectetur hendrerit</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">57 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/10.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        malesuada consectetur hendrerit</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">55 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/07.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        sodales lorem eget</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">69 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->

                                                <div class="tab-pane" id="coffee">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/04.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Donec
                                                                        laoreet, tellus ut faucibus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">45 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/02.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Nunc
                                                                        elementum augue massa</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">65 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Praesent
                                                                        hendrerit faucibus nulla</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">62 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Mauris
                                                                        fermentum rhoncus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">32 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/08.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Donec
                                                                        laoreet, tellus ut faucibus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">51 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/10.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Quisque
                                                                        orci urna, tristique eget pharetra</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">86 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->

                                                <div class="tab-pane" id="food">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Donec
                                                                        laoreet, tellus ut faucibus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">70 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/04.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Quisque
                                                                        orci urna, tristique eget pharetra</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">2 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/06.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Mauris
                                                                        fermentum rhoncus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">84 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/08.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Vivamus
                                                                        pretium vitae libero at ultrices</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">39 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/06.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Suspendisse
                                                                        adipiscing, tellus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">37 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/05.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Nunc
                                                                        elementum augue massa</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">13 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->

                                                <div class="tab-pane" id="malls">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/03.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Suspendisse
                                                                        adipiscing, tellus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">42 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/05.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Donec
                                                                        laoreet, tellus ut faucibus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">1 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/06.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Quisque
                                                                        orci urna, tristique eget pharetra</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">94 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/04.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Nunc
                                                                        elementum augue massa</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">45 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/05.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        sodales lorem eget</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">57 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/08.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Mauris
                                                                        fermentum rhoncus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">89 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->

                                                <div class="tab-pane" id="restaurants">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/03.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        malesuada consectetur hendrerit</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">23 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Suspendisse
                                                                        adipiscing, tellus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">51 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/09.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Quisque
                                                                        orci urna, tristique eget pharetra</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">26 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/07.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Suspendisse
                                                                        adipiscing, tellus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">73 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/09.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Vivamus
                                                                        pretium vitae libero at ultrices</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">81 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/10.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Vestibulum
                                                                        interdum velit mauris</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">93 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->

                                                <div class="tab-pane" id="transportation">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/04.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        sodales lorem eget</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">33 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/05.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Suspendisse
                                                                        adipiscing, tellus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">80 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/04.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Quisque
                                                                        orci urna, tristique eget pharetra</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">38 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/04.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        sodales lorem eget</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">41 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/08.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Aenean
                                                                        consequat ut sapien</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">81 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/08.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Praesent
                                                                        hendrerit faucibus nulla</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">70 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->

                                                <div class="tab-pane" id="wine">
                                                    <div class="reviews">
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/07.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Vivamus
                                                                        pretium vitae libero at ultrices</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">17 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Quisque
                                                                        orci urna, tristique eget pharetra</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">14 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/08.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Mauris
                                                                        fermentum rhoncus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">65 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Nunc
                                                                        elementum augue massa</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">67 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/05.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Mauris
                                                                        fermentum rhoncus</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">89 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                        <div class="review">
                                                            <div class="review-image">
                                                                <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">
                                                                    <img src="{{ URL::asset('filemanager/userfiles/data/01.jpg') }}"
                                                                         alt="">
                                                                </a>
                                                            </div><!-- /.review-image -->

                                                            <div class="review-title">
                                                                <h2>
                                                                    <a href="http://director-html.wearecodevision.com/demo/directory/detail.html">Etiam
                                                                        malesuada consectetur hendrerit</a></h2>
                                                                <div class="rating pull-left">
                                                                    <a href="{{ URL::to('/') }}#">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-half-empty"></i>
                                                                    </a>
                                                                </div>

                                                                <span class="review-count">16 reviews</span>
                                                            </div><!-- /.review-title -->

                                                            <a href="http://director-html.wearecodevision.com/demo/directory/detail.html"
                                                               class="review-read-more">
                                                                <i class="fa fa-angle-right"></i>
                                                            </a><!-- /.review-read-more -->
                                                        </div>
                                                    </div><!-- /.reviews -->
                                                </div><!-- /.tab-pane -->
                                            </div><!-- /.tab-content -->
                                        </div><!-- /.vertical-tab-right -->
                                    </div><!-- /.vertical-tabs -->                </div>

                                <div class="sidebar col-sm-3">
                                    <div class="widget">
                                        <h3>Tin mới</h3>

                                        <?php $listItem = \App\Models\House::where('status', 1)->orderBy('id', 'desc')->limit(7)->get();?>
                                        @if(count($listItem) > 0)
                                            @foreach($listItem as $item)
                                                <div class="content-row-small">
                                                    @if($item->vip == 1)
                                                        <img src="{{ URL::asset('images/icon_hot.png') }}"
                                                             class="icon_hot">
                                                    @endif
                                                    <div class="row">
                                                        <div class="content-row-small-picture-standard squared col-xs-3 col-sm-4">
                                                            <a href="{{ URL::to('bai-viet' . $item->id) }}">
                                                                <?php showImage($item->image);?>
                                                            </a>
                                                        </div>

                                                        <div class="content-row-small-body col-xs-9 col-sm-8">
                                                            <h4>
                                                                <a href="{{ URL::to('bai-viet' . $item->id) }}">{{ $item->name }}</a>
                                                            </h4>
                                                            <table>
                                                                <tr>
                                                                    <td>Giá</td>
                                                                    <td>{{ $item->price }} triệu</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Diện tích</td>
                                                                    <td>{{ $item->acreage }} m2</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Địa chỉ</td>
                                                                    <td>{{ $item->address }}</td>
                                                                </tr>
                                                            </table>
                                                            <div class="content-row-small-meta">
                                                                {{ date('H:i d/m/Y', strtotime($item->updated_at)) }}
                                                            </div>
                                                            <!-- /.content-row-small-meta -->
                                                        </div><!-- /.content-row-body -->
                                                    </div><!-- /.row -->
                                                </div><!-- /.content-row-small -->
                                            @endforeach
                                        @endif

                                        <a href="{{ URL::to('/') }}"
                                           class="read-more">Xem thêm <i class="fa fa-angle-double-right"></i></a>
                                    </div><!-- /.widget -->
                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.block-content-inner -->
                    </div><!-- /.block-content -->

                    <div class="block-content background-secondary background-technology fullwidth">
                        <div class="block-content-inner">
                            <div class="merged-boxes clearfix">
                                <div class="merged-box col-sm-4">
                                    <div class="merged-box-content clearfix">
                                        <h3><i class="fa fa-windows color-windows"></i> Win Phone</h3>

                                        <p>
                                            Nullam in sodales metus, non scelerisque tellus. Quisque a volutpat nunc.
                                            Donec laoreet diam.
                                        </p>

                                        <a href="{{ URL::to('/') }}#"
                                           class="merged-box-read-more">Tải ứng dụng <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div><!-- /.merged-box -->

                                <div class="merged-box col-sm-4">
                                    <div class="merged-box-content clearfix">
                                        <h3><i class="fa fa-apple color-apple"></i> iPhone</h3>

                                        <p>
                                            Nullam in sodales metus, non scelerisque tellus. Quisque a volutpat nunc.
                                            Donec laoreet diam.
                                        </p>

                                        <a href="{{ URL::to('/') }}#"
                                           class="merged-box-read-more">Tải ứng dụng <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div><!-- /.merged-box -->

                                <div class="merged-box col-sm-4">

                                    <div class="merged-box-content clearfix">
                                        <h3><i class="fa fa-android color-android"></i> Android</h3>

                                        <p>
                                            Nullam in sodales metus, non scelerisque tellus. Quisque a volutpat nunc.
                                            Donec laoreet diam.
                                        </p>

                                        <a href="{{ URL::to('/') }}#"
                                           class="merged-box-read-more">Tải ứng dụng <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div><!-- /.merged-box -->
                            </div><!-- /.merged-boxes -->
                        </div><!-- /.block-content-inner -->
                    </div><!-- /.block-content -->
                    <div class="block-content directory-carousel-wrapper fullwidth background-gray">
                        <div class="block-content-inner carousel">
                            <h3 class="center mb40 widgetized-title">Dự án bất động sản nổi bật mới nhất</h3>

                            <div class="bx-wrapper" style="max-width: 1110px;">
                                <div class="bx-viewport"
                                     style="width: 100%; overflow: hidden; position: relative; height: 415px;">
                                    <?php $listItem = \App\Models\Post::where('status', 1)->where('category_id', 11)->orderBy('id', 'desc')->limit(8)->get();?>
                                    @if(count($listItem) > 0)
                                        @foreach($listItem as $item)
                                            <div class="box background-white"
                                                 style="float: left; list-style: none; position: relative; width: 257px; margin-right: 30px;">
                                                <div class="box-picture">
                                                    <a href="{{ URL::to($item->category->slug .'/'. $item->slug) }}">
                                                        <img src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                                             alt="{{ $item->name }}">
                                                        <span></span>
                                                    </a>
                                                </div><!-- /.box-picture -->

                                                <div class="box-body">
                                                    <h2 class="box-title-plain">
                                                        <a href="{{ URL::to($item->category->slug .'/'. $item->slug) }}">{{ $item->name }}</a>
                                                        <small class="clearfix">{{ $item->intro }}</small>
                                                    </h2><!-- /.box-title-plain -->

                                                    <div class="box-content">
                                                        <div class="box-meta">
                                                            <div class="rating center">
                                                                <a href="{{ URL::to('/') }}#">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star-half-empty"></i>
                                                                </a>
                                                            </div><!-- /.rating -->
                                                        </div>
                                                    </div><!-- /.box-content -->
                                                </div><!-- /.box-body -->
                                            </div><!-- /.box -->
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div><!-- /.bxslider -->
                    </div><!-- /.block-content-inner -->
                </div><!-- /.block-content -->
                <div class="block-content background-map background-white fullwidth">
                    <div class="block-content-inner">
                        <div class="row">
                            <div class="col-sm-3 center">
                                <i class="large-icon color-primary entypo paper-plane"></i>
                                <h4>Unique Design</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna</p>
                            </div>

                            <div class="col-sm-3 center">
                                <i class="large-icon color-primary entypo archive"></i>
                                <h4>Perfect Files</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna</p>
                            </div>

                            <div class="col-sm-3 center">
                                <i class="large-icon color-primary entypo star"></i>
                                <h4>Creative Layout</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna</p>
                            </div>

                            <div class="col-sm-3 center">
                                <i class="large-icon color-primary entypo lifebuoy"></i>
                                <h4>Professional Support</h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna</p>
                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.block-content-inner -->
                </div><!-- /.block-content -->
            </div><!-- /.container -->

        </div><!-- /#main-inner -->
    </div>

@stop
