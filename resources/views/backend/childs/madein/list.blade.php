@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.madein') }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--Sidebar--}}
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.add_something', [':something' => trans('system.madein')]) }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="add-madein">
                                    <div class="form-group">
                                        <label>{{ trans('form.name') }}</label>
                                        <input class="form-control" name="name" required>

                                        <p class="help-block">{{ trans('form.name_des') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <div class="thumbwrapper">
                                            <div class="previewshow previewshow-home" style="display: none;">
                                                <div class="imagepr_wrap imagepr_wrap-home"></div>
                                                <div class="thumbactions" data-thumb="home">
                                                    <a class="button button-red deleteimage" data-action="remove"
                                                       data-target="thumb"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('form.description') }}</label>
                                        <textarea class="form-control" name="intro"></textarea>

                                        <p class="help-block">{{ trans('form.description_des') }}</p>
                                    </div>
                                    <textarea class="tinymce" style="display: none;"></textarea>
                                    <input type="hidden" value="" name="id">
                                    <button class="btn btn-primary create_madein">{{ trans('form.btn_create') }}</button>
                                    <button class="btn btn-primary edit_madein">{{ trans('form.btn_edit') }}</button>
                                    <button class="btn btn-default reset"
                                            onclick="resetForm('.add-madein')">{{ trans('form.btn_reset') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name">{{ trans('product.list_madein') }}</div>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">{{ trans('system.actions') }}</option>
                                <option value="delete">{{ trans('form.delete') }}</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('form.description') }}</th>
                                        <th>{{ trans('form.created_at') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php ?>
                                    @if($listMadein)
                                        @foreach($listMadein as $item)
                                            <tr>
                                                <td><input type="checkbox" class="ids" name="id[]"
                                                           value="{{ $item->id }}"></td>
                                                <td class="item-name">{{ $item->name }}</td>
                                                <td class="item-intro">{{ $item->intro }}</td>
                                                <td class="item-created_at">{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                                <td style="display: none"
                                                    class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        {{--END: Sidebar--}}

        {{--Main--}}
        <div class="row">
            <div class="col-lg-8">

            </div>
        </div>
        {{--END: Main--}}
    </div>

    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'madein'])

@stop

@section('custom_header')
    <link href="{{ URL::asset('css/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('js/partials/initFormTable.js') }}"></script>

    <script>
        // DataTable
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });

        function getData() {
            var data = {
                id: $('input[name=id]').val(),
                name: $('input[name=name]').val() === '' ? swal({
                    title: 'Name required',
                    type: 'error'
                }) : $('input[name=name]').val(),
                intro: $('textarea[name=intro]').val(),
                _token: '{{ csrf_token() }}'
            };
            return data;
        }

        // Create madein
        $('.create_madein').click(function () {

            var formData = getData();

            if ($('input[name=name]').val() != '' && $('input[name=slug]').val() != '') {
                $.ajax({
                    url: '{{ route('madein.postCreate') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    success: function (resp) {
                        if (resp.status == 'success') {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                            // Add new element to table
                            $('table tbody').html('<tr role="row" class="odd"><td class="sorting_1"><input type="checkbox" class="ids" name="id[]" value="'+resp.id+'"></td> <td class="item-name">'+formData.name+'</td><td class="item-intro">'+formData.intro+'</td><td class="item-created_at">'+resp.created_at+'</td><td style="display: none" class="id id-'+resp.id+'">'+resp.id+'</td></tr>' + $('table tbody').html());
                            // Reset form
                            $('button.reset').click();
                        } else {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_create_error') }}', type: 'error'});
                    }
                });
            }

        });

        // Edit madein
        $('table').on('click', 'td.item-name', function () {
            var id = $(this).parent('tr').find('td.id').html();
            $.ajax({
                url: '{{ route('madein.getEdit') }}',
                type: 'GET',
                dataType: 'json',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (resp) {
                    if (resp.status == 'success') {
                        // Insert data to form edit
                        $('input[name=id]').val(resp.data.id);
                        $('input[name=name]').val(resp.data.name);
                        $('textarea[name=intro]').val(resp.data.intro);

                    } else {
                        swal({title: resp.status, text: resp.msg, type: resp.status});
                    }
                },
                error: function (resp) {
                    swal({title: '{{ trans('system.ajax_create_error') }}', type: 'error'});
                }
            });
        });


        $('.edit_madein').click(function () {

            var formData = getData();

            if ($('input[name=id]').val() != '' && $('input[name=name]').val() != '') {
                $.ajax({
                    url: '{{ route('madein.postEdit') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    success: function (resp) {
                        if (resp.status == 'success') {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                            // Reset form
                            $('button.reset').click();

                            var tr = $('table td.id.id-' + formData.id).parent('tr');
                            tr.find('td.item-name').html(formData.name);
                            tr.find('td.item-intro').html(formData.intro);
                            tr.find('td.item-created_at').html(resp.data.created_at);
                        } else {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            }
        });
        
    </script>

@stop