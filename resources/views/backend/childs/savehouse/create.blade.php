@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.create') }} {{ ucwords($module['name']) }}
                    <a class="btn btn-success" href="javascript:;" onclick="history.go(-1);">{{ trans('system.btn_cancel') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        {{--Create item--}}
        <form class="row add-item" method="POST" action="" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-lg-8">
                @if(isset($module['form']['left']))
                    <div class="panel panel-default">
                        <div class="panel-heading table-heading">
                            <div class="table-heading-name"
                                 style="width: 100%;">{{ trans('system.main_info') }}</div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    @foreach($module['form']['left'] as $feild)
                                        <?php if (!isset($feild['inner'])) $feild['inner'] = '';?>
                                        @include('backend.partials.form_create', ['type'=>$feild['type']])
                                    @endforeach

                                </div>

                                <div class="col-xs-12" style="padding: 0px 30px;">
                                    <button class="btn btn-primary create_item">{{ trans('form.btn_create') }}</button>
                                    <button class="btn btn-default reset" type="button"
                                            onclick="resetForm('.add-item')">{{ trans('form.btn_reset') }}</button>
                                </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                @endif
            </div>

            <div class="col-lg-4">
                @if(isset($module['form']['right']))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('system.category_info') }}
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    @foreach($module['form']['right'] as $feild)
                                        <?php if (!isset($feild['inner'])) $feild['inner'] = '';?>
                                        @include('backend.partials.form_create', ['type'=>$feild['type']])
                                    @endforeach


                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

        </form>
        {{--END: Create item--}}
    </div>
@stop
