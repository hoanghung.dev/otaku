@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.product_relate') }}
                    <a class="btn btn-success btn-add-product"
                       href="{{ route('productRelate') }}">{{ trans('form.btn_cancel') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">User lucky: {{session('success')}}</span>@endif

        {{--Create product--}}
        <div class="row add-product" style="display: block;">
            <div class="col-lg-8">
                <form method="POST" action="">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $productRelate->id }}">
                    <?php $product = \App\Models\Product::find($productRelate->product_id);?>
                    <div class="panel panel-default">
                        <div class="panel-heading table-heading">
                            <div class="table-heading-name"
                                 style="width: 100%;">{{ trans('product.product_info') }}</div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>{{ trans('product.name') }}</label>
                                        <div class="ui-widget">
                                            <input name="name" class="form-control" value="{{ $product->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.category_included') }}</label>
                                    <textarea id="included" name="included" class="tag-editor-hidden-src"
                                              readonly="readonly"
                                              style="display: block;"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.category_selling') }}</label>
                                    <textarea id="selling" name="selling" class="tag-editor-hidden-src"
                                              readonly="readonly"
                                              style="display: block;"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.category_sale') }}</label>
                                    <textarea id="sale" name="sale" class="tag-editor-hidden-src" readonly="readonly"
                                              style="display: block;"></textarea>
                                    </div>
                                    <p>write "all" if chose all category</p>
                                    <button class="btn btn-primary create_product" type="submit">{{ trans('form.btn_create') }}</button>
                                    <button class="btn btn-default reset" type="button"
                                            onclick="resetForm('.add-product')">{{ trans('form.btn_reset') }}</button>
                                </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </form>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.category_info') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">

                                @include('backend.partials.image_thumb', ['name'=>'image'])

                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        {{--END: Create product--}}
    </div>


    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'product'])
    @include('backend.partials.autocomplete', ['name' => 'name', 'type' => 'product'])
@stop

@section('custom_header')
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.tag-editor.css') }}">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.caret.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.tag-editor.js') }}"></script>

    <script>

        $('.reset').click(function () {
            $('#included').tagEditor({initialTags: []});
            $('#selling').tagEditor({initialTags: []});
            $('#sale').tagEditor({initialTags: []});
        });

        $('#included').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: {collision: 'flip'}, // automatic menu position up/down
                source: []
            },
            forceLowercase: false,
            placeholder: 'Enter product included ...'
        });

        $('#selling').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: {collision: 'flip'}, // automatic menu position up/down
                source: []
            },
            forceLowercase: false,
            placeholder: 'Enter product selling ...'
        });

        $('#sale').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: {collision: 'flip'}, // automatic menu position up/down
                source: []
            },
            forceLowercase: false,
            placeholder: 'Enter product sale ...'
        });



                <?php if($productRelate->category_included != '|'):?>
        var data = [
                    @foreach(explode('|',$productRelate->category_included) as $item)
                            <?php $cat = \App\Models\Category::find($item);?>
                            @if($cat != null) '{{ $cat->name }}', @endif
                    @endforeach
                ];
                <?php else:?>
        var data = [' '];
        <?php endif; ?>

        $('#included').tagEditor({initialTags: data});



                <?php if($productRelate->category_selling != '|'):?>
        var data = [
                    @foreach(explode('|',$productRelate->category_selling) as $item)
                    <?php $cat = \App\Models\Category::find($item);?>
                            @if($cat != null) '{{ $cat->name }}', @endif
                    @endforeach
                ];
                <?php else:?>
        var data = [' '];
        <?php endif; ?>

        $('#selling').tagEditor({initialTags: data});



                <?php if($productRelate->category_sale != '|'):?>
        var data = [
                    @foreach(explode('|',$productRelate->category_sale) as $item)
                    <?php $cat = \App\Models\Category::find($item);?>
                            @if($cat != null) '{{ $cat->name }}', @endif
                    @endforeach
                ];
                <?php else:?>
        var data = [' '];
        <?php endif; ?>

        $('#sale').tagEditor({initialTags: data});
    </script>

@stop
