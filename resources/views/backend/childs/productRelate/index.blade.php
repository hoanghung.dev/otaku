@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.product_relate') }}
                    <a class="btn btn-success btn-add-product" href="{{ URL::to('dashboard/product-relate/create') }}">{{ trans('form.btn_add') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">User lucky: {{session('success')}}</span>@endif

        {{--List product--}}
        <div class="row list-product">
            <form class="col-xs-12 top-action" method="GET" action="">
                <div class="col-md-3">
                    <input name="keyword" value="" class="form-control">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary">Search</button>
                </div>
            </form>

            <div class="col-xs-12">
                <div class="panel panel-default list-product">
                    <div class="panel-heading">
                        <span>{{ trans('product.list_product') }}</span>
                        <ul class="subsubsub">
                            <?php
                            $count_all_product = \App\Models\ProductRelate::all()->count();
                            ?>
                            <li class="all"><span><span data-type="all">All</span> <span
                                            class="count">({{ $count_all_product }})</span></span>&nbsp;|&nbsp;
                            </li>
                        </ul>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">Actions</option>
                                <option value="publish">Publish</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if($listProduct)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.image') }}</th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('product.category_included') }}</th>
                                        <th>{{ trans('product.category_sale') }}</th>
                                        <th>{{ trans('product.category_selling') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listProduct as $item)
                                        <?php
                                        $product = \App\Models\Product::find($item->id);
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="ids" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td class="item-image"><img
                                                        src="{{ URL::asset('filemanager/userfiles/' . $product->image) }}"
                                                        style="max-width: 97px;"/></td>
                                            <td class="item-name"><a href="{{ URL::to('dashboard/product-relate/' . $item->id) }}">{{ $product->name }}</a></td>
                                            <?php
                                                $string = '';
                                                if($item->category_included != '|') {
                                                    foreach (explode('|',$item->category_included) as $i) {
                                                        $cat = \App\Models\Category::find($i);
                                                        if($cat != null) $string .= $cat->name . ', ';
                                                    }
                                                }
                                            ?>
                                            <td class="item-code">{{ $string }}</td>
                                            <?php
                                            $string = '';
                                            if($item->category_sale != '|') {
                                                foreach (explode('|',$item->category_sale) as $i) {
                                                    $cat = \App\Models\Category::find($i);
                                                    if($cat != null) $string .= $cat->name . ', ';
                                                }
                                            }
                                            ?>
                                            <td class="item-code">{{ $string }}</td>
                                            <?php
                                            $string = '';
                                            if($item->category_selling != '|') {
                                                foreach (explode('|',$item->category_sale) as $i) {
                                                    $cat = \App\Models\Category::find($i);
                                                    if($cat != null) $string .= $cat->name . ', ';
                                                }
                                            }
                                            ?>
                                            <td class="item-code">{{ $string }}</td>
                                            <td style="display: none;"
                                                class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- /.panel-body -->

                    {{--Paginate--}}
                    {!! $listProduct->links() !!}
                </div>
            </div>
        </div>
        {{--END: List product--}}
    </div>

    @include('backend.partials.delete_publish_table', ['name' => 'productRelate'])

@stop