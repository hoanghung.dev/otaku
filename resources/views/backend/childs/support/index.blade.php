@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Hỗ trợ</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Support
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="">
                            {{ csrf_field() }}
                            @foreach($listSupport as $key => $value)
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{ trans('product.name') }}</label>
                                        <input class="form-control" name="name{{ $value->id }}"
                                               value="{{ $value->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" name="email{{ $value->id }}"
                                               value="{{ $value->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label>SĐT</label>
                                        <input class="form-control" name="tel{{ $value->id }}"
                                               value="{{ $value->tel }}">
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="form-group">
                                        <label>Ảnh đại diện</label>
                                        @include('backend.partials.image_thumb', ['name' => 'image' . $value->id])
                                    </div>
                                    <div class="icon-delete" data-id="{{ $value->id }}"><p class="fa fa-times"></p></div>
                                </div>
                            @endforeach

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>{{ trans('product.name') }}</label>
                                    <input class="form-control" name="name"
                                           value="">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="email"
                                           value="">
                                </div>
                                <div class="form-group">
                                    <label>SĐT</label>
                                    <input class="form-control" name="tel"
                                           value="">
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="form-group">
                                    <label>Ảnh đại diện</label>
                                    @include('backend.partials.image_thumb', ['name' => 'image'])
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success create-slider" name="create" value="{{ trans('form.btn_create') }}">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <input type="submit"
                                        class="btn btn-primary" value="{{ trans('form.btn_save') }}" name="edit">
                            </div>
                            <!-- /.row (nested) -->
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

    </div>


@stop

@section('custom_footer')
    <script>
        @foreach($listSupport as $item)
            $('.div0-image{{ $item->id }}').find('.wrap-thumbnail').html('<img src="http://' + location.host + '/filemanager/userfiles/' + '{{ $item->image }}">');
        @if($item->image != '')
            $('.thumb .div1-image{{ $item->id }}').css('display', 'block');
        $('.thumb .div2-image{{ $item->id }}').css('display', 'none');
        $('input[name=image{{ $item->id }}]').val('{{ $item->image }}');
        @endif
        @endforeach
    </script>

    <script>
        $('.icon-delete').each(function () {
            $(this).click(function () {
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('support.postDelete') }}',
                    type: 'POST',
                    data: { id: id, _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        location.reload();
                    },
                    error: function (resp) {

                    }
                });
            });
        });
    </script>
@stop