@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.order') }}
                    <button class="btn btn-success btn-bill" onclick="addproduct()" status="off">Cancel</button>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">User lucky: {{session('success')}}</span>@endif

        {{--List bill--}}
        <div class="row list-bill">
            <form class="col-xs-12 top-action" method="GET" action="">
                <div class="col-md-2">
                    <select name="search_date" class="form-control">
                        <option value="0">{{ trans('form.all_date') }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 1 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 1 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 2 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 2 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 3 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 3 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 4 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 4 month')) }}</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="search_status" class="form-control">
                        <option value="">{{ trans('post.status') }}</option>
                        <option value="publish">{{ trans('post.publish') }}</option>
                        <option value="draf">{{ trans('post.draf') }}</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input name="keyword" value="" class="form-control">
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary">Search</button>
                </div>
            </form>

            <div class="col-xs-12">
                <div class="panel panel-default list-bill">
                    <div class="panel-heading">
                        <span>{{ trans('product.list_bill') }}</span>
                        <ul class="subsubsub">
                            <?php
                            $count_all_bill = \App\Models\Bill::get()->count();
                            ?>
                            <li class="all"><span><span>All</span> <span class="count">({{ $count_all_bill }})</span></span>&nbsp;|&nbsp;
                            </li>
                            <li class="publish"><span><span>Published</span> <span
                                            class="count">({{ count($listBill) }})</span></span>&nbsp;|&nbsp;</li>
                            <li class="draft"><span><span>Drafts</span> <span
                                            class="count">({{ $count_all_bill - count($listBill) }})</span></span>
                            </li>
                        </ul>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">Actions</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if($listBill)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('product.total_price') }}</th>
                                        <th>{{ trans('product.receipt_method') }}</th>
                                        <th>{{ trans('product.date') }}</th>
                                        <th>{{ trans('product.address') }}</th>
                                        <th>{{ trans('product.status') }}</th>
                                        <th>{{ trans('form.created_at') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listBill as $item)
                                        <tr>
                                            <td><input type="checkbox" class="ids" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td class="item-name">{{ $item->name }}</td>
                                            <td class="item-base_price">{!! number_format($item->total_price,0,",",".") !!}
                                                đ
                                            </td>
                                            <td>{{ $item->receipt_method }}</td>
                                            <td>{{ date('d/m/Y', strtotime($item->date)) }}</td>
                                            <td>{{ $item->address }}</td>
                                            <td>{{ $item->status }}</td>
                                            <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                            <td style="display: none;"
                                                class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- /.panel-body -->

                    {{--Paginate--}}
                    {!! $listBill->links() !!}
                </div>
            </div>
        </div>

        {{--END: List bill--}}
        <div class="row add-bill" style="display: none;">

        </div>
    </div>


    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'order'])

@stop


@section('custom_footer')
    <script src="{{ URL::asset('js/partials/initFormTable.js') }}"></script>

    <script>
        function addproduct(status) {
            $('.list-bill').slideToggle(200);
            $('.add-bill').slideToggle(200);
        }

        // ajax find bill
        $('table').on('click', 'td.item-name', function () {

            var id = $(this).parent('tr').find('td.id').html();
            $.ajax({
                url: '{{ route('order.postFindBill') }}',
                type: 'POST',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (resp) {
                    $('.btn-bill').click();
                    $('.add-bill').html(resp);
                },
                error: function (resp) {
                    swal({title: '{{ trans('system.ajax_create_error') }}', type: 'error'});
                }
            });
        });
    </script>

@stop