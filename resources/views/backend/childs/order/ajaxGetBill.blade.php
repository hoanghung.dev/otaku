@if($bill)
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading table-heading">
                <div class="table-heading-name" style="width: 100%;">{{ trans('product.customer_info') }}</div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>{{ trans('form.name') }}</td>
                        <td>{{ $bill->name }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('product.sex') }}</td>
                        <td>{{ $bill->sex }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('product.tel') }}</td>
                        <td>{{ $bill->tel }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('product.email') }}</td>
                        <td>{{ $bill->email }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('product.address') }}</td>
                        <td>{{ $bill->address }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="panel panel-default">
            <div class="panel-heading table-heading">
                <div class="table-heading-name" style="width: 100%;">{{ trans('product.order_info') }}</div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @if($listOrder)
                    <table class="table">
                        <thead>
                        <tr>
                            <td>{{ trans('form.image') }}</td>
                            <td>{{ trans('form.name') }}</td>
                            <td>{{ trans('product.value') }}</td>
                            <td>{{ trans('product.price') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total_price = 0;?>
                        @foreach($listOrder as $item)
                            <?php $total_price += $item->price;?>
                            <?php $product = $item->product;?>
                            <tr>
                                <td><img src="{{ URL::asset('filemanager/userfiles/'.$product->image) }}"></td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $item->value }}</td>
                                <td>{{ number_format($item->price,0,",",".") }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>{{ trans('product.total_price') }}</td>
                            <td>{{ number_format($total_price,0,",",".") }}</td>
                        </tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endif