@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.product') }}
                    <a class="btn btn-success" href="{{ route('product') }}">{{ trans('form.btn_cancel') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        {{--Create product--}}
        <form class="row add-product" method="POST" action="" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name" style="width: 100%;">{{ trans('product.product_info') }}</div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('form.name') }}</label>
                                    <input class="form-control" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.slug') }}</label>
                                    <input class="form-control" name="slug" required>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('product.code') }}</label>
                                    <input class="form-control" name="code">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>{{ trans('product.base_price') }}</label>
                                            <input type="number" class="form-control" name="base_price" value="0">
                                        </div>
                                        <div class="col-md-6">
                                            <label>{{ trans('product.sale') }}</label>
                                            <select class="form-control" name="sale_id">
                                                <option value="0">Không giảm giá</option>
                                                @if($listSale != null)
                                                    @foreach($listSale as $item)
                                                        <option value="{{ $item->id }}"
                                                                class="sale-id-{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>{{ trans('product.final_price') }}</label>
                                            <input type="number" class="form-control" name="final_price" value="0">
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{ trans('product.tax_vat') }}</label>
                                                <input type="checkbox" class="form-control" name="tax_vat">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>{{ trans('product.guarantee') }}</label>
                                            <input type="number" class="form-control" name="guarantee" value="0">
                                        </div>
                                        <div class="col-md-4">
                                            <label>{{ trans('product.manufacturer') }}</label>
                                            <select name="manufacturer_id" class="form-control">
                                                @if($listManufacturer)
                                                    @foreach($listManufacturer as $manufacturer)
                                                        <option class="manufacturer-id-{{ $manufacturer->id }}"
                                                                value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ trans('product.madein') }}</label>
                                                <select name="madein_id" class="form-control">
                                                    @if($listMadein)
                                                        @foreach($listMadein as $madein)
                                                            <option class="madein-id-{{ $madein->id }}"
                                                                    value="{{ $madein->id }}">{{ $madein->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('product.sale_description') }}</label>
                                    <textarea class="form-control" name="sale_description" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.description') }}</label>
                                    <textarea class="form-control" name="intro" rows="5"></textarea>
                                </div>
                                <div class="form-group editor">
                                    <label>{{ trans('form.content') }}</label>
                                    <textarea class="form-control content" rows="15"
                                              name="content"></textarea>
                                </div>
                                <div class="form-group editor">
                                    <label>Thông số kỹ thuật</label>
                                    <textarea class="form-control content" rows="15"
                                              name="specifications"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Core</label>
                                            <select name="core" class="form-control">
                                                <option>Chọn Core</option>
                                                <option value="Atom 1.8">Atom 1.8</option>
                                                <option value="Atom 2.13">Atom 2.13</option>
                                                <option value="Core 2 Duo 2.6">Core 2 Duo 2.6</option>
                                                <option value="I3">I3</option>
                                                <option value="I5">I5</option>
                                                <option value="I7">I7</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Hệ điều hành</label>
                                            <select name="system" class="form-control">
                                                <option>Chọn hệ điều hành</option>
                                                <option value="Windows 7">Windows 7</option>
                                                <option value="Pos Ready">Pos Ready</option>
                                                <option value="Dos">Dos</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>RAM</label>
                                            <select name="ram" class="form-control">
                                                <option>Chọn RAM</option>
                                                <option value="1 Gb">1 Gb</option>
                                                <option value="2 Gb">2 Gb</option>
                                                <option value="4 Gb">4 Gb</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Chip</label>
                                            <select name="chip" class="form-control">
                                                <option>Chọn chip</option>
                                                <option value="Atom 1.8">Atom 1.8</option>
                                                <option value="Atom 2.13">Atom 2.13</option>
                                                <option value="chip 2 Duo 2.6">chip 2 Duo 2.6</option>
                                                <option value="I3">I3</option>
                                                <option value="I5">I5</option>
                                                <option value="I7">I7</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Ổ cứng</label>
                                            <select name="hard" class="form-control">
                                                <option>Chọn ổ cứng</option>
                                                <option value="HDD">HDD</option>
                                                <option value="SSD">SSD</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Video</label>
                                    <input class="form-control" name="video">
                                </div>
                                {{--<div class="form-group">
                                    <label>Link tài liệu</label>
                                    <input class="form-control" name="iframe">
                                </div>--}}
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="status" checked> {{ trans('form.publish') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.seo_title') }}</label>
                                    <input class="form-control" name="seo_title">
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.seo_description') }}</label>
                                    <input class="form-control" name="seo_description">
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('product.seo_keywords') }}</label>
                                    <input class="form-control" name="seo_keywords">
                                </div>
                                <div class="form-group">
                                </div>
                                <input type="hidden" value="" name="id">
                                <button class="btn btn-primary create_product">{{ trans('form.btn_create') }}</button>
                                <button class="btn btn-default reset" type="button"
                                        onclick="resetForm('.add-product')">{{ trans('form.btn_reset') }}</button>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.category_info') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">

                                @include('backend.partials.image_thumb_default', ['name'=>'image'])

                                <label>Ảnh liên quan</label>
                                @include('backend.partials.image_extend_default', ['name'=>'image_extra', 'multiple' => true])

                                <label>File PDF</label>
                                @include('backend.partials.image_extend_default', ['name'=>'iframe'])

                                <div class="form-group">
                                    <label>{{ trans('system.tags') }}</label>
                                    <textarea id="tags" name="tags" class="tag-editor-hidden-src" readonly="readonly"
                                              style="display: block;"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('system.category') }}</label>
                                    <select class="form-control" name="category_id">
                                        @foreach($listCategory as $item)
                                            @if($item->parent_id == 0)
                                                {{--Option parent--}}
                                                <option value="{{ $item->id }}"
                                                        class="category-id-{{ $item->id }}">{{ $item->name }}</option>
                                                <?php
                                                $listChilds = $item->childs;?>
                                                @if($listChilds != null)
                                                    @foreach($listChilds as $one)
                                                        {{--Option child level 1--}}
                                                        <option value="{{ $one->id }}"
                                                                class="category-id-{{ $one->id }}">
                                                            --{{ $one->name }}</option>
                                                        <?php
                                                        $listChilds2 = $one->childs; ?>
                                                        @if($listChilds2)
                                                            @foreach($listChilds2 as $one2)
                                                                {{--Option child level 2--}}
                                                                <option value="{{ $one2->id }}"
                                                                        class="category-id-{{ $one2->id }}">
                                                                    ----{{ $one2->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('product.category_orther') }}</label>
                                    <select multiple class="form-control" name="category_orther_id[]">
                                        <option value="0" class="category-orther-id-0">-- Null --</option>
                                        @foreach($listCategory as $item)
                                            @if($item->parent_id == 0)
                                                {{--Option parent--}}
                                                <option value="{{ $item->id }}"
                                                        class="category-orther-id-{{ $item->id }}">{{ $item->name }}</option>
                                                <?php
                                                $listChilds = $item->childs;?>
                                                @if($listChilds != null)
                                                    @foreach($listChilds as $one)
                                                        {{--Option child level 1--}}
                                                        <option value="{{ $one->id }}"
                                                                class="category-orther-id-{{ $one->id }}">
                                                            --{{ $one->name }}</option>
                                                        <?php
                                                        $listChilds2 = $one->childs;?>
                                                        @if ($listChilds2)
                                                            @foreach($listChilds2 as $one2)
                                                                {{--Option child level 2--}}
                                                                <option value="{{ $one2->id }}"
                                                                        class="category-orther-id-{{ $one2->id }}">
                                                                    ----{{ $one2->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

        </form>
        {{--END: Create product--}}
    </div>

@stop

@section('custom_header')
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.tag-editor.css') }}">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.caret.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.tag-editor.js') }}"></script>
    <script src="{{ URL::asset('js/partials/initFormTable.js') }}"></script>

    <script>
        $('#tags').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: {collision: 'flip'}, // automatic menu position up/down
                source: []
            },
            forceLowercase: false,
            placeholder: 'Enter tags ...'
        });
    </script>

@stop
