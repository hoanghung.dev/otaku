@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.product') }}
                    <a class="btn btn-success" href="{{ route('product.getCreate') }}">{{ trans('form.btn_add') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--List product--}}
        <div class="row list-product">
            <form class="col-xs-12 top-action" method="GET" action="">
                <div class="col-md-2">
                    <select name="search_date" class="form-control">
                        <option value="0">{{ trans('form.all_date') }}</option>
                        <option value="{{ date('Y-m', strtotime(date('M Y') . '- 1 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 1 month')) }}</option>
                        <option value="{{ date('Y-m', strtotime(date('M Y') . '- 2 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 2 month')) }}</option>
                        <option value="{{ date('Y-m', strtotime(date('M Y') . '- 3 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 3 month')) }}</option>
                        <option value="{{ date('Y-m', strtotime(date('M Y') . '- 4 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 4 month')) }}</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="search_category" class="form-control">
                        <option value="0">{{ trans('form.all_category') }}</option>
                        @foreach($listCategory as $item)
                            @if($item->parent_id == 0)
                                {{--Option parent--}}
                                <option value="{{ $item->id }}"
                                        class="parent-id-{{ $item->id }}">{{ $item->name }}</option>
                                <?php
                                $listChilds = $item->childs; ?>
                                @if($listChilds !=null)
                                    @foreach($listChilds as $one)
                                        {{--Option child level 1--}}
                                        <option value="{{ $one->id }}"
                                                class="parent-id-{{ $one->id }}">
                                            --{{ $one->name }}</option>
                                        <?php
                                        $listChilds2 = $one->childs;?>
                                        @if($listChilds2 != null)
                                            @foreach($listChilds2 as $one2)
                                                {{--Option child level 2--}}
                                                <option value="{{ $one2->id }}"
                                                        class="parent-id-{{ $one2->id }}">
                                                    ----{{ $one2->name }}</option>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="search_status" class="form-control">
                        <option value="">{{ trans('post.status') }}</option>
                        <option value="1">{{ trans('post.publish') }}</option>
                        <option value="0">{{ trans('post.draf') }}</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input name="keyword" value="" class="form-control">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary">Search</button>
                </div>
            </form>

            <div class="col-xs-12">
                <div class="panel panel-default list-product">
                    <div class="panel-heading">
                        <span>{{ trans('product.list_product') }}</span>
                        <ul class="subsubsub">
                            <?php
                            $count_all_product = \App\Models\Product::withTrashed()->get()->count();
                            ?>
                            <li class="all"><span><span data-type="all">All</span> <span
                                            class="count">({{ $count_all_product }})</span></span>&nbsp;|&nbsp;
                            </li>
                            <li class="publish"><span><span data-type="published">Published</span> <span
                                            class="count">({{ count($listProduct) }})</span></span>&nbsp;|&nbsp;</li>
                            <li class="draft"><span><span data-type="draf">Drafts</span> <span
                                            class="count">({{ $count_all_product - count($listProduct) }})</span></span>
                            </li>
                        </ul>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">Actions</option>
                                <option value="publish">Publish</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if($listProduct)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.image') }}</th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('product.code') }}</th>
                                        <th>{{ trans('system.category') }}</th>
                                        <th>{{ trans('product.base_price') }}</th>
                                        <th>{{ trans('product.final_price') }}</th>
                                        <th>{{ trans('form.author') }}</th>
                                        <th>{{ trans('system.status') }}</th>
                                        <th>{{ trans('form.view') }}</th>
                                        <th>{{ trans('form.created_at') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listProduct as $item)
                                        <tr>
                                            <td><input type="checkbox" class="ids" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td class="item-image"><img
                                                        src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                                        style="max-width: 97px;"/></td>
                                            <td class="item-name">@if(isset($item->category))<a href="{{ route('product.getEdit', ['id'=>$item->id]) }}">{{ $item->name }}</a>@endif</td>
                                            <td class="item-code">{{ $item->code }}</td>
                                            <td class="item-category">@if(isset($item->category)){{ $item->category->name }}@endif</td>
                                            <td class="item-base_price">{!! number_format($item->base_price,0,",",".") !!}
                                                đ
                                            </td>
                                            <td class="item-final_price">{!! number_format($item->final_price,0,",",".") !!}
                                                đ
                                            </td>
                                            <td>{{ $item->user->name }}</td>
                                            <th style="text-align: center;" id="img-{{ $item->id }}"><img
                                                        data-id="{{ $item->id }}" class="publish"
                                                        style="cursor:pointer;"
                                                        src="@if($item->status==1){{ URL::asset('images/published.png') }}@else{{ URL::asset('images/unpublish.png') }}@endif">
                                            </th>
                                            <td>@if(isset($item->category))<a href="{{ URL::to($item->category->slug.'/'.$item->slug) }}"
                                                   target="_blank">{{ trans('system.view') }}</a>@endif</td>
                                            <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                            <td style="display: none;"
                                                class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- /.panel-body -->

                    {{--Paginate--}}
                    {!! $listProduct->appends($appends)->links() !!}
                </div>
            </div>
        </div>
        {{--END: List product--}}
    </div>


    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'product'])

@stop

@section('custom_footer')

    <script>

        // Chose All/ publish/ draf
        $('.subsubsub li span span:nth-child(1), .subsubsub li span span:nth-child(1)').each(function () {
            $(this).click(function () {
                if (href.indexOf('?') == -1) window.location.href = href.replace('&' + $(this).data('type') + '=1', '') + '?' + $(this).data('type') + '=1';
                else window.location.href = href.replace('?' + $(this).data('type') + '=1', '') + '&' + $(this).data('type') + '=1';
            });
        });

        // Publish
        $('table img.publish').each(function () {
            $(this).click(function () {
                var id = $(this).data('id');

                if (id != '') {
                    $.ajax({
                        url: '{{ route('product.postPublish') }}',
                        type: 'post',
                        dataType: 'json',
                        data: {id: id, _token: '{{ csrf_token() }}'},
                        success: function (resp) {
                            if (resp.status == 'success') {
                                if (resp.status_publish == 1) $('#img-' + id).find('img').attr('src', '{{ URL::asset('images/published.png') }}');
                                else if (resp.status_publish == 0) $('#img-' + id).find('img').attr('src', '{{ URL::asset('images/unpublish.png') }}');
                            } else {
                                swal({title: resp.status, text: resp.msg, type: resp.status});
                            }
                        },
                        error: function (resp) {
                            swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                        }
                    });
                }
            });
        });
    </script>

@stop