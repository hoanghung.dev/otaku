@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.user') }}
                    <a class="btn btn-success btn-add-user" href="{{ URL::to('dashboard/user') }}">Quay lại</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}
        @if(session('success')) <span class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
        {{--Create user--}}
        <div class="row add-user" style="display: block;">
            <form action="" method="POST">
                {{ csrf_field() }}
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading table-heading">
                            <div class="table-heading-name">Thông tin tài khoản</div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>{{ trans('user.name') }}</label>
                                        <input class="form-control" name="name" value="{{ $user->name }}" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>{{ trans('user.sex') }}</label>
                                                <select name="sex" class="form-control">
                                                    <option value="1">Nam</option>
                                                    <option value="2" @if($user->sex == 2){{ 'selected' }}@endif>
                                                        Nữ
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{ trans('user.birthday') }}</label>
                                                <input name="birthday" type="date" class="form-control"
                                                       data-date-format="DD/MMMM/YYYY" required
                                                       value="{{ date('m-d-Y', strtotime($user->birthday)) }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input name="email" type="email" class="form-control" required
                                               value="{{ $user->email }}">
                                    </div>

                                    <div class="form-group">
                                        <label>{{ trans('user.role_user') }}</label>
                                        <select name="role_user" class="form-control">
                                            <option value="admin">Admin</option>
                                            <option value="sale" @if($user->is('sale')){{ 'selected' }}@endif>Sale
                                            </option>
                                            <option value="support" @if($user->is('support')){{ 'selected' }}@endif>
                                                Support
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"
                                                       name="status" @if($user->status == 'active'){{ 'checked' }}@endif>
                                                Kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>{{ trans('user.tel') }}</label>
                                                <input class="form-control" name="tel" value="{{ $user->tel }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label>{{ trans('user.password') }}</label>
                                                <input type="password" class="form-control" name="password" placeholder="******" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Xác nhận mật khẩu</label>
                                                <input type="password" class="form-control" name="repassword" placeholder="******" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('user.address') }}</label>
                                        <input class="form-control" name="address" value="{{ $user->address }}">
                                    </div>
                                    <button class="btn btn-primary create_user">{{ trans('form.btn_edit') }}</button>
                                    <a class="btn btn-default reset"
                                       href="{{ route('user') }}">{{ trans('form.btn_cancel') }}</a>
                                </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('system.category_info') }}
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    @include('backend.partials.image_thumb', ['name' => 'avatar'])

                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </form>
        </div>
        {{--END: Create user--}}
    </div>

@stop

@section('custom_footer')
    <script>
        $('.div0-avatar').find('.wrap-thumbnail').html('<img src="http://' + location.host + '/public/filemanager/userfiles/' + '{{ $user->avatar }}">');
        @if($user->avatar != '')
            $('.thumb .div1-avatar').css('display', 'block');
        $('.thumb .div2-avatar').css('display', 'none');
        $('input[name=avatar]').val('{{ $user->avatar }}');
        @endif
    </script>

@stop