@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.user') }}
                    <a class="btn btn-success btn-add-user"
                       href="{{ URL::to('user/create') }}">{{ trans('form.btn_add') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--List user--}}
        <div class="row list-user">
            @if(session('success')) <span class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
            @if(session('error')) <span class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
            <form class="col-xs-12 top-action" method="get" action="">
                <div class="col-md-3 datetime-action">
                    <input name="date_start" type="datetime-local" value="{{ date('d-m-Y 0:0:0',time()) }}" class="form-control">
                </div>
                <div class="col-md-3 datetime-action">
                    <input name="date_end" type="datetime-local" value="{{ date('d-m-Y H:m:i',time()) }}" class="form-control">
                </div>
                {{--<div class="col-md-2">
                    <select name="search_date" class="form-control">
                        <option value="0">{{ trans('form.all_date') }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 1 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 1 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 2 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 2 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 3 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 3 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 4 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 4 month')) }}</option>
                    </select>
                </div>--}}
                {{--<div class="col-md-2">
                    <select name="role" class="form-control">
                        <option value="0">Quyền</option>
                        <option value="user">Người dùng</option>
                        <option value="duocsi">Dược sĩ</option>
                        <option value="bacsi">Bác sĩ</option>
                        <option value="nguoiduyet">Người duyệt</option>
                        <option value="quantri">Quản trị</option>
                    </select>
                </div>--}}
                <div class="col-md-2">
                    <input name="keyword" value="" placeholder="Từ khóa" class="form-control">
                </div>
                <div class="col-md-2">
                    <select name="search_type" class="form-control">
                        <option value="name">Lọc theo</option>
                        <option value="name">Tên</option>
                        <option value="sex">Giới tính</option>
                        <option value="email">Email</option>
                        <option value="tel">Điện thoại</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary">Search</button>
                </div>
            </form>

            <div class="col-xs-12">
                <div class="panel panel-default list-user">
                    <div class="panel-heading">
                        <span>{{ trans('system.list_user') }}</span>
                        <ul class="subsubsub">
                            <li class="all"><span><span>Tất cả</span> <span class="count">({{ $countTotal }})</span></span>&nbsp;|&nbsp;
                            </li>
                        </ul>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">Actions</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if($listUser)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value=""></th>
                                        <th>{{ trans('form.image') }}</th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('form.role_user') }}</th>
                                        <th>{{ trans('form.birthday') }}</th>
                                        <th>{{ trans('form.created_at') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listUser as $item)
                                        <tr>
                                            <td><input type="checkbox" class="ids" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td>
                                                @if($item->avatar != '')
                                                    <img class="avatar"
                                                         src="{{ URL::asset('filemanager/userfiles/' . $item->avatar) }}">
                                                @else
                                                    <img class="avatar" src="{{ URL::asset('images/no-avatar.jpg') }}">
                                                @endif
                                            </td>
                                            <td class="item-name"><a
                                                        href="{{ URL::to('dashboard/user/' . $item->id) }}">{{ $item->name }}</a>
                                            </td>
                                            <td>
                                                @if($item->is('admin'))
                                                    Admin
                                                @elseif($item->is('sale'))
                                                    Sale
                                                @elseif($item->is('support'))
                                                    Support
                                                @endif
                                            </td>
                                            <td>{{ date('d/m/Y', strtotime($item->birthday)) }}</td>
                                            <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                            <td style="display: none;"
                                                class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- /.panel-body -->

                    {{--Paginate--}}
                    {!! $listUser->appends($appends)->links() !!}
                </div>
            </div>
        </div>
        {{--END: List user--}}
    </div>

    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'user'])

@stop

@section('custom_header')
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.tag-editor.css') }}">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.caret.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.tag-editor.js') }}"></script>
    <script>
        $('#tags').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: {collision: 'flip'}, // automatic menu position up/down
                source: []
            },
            forceLowercase: false,
            placeholder: 'Enter tags ...'
        });

        // Set thoi gian cho input datetime-local
        var now = new Date($.now())
                , year
                , month
                , date
                , hours
                , minutes
                , seconds
                , formattedDateTime
                ;
        year = now.getFullYear();
        month = now.getMonth().toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
        date = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate();
        hours = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
        minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes();
        seconds = now.getSeconds().toString().length === 1 ? '0' + now.getSeconds().toString() : now.getSeconds();
        SetTimeForInput('input[name=date_start]', year, month, date, 10, 10);
        SetThisTimeForInput('input[name=date_end]');
    </script>

@stop