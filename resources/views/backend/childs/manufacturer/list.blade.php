@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.manufacturer') }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--Sidebar--}}
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.add_something', [':something' => trans('product.manufacturer')]) }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="add-manufacturer">
                                    <div class="form-group">
                                        <label>{{ trans('form.name') }}</label>
                                        <input class="form-control" name="name" required>

                                        <p class="help-block">{{ trans('form.name_des') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('form.slug') }}</label>
                                        <input class="form-control" name="slug" required>

                                        <p class="help-block">{{ trans('form.slug_des') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.guarantee') }}</label>
                                        <input class="form-control" name="guarantee" type="number">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.madein') }}</label>
                                        <select name="madein_id" class="form-control">
                                            @if($listMadein)
                                                @foreach($listMadein as $madein)
                                                    <option class="madein-id-{{ $madein->id }}"
                                                            value="{{ $madein->id }}">{{ $madein->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.size') }}</label>
                                        <input class="form-control" name="size">
                                    </div>
                                    <div class="form-group">
                                        <div class="thumbwrapper">
                                            @include('backend.partials.image_thumb', ['name'=>'image'])
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('form.description') }}</label>
                                        <textarea class="form-control" name="intro"></textarea>

                                        <p class="help-block">{{ trans('form.description_des') }}</p>
                                    </div>
                                    <textarea class="tinymce" style="display: none;"></textarea>
                                    <input type="hidden" value="" name="id">
                                    <button class="btn btn-primary create_manufacturer">{{ trans('form.btn_create') }}</button>
                                    <button class="btn btn-primary edit_manufacturer">{{ trans('form.btn_edit') }}</button>
                                    <button class="btn btn-default reset"
                                            onclick="resetForm('.add-manufacturer')">{{ trans('form.btn_reset') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name">{{ trans('product.list_manufacturer') }}</div>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">{{ trans('system.actions') }}</option>
                                <option value="delete">{{ trans('form.delete') }}</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-master" value=""></th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('form.slug') }}</th>
                                        <th>{{ trans('product.madein') }}</th>
                                        <th>{{ trans('product.size') }}</th>
                                        <th>{{ trans('product.guarantee') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php ?>
                                    @if($listManufacturer)
                                        @foreach($listManufacturer as $item)
                                            <tr>
                                                <td><input type="checkbox" class="ids" name="id[]"
                                                           value="{{ $item->id }}"></td>
                                                <td class="item-name">{{ $item->name }}</td>
                                                <td class="item-slug">{{ $item->slug }}</td>
                                                <td class="item-madein">@if(isset($item->madein->name)){{ $item->madein->name }}@endif</td>
                                                <td class="item-size">{{ $item->size }}</td>
                                                <td class="item-guarantee">{{ $item->guarantee }}</td>
                                                <td style="display: none"
                                                    class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        {{--END: Sidebar--}}

        {{--Main--}}
        <div class="row">
            <div class="col-lg-8">

            </div>
        </div>
        {{--END: Main--}}
    </div>

    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'manufacturer'])

@stop

@section('custom_header')
    <link href="{{ URL::asset('css/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('js/partials/initFormTable.js') }}"></script>

    <script>
        // DataTable
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });

        function getData() {
            var data = {
                id: $('input[name=id]').val(),
                name: $('input[name=name]').val() === '' ? swal({
                    title: 'Name required',
                    type: 'error'
                }) : $('input[name=name]').val(),
                slug: $('input[name=slug]').val() === '' ? swal({
                    title: 'Slug required',
                    type: 'error'
                }) : $('input[name=slug]').val(),
                intro: $('textarea[name=intro]').val(),
                guarantee: $('input[name=guarantee]').val(),
                madein_id: $('select[name=madein_id]').val(),
                size: $('input[name=size]').val(),
                image: $('input[name=image]').val(),
                _token: '{{ csrf_token() }}'
            };
            return data;
        }

        // Create manufacturer
        $('.create_manufacturer').click(function () {

            var formData = getData();

            if ($('input[name=name]').val() != '' && $('input[name=slug]').val() != '') {
                $.ajax({
                    url: '{{ route('manufacturer.postCreate') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    success: function (resp) {
                        if (resp.status == 'success') {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                            // Add new element to table
                            $('table tbody').html('<tr role="row" class="odd"><td class="sorting_1"><input type="checkbox" class="ids" name="id[]" value="' + resp.id + '"></td><td class="item-name">' + formData.name + '</td><td class="item-slug">' + formData.slug + '</td><td class="item-madein">' + resp.madein + '</td><td class="item-size">' + formData.size + '</td><td class="item-guarantee">' + formData.guarantee + '</td><td style="display: none" class="id id-' + resp.id + '">' + resp.id + '</td></tr>' + $('table tbody').html());
                            // Reset form
                            $('button.reset').click();
                        } else {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_create_error') }}', type: 'error'});
                    }
                });
            }

        });

        // Edit manufacturer
        $('table').on('click', 'td.item-name', function () {
            var id = $(this).parent('tr').find('td.id').html();
            $.ajax({
                url: '{{ route('manufacturer.getEdit') }}',
                type: 'GET',
                dataType: 'json',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (resp) {
                    if (resp.status == 'success') {
                        // Insert data to form edit
                        $('input[name=id]').val(resp.data.id);
                        $('input[name=name]').val(resp.data.name);
                        $('input[name=slug]').val(resp.data.slug);
                        $('input[name=guarantee]').val(resp.data.guarantee);
                        $('input[name=size]').val(resp.data.size);
                        $('.div0-image').find('.wrap-thumbnail').html('<img src="http://' + location.host + '/filemanager/userfiles/' + resp.data.image + '">');
                        if (resp.data.image != '') {
                            $('.thumb .div1-image').css('display', 'block');
                            $('.thumb .div2-image').css('display', 'none');
                        }
                        $('.madein-id-' + resp.data.madein_id).attr('selected', 'selected');
                        $('.madein-id-' + resp.data.madein_id).siblings('option').each(function () {
                            $(this).removeAttr('selected');
                        });
                        $('textarea[name=intro]').val(resp.data.intro);

                    } else {
                        swal({title: resp.status, text: resp.msg, type: resp.status});
                    }
                },
                error: function (resp) {
                    swal({title: '{{ trans('system.ajax_create_error') }}', type: 'error'});
                }
            });
        });


        $('.edit_manufacturer').click(function () {

            var formData = getData();

            if ($('input[name=id]').val() != '' && $('input[name=name]').val() != '' && $('input[name=slug]').val() != '') {
                $.ajax({
                    url: '{{ route('manufacturer.postEdit') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    success: function (resp) {
                        if (resp.status == 'success') {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                            // Reset form
                            $('button.reset').click();

                            var tr = $('table td.id.id-' + formData.id).parent('tr');
                            tr.find('td.item-name').html(formData.name);
                            tr.find('td.item-slug').html(resp.slug);
                            tr.find('td.item-madein').html(resp.madein);
                            tr.find('td.item-size').html(formData.size);
                            tr.find('td.item-guarantee').html(formData.guarantee);
                        } else {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            }
        });

    </script>

@stop