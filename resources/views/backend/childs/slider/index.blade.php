@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.slider') }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert aleert-danger col-xs-12">{{session('error')}}</span>@endif

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('product.list_slider') }}
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="">
                            {{ csrf_field() }}
                            @foreach($listSlider as $key => $item)
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{ trans('product.name') }}</label>
                                        <input class="form-control" name="name{{ $item->id }}"
                                               value="{{ $item->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.intro') }}</label>
                                        <input class="form-control" name="intro{{ $item->id }}"
                                               value="{{ $item->intro }}">
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="form-group">
                                        <label>Ảnh đại diện</label>
                                        @include('backend.partials.image_thumb', ['name' => 'image' . $item->id, 'src' => $item->image])
                                    </div>
                                    <div class="icon-delete" data-id="{{ $item->id }}"><p class="fa fa-times"></p></div>
                                </div>
                            @endforeach

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>{{ trans('product.name') }}</label>
                                    <input class="form-control" name="name"
                                           value="">
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('product.intro') }}</label>
                                    <input class="form-control" name="intro"
                                           value="">
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="form-group">
                                    <label>Ảnh đại diện</label>
                                    @include('backend.partials.image_thumb', ['name' => 'image'])
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success create-slider" name="create" value="{{ trans('form.btn_create') }}">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <input type="submit"
                                        class="btn btn-primary" value="{{ trans('form.btn_save') }}" name="edit">
                            </div>
                            <!-- /.row (nested) -->
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

    </div>


@stop

@section('custom_footer')

    <script>
        $('.icon-delete').each(function () {
            $(this).click(function () {
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('slider.postDelete') }}',
                    type: 'POST',
                    data: { id: id, _token: '{{ csrf_token() }}' },
                    success: function (resp) {
                        location.reload();
                    },
                    error: function (resp) {

                    }
                });
            });
        });
    </script>
@stop