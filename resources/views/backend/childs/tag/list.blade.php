@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.tag') }}
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--List Tag--}}
        <div class="row list-Tag">
            <form class="col-xs-12 top-action" method="GET" action="">
                <div class="col-md-3">
                    <input name="keyword" value="" class="form-control">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary">Search</button>
                </div>
            </form>

            <div class="col-xs-12">
                <div class="panel panel-default list-Tag">
                    <div class="panel-heading">
                        <span>Danh sách tag</span>
                        <ul class="subsubsub">
                            <?php
                            $count_all_Tag = \App\Models\Tag::get()->count();
                            ?>
                            <li class="all"><span><span>All</span> <span class="count">({{ $count_all_Tag }}
                                        )</span></span>
                            </li>
                        </ul>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">Actions</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if($listTag)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('form.slug') }}</th>
                                        <th>Số lần dùng</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listTag as $item)
                                        <tr>
                                            <td><input type="checkbox" class="ids" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td class="item-name"><a href="{{ route('tag.getEdit', ['id'=>$item->id]) }}">{{ $item->name }}</a></td>
                                            <td class="item-slug">{{ $item->slug }}</td>
                                            <td>{{ $item->count }}</td>
                                            <td style="display: none;"
                                                class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- /.panel-body -->

                    {{--Paginate--}}
                    {!! $listTag->appends($appends)->links() !!}
                </div>
            </div>
        </div>
        {{--END: List Tag--}}

    </div>


    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'tag'])

@stop