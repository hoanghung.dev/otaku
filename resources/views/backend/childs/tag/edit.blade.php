@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.post') }}
                    <a href="{{ route('tag') }}"
                       class="btn btn-success btn-add-post">{{ trans('form.btn_cancel') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        {{--Create post--}}
        <form class="row add-post" method="POST" action="" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name">{{ trans('system.post_info') }}</div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Product</label>
                                <div class="form-group">
                                    @if(count($listProduct) > 0)
                                        @foreach($listProduct as $item)
                                            <input type="checkbox" name="product[]" value="{{ $item->id }}">
                                            {{ $item->name }}
                                        @endforeach
                                    @endif
                                </div>

                                <label>Post</label>
                                <div class="form-group">
                                    @if(count($listPost) > 0)
                                        @foreach($listPost as $item)
                                            <input type="checkbox" name="post[]" value="{{ $item->id }}">
                                            {{ $item->name }}
                                        @endforeach
                                    @endif
                                </div>

                                <input class="btn btn-primary" value="Xóa gắn tag" type="submit">
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </form>
        {{--END: Create post--}}
    </div>

@stop