@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.category') }}
                    <a href="{{ route('category') }}" class="btn btn-success">{{ trans('form.btn_cancel') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        {{--Sidebar--}}
        <form class="row" method="POST" action="" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.add_something', [':something' => trans('system.category')]) }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="add-category">
                                    <div class="form-group">
                                        <label>{{ trans('form.name') }}</label>
                                        <input class="form-control" name="name" required>

                                        <p class="help-block">{{ trans('form.name_des') }}</p>
                                    </div>
                                    <div class="form-group" style="display: none;">
                                        <label>{{ trans('form.slug') }}</label>
                                        <input class="form-control" name="slug" required>

                                        <p class="help-block">{{ trans('form.slug_des') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('form.category_parent') }}</label>
                                        @if($listCategory)
                                            <select class="form-control" name="parent_id">
                                                <option value="0"
                                                        class="parent-id-0">{{ trans('system.no_parent') }}</option>
                                                @foreach($listCategory as $item)
                                                    @if($item->parent_id == 0)
                                                        {{--Option parent--}}
                                                        <option value="{{ $item->id }}"
                                                                class="parent-id-{{ $item->id }}">{{ $item->name }}</option>
                                                        <?php
                                                        $listChilds = $item->childs;?>
                                                        @if($listChilds != null)
                                                            @foreach($listChilds as $one)
                                                                {{--Option child level 1--}}
                                                                <option value="{{ $one->id }}"
                                                                        class="parent-id-{{ $one->id }}">
                                                                    --{{ $one->name }}</option>
                                                                <?php
                                                                $listChilds2 = $one->childs;?>
                                                                @if($listChilds2 != null)
                                                                    @foreach($listChilds2 as $one2)
                                                                        {{--Option child level 2--}}
                                                                        <option value="{{ $one2->id }}"
                                                                                class="parent-id-{{ $one2->id }}">
                                                                            ----{{ $one2->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('category.order') }}</label>
                                        <input class="form-control" name="order_no" type="number" value="1">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('form.description') }}</label>
                                        <textarea class="form-control" name="intro"></textarea>
                                    </div>
                                    <button class="btn btn-primary">{{ trans('form.btn_create') }}</button>
                                    <button class="btn btn-default reset" type="button"
                                            onclick="resetForm('.add-category')">{{ trans('form.btn_reset') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.category_info') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">

                                <label>Ảnh đại diện</label>

                                @include('backend.partials.image_thumb_default', ['name'=>'image'])

                                <label>Ảnh icon</label>

                                @include('backend.partials.image_thumb', ['name'=>'icon'])

                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </form>
        {{--END: Sidebar--}}
    </div>

@stop