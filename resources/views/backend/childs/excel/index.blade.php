@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('form.export_import') }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-success col-xs-12">{{session('error')}}</span>@endif

        <div class="row">
            <form role="form" method="POST" action="">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('post.export') }}
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <input type="checkbox" name="product_name" checked>
                                        <label>{{ trans('product.name') }}</label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit"
                                    class="btn btn-primary save_setting">{{ trans('form.btn_export') }}</button>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('product.export') }}
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <input type="checkbox"
                                               name="site_offline" @if($setting->site_offline == 1){{ 'checked' }}@endif>
                                        <label>{{ trans('system.site_offline') }}</label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit"
                                    class="btn btn-primary save_setting">{{ trans('form.btn_export') }}</button>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </form>
            <!-- /.col-lg-12 -->
        </div>

    </div>


@stop

@section('custom_footer')
    <script>
        $('.div0-logo').find('.wrap-thumbnail').html('<img src="http://' + location.host + '/filemanager/userfiles/' + '{{ $setting->logo }}">');
        @if($setting->logo != '')
            $('.thumb .div1-logo').css('display', 'block');
        $('.thumb .div2-logo').css('display', 'none');
        $('input[name=logo]').val('{{ $setting->logo }}');
        @endif

        $('.div0-favicon').find('.wrap-thumbnail').html('<img src="http://' + location.host + '/filemanager/userfiles/' + '{{ $setting->favicon }}">');
        @if($setting->favicon != '')
            $('.thumb .div1-favicon').css('display', 'block');
        $('.thumb .div2-favicon').css('display', 'none');
        $('input[name=favicon]').val('{{ $setting->favicon }}');
        @endif
    </script>
@stop