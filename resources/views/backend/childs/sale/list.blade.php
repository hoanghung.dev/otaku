@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('product.sale') }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--Sidebar--}}
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.add_something', [':something' => trans('system.sale')]) }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="add-sale">
                                    <div class="form-group">
                                        <label>{{ trans('form.name') }}</label>
                                        <input class="form-control" name="name" required>

                                        <p class="help-block">{{ trans('form.name_des') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.type') }}</label>
                                        <select name="type" class="form-control">
                                            <option value="0" class="type-0">0</option>
                                            <option value="-" class="type--">-</option>
                                            <option value="-%" class="giampt">-%</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('product.value') }}</label>
                                        <input type="number" class="form-control" name="value" required>

                                        <p class="help-block">{{ trans('form.name_des') }}</p>
                                    </div>

                                    <div class="form-group">
                                        <label>{{ trans('form.description') }}</label>
                                        <textarea class="form-control" name="intro"></textarea>

                                        <p class="help-block">{{ trans('form.description_des') }}</p>
                                    </div>
                                    <input type="hidden" value="" name="id">
                                    <button class="btn btn-primary create_sale">{{ trans('form.btn_create') }}</button>
                                    <button class="btn btn-primary edit_sale">{{ trans('form.btn_edit') }}</button>
                                    <button class="btn btn-default reset"
                                            onclick="resetForm('.add-sale')">{{ trans('form.btn_reset') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name">{{ trans('product.list_sale') }}</div>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">{{ trans('system.actions') }}</option>
                                <option value="delete">{{ trans('form.delete') }}</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('product.type') }}</th>
                                        <th>{{ trans('product.value') }}</th>
                                        <th>{{ trans('form.description') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php ?>
                                    @if($listSale)
                                        @foreach($listSale as $item)
                                            <tr>
                                                <td><input type="checkbox" class="ids" name="id[]"
                                                           value="{{ $item->id }}"></td>
                                                <td class="item-name">{{ $item->name }}</td>
                                                <td class="item-name">{{ $item->type }}</td>
                                                <td class="item-name">{{ $item->value }}</td>
                                                <td class="item-intro">{{ $item->intro }}</td>
                                                <td style="display: none"
                                                    class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        {{--END: Sidebar--}}

        {{--Main--}}
        <div class="row">
            <div class="col-lg-8">

            </div>
        </div>
        {{--END: Main--}}
    </div>

    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'sale'])

@stop

@section('custom_header')
    <link href="{{ URL::asset('css/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('js/partials/initFormTable.js') }}"></script>

    <script>
        // DataTable
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });

        function getData() {
            var data = {
                id: $('input[name=id]').val(),
                name: $('input[name=name]').val() === '' ? swal({
                    title: 'Name required',
                    type: 'error'
                }) : $('input[name=name]').val(),
                type: $('select[name=type]').val(),
                value: $('input[name=value]').val(),
                manufacturer_id: $('select[name=manufacturer_id]').val(),
                category_id: $('select[name=category_id]').val(),
                intro: $('textarea[name=intro]').val(),
                _token: '{{ csrf_token() }}'
            };
            return data;
        }

        // Create sale
        $('.create_sale').click(function () {

            var formData = getData();

            if ($('input[name=name]').val() != '' && $('input[name=value]').val() != '') {
                $.ajax({
                    url: '{{ route('sale.postCreate') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    success: function (resp) {
                        if (resp.status == 'success') {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                            // Add new element to table
                            $('table tbody').html('<tr role="row" class="odd"><td class="sorting_1"><input type="checkbox" class="ids" name="id[]" value="' + resp.id + '"></td><td class="item-name">' + formData.name + '</td><td class="item-type">' + formData.type + '</td><td class="item-value">' + formData.value + '</td><td class="item-intro">' + formData.intro + '</td><td style="display: none" class="id id-' + resp.id + '">' + resp.id + '</td></tr>' + $('table tbody').html());
                            // Reset form
                            $('button.reset').click();
                        } else {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_create_error') }}', type: 'error'});
                    }
                });
            }

        });

        // Edit sale
        $('table').on('click', 'td.item-name', function () {
            var id = $(this).parent('tr').find('td.id').html();
            $.ajax({
                url: '{{ route('sale.getEdit') }}',
                type: 'GET',
                dataType: 'json',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (resp) {
                    if (resp.status == 'success') {
                        // Insert data to form edit
                        $('input[name=id]').val(resp.data.id);
                        $('input[name=name]').val(resp.data.name);
                        if(resp.data.type=='-%') {
                            $('.giampt').attr('selected', 'selected');
                            $('.giampt').siblings('option').each(function () {
                                $(this).removeAttr('selected');
                            });
                        } else {
                            $('.type-' + resp.data.type).attr('selected', 'selected');
                            $('.type-' + resp.data.type).siblings('option').each(function () {
                                $(this).removeAttr('selected');
                            });
                        }

                        $('input[name=value]').val(resp.data.value);
                        $('textarea[name=intro]').val(resp.data.intro);
                        if (resp.data.category_id == '') resp.data.category_id = 0;
                        $('.category-id-' + resp.data.category_id).attr('selected', 'selected');
                        $('.category-id-' + resp.data.category_id).siblings('option').each(function () {
                            $(this).removeAttr('selected');
                        });
                        if (resp.data.manufacturer_id == '') resp.data.manufacturer_id = 0;
                        $('.manufacturer-id-' + resp.data.manufacturer_id).attr('selected', 'selected');
                        $('.manufacturer-id-' + resp.data.manufacturer_id).siblings('option').each(function () {
                            $(this).removeAttr('selected');
                        });
                    } else {
                        swal({title: resp.status, text: resp.msg, type: resp.status});
                    }
                },
                error: function (resp) {
                    swal({title: '{{ trans('system.ajax_create_error') }}', type: 'error'});
                }
            });
        });


        $('.edit_sale').click(function () {

            var formData = getData();

            if ($('input[name=id]').val() != '' && $('input[name=name]').val() != '' && $('input[name=value]').val() != '') {
                $.ajax({
                    url: '{{ route('sale.postEdit') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    success: function (resp) {
                        if (resp.status == 'success') {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                            // Reset form
                            $('button.reset').click();

                            var tr = $('table td.id.id-' + formData.id).parent('tr');
                            tr.find('td.item-name').html(formData.name);
                            tr.find('td.item-type').html(formData.type);
                            tr.find('td.item-value').html(formData.value);
                            tr.find('td.item-intro').html(formData.intro);
                        } else {
                            swal({title: resp.status, text: resp.msg, type: resp.status});
                        }
                    },
                    error: function (resp) {
                        swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                    }
                });
            }
        });
    </script>

@stop