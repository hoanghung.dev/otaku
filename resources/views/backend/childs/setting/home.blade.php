@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.settings') }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        <div class="row page-setting">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.settings') }}
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="">
                            <div class="row">
                                <div class="col-xs-12">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>5 block danh mục</label>
                                        <div class="col-xs-12">
                                            @if(count($listCategory) > 0)
                                                <ul>
                                                    @foreach($listCategory as $item)
                                                        <li @if($item->parent_id == 0){{ 'class=parent' }}@endif>
                                                            <input value="{{ $item->id }}" @if(in_array($item->id, $settingHome->listCategory)){{ 'checked' }}@endif
                                                            type="checkbox" name="listCategory[]"> {{ $item->name }}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>6 block ảnh danh mục</label>
                                        @if(count($listCategory) > 0)
                                            <ul>
                                                @foreach($listCategory as $item)
                                                    <li @if($item->parent_id == 0){{ 'class=parent' }}@endif>
                                                        <input value="{{ $item->id }}" @if(in_array($item->id, $settingHome->listBox)){{ 'checked' }}@endif
                                                        type="checkbox" name="listBox[]" > {{ $item->name }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Block support</label>
                                        <div class="col-xs-12">
                                            <label>Kinh doanh Bắc</label>
                                            <input class="form-control" name="hotline1"
                                                   value="{{ $settingHome->support->hotline1 }}">
                                            <input class="form-control" name="email1"
                                                   value="{{ $settingHome->support->email1 }}">
                                        </div>
                                        <div class="col-xs-12">
                                            <label>Kinh doanh Nam</label>
                                            <input class="form-control" name="hotline2"
                                                   value="{{ $settingHome->support->hotline2 }}">
                                            <input class="form-control" name="email2"
                                                   value="{{ $settingHome->support->email2 }}">
                                        </div>
                                        <div class="col-xs-12">
                                            <label>Kỹ thuật Bắc</label>
                                            <input class="form-control" name="hotline3"
                                                   value="{{ $settingHome->support->hotline3 }}">
                                            <input class="form-control" name="email3"
                                                   value="{{ $settingHome->support->email3 }}">
                                        </div>
                                        <div class="col-xs-12">
                                            <label>Kỹ thuật Nam</label>
                                            <input class="form-control" name="hotline4"
                                                   value="{{ $settingHome->support->hotline4 }}">
                                            <input class="form-control" name="email4"
                                                   value="{{ $settingHome->support->email4 }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit"
                                        class="btn btn-primary save_setting">{{ trans('form.btn_save') }}</button>
                            </div>
                            <!-- /.row (nested) -->
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

    </div>


@stop