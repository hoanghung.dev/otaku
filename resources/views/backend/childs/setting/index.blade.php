@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.settings') }}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.settings') }}
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="">
                            <div class="row">
                                <div class="col-lg-6">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>{{ trans('system.site_name') }}</label>
                                        <input class="form-control" name="site_name" value="{{ $setting->site_name }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox"
                                               name="site_offline" @if($setting->site_offline == 1){{ 'checked' }}@endif>
                                        <label>{{ trans('system.site_offline') }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.site_offline_message') }}</label>
                                        <input class="form-control" name="site_offline_message"
                                               value="{{ $setting->site_offline_message }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox"
                                               name="cache" @if($setting->cache == 1){{ 'checked' }}@endif>
                                        <label>{{ trans('system.cache') }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.cache_time') }}</label>
                                        <input type="number" class="form-control" name="cache_time"
                                               value="{{ $setting->cache_time }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.meta_description') }}</label>
                                        <input class="form-control" name="meta_description"
                                               value="{{ $setting->meta_description }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.meta_keywords') }}</label>
                                        <input class="form-control" name="meta_keywords"
                                               value="{{ $setting->meta_keywords }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.robots') }}</label>
                                        <select name="robots" class="form-control">
                                            <option @if($setting->robots == 'index, follow'){{ 'selected' }}@endif value="index, follow">
                                                Index, Follow
                                            </option>
                                            <option @if($setting->robots == 'noindex, follow'){{ 'selected' }}@endif value="noindex, follow">
                                                No index, follow
                                            </option>
                                            <option @if($setting->robots == 'index, nofollow'){{ 'selected' }}@endif value="index, nofollow">
                                                Index, No follow
                                            </option>
                                            <option @if($setting->robots == 'noindex, nofollow'){{ 'selected' }}@endif value="noindex, nofollow">
                                                No index, No follow
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload file sitemap.xml</label>
                                        <input name="sitemap" type="file">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.language') }}</label>
                                        <select name="language" class="form-control">
                                            <option @if($setting->language == 'en'){{ 'selected' }}@endif value="en">
                                                English
                                            </option>
                                            <option @if($setting->language == 'vi'){{ 'selected' }}@endif value="vi">
                                                Việt Nam
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <input type="checkbox"
                                               name="send_mail" @if($setting->send_mail == 1){{ 'checked' }}@endif>
                                        <label>{{ trans('system.send_mail') }}</label>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.mail_from') }}</label>
                                        <input class="form-control" name="mail_from" value="{{ $setting->mail_from }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.mail_name') }}</label>
                                        <input class="form-control" name="mail_name" value="{{ $setting->mail_name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.hotline') }}</label>
                                        <input class="form-control" name="hotline" value="{{ $setting->hotline }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Hỗ trợ trực tuyến</label>
                                        <textarea class="form-control content" rows="7" name="box_support">{!! $setting->box_support !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Footer 1</label>
                                        <textarea class="form-control content" rows="7" name="footer1">{!! $setting->footer1 !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Footer 2</label>
                                        <textarea class="form-control content" rows="7" name="footer2">{!! $setting->footer2 !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Footer 3</label>
                                        <textarea class="form-control content" rows="7" name="footer3">{!! $setting->footer3 !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Địa chỉ 1</label>
                                        <textarea class="form-control content" rows="7" name="address1">{!! $setting->address1 !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Footer 2</label>
                                        <textarea class="form-control content" rows="7" name="address2">{!! $setting->address2 !!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('system.copyright') }}</label>
                                        <input class="form-control" name="copyright" value="{{ $setting->copyright }}">
                                    </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                    <label>Logo</label>
                                    @include('backend.partials.image_thumb', ['name' => 'logo', 'src' => $setting->logo])

                                    <label>Favicon</label>
                                    @include('backend.partials.image_thumb', ['name' => 'favicon', 'src' => $setting->favicon])

                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <button type="submit"
                                    class="btn btn-primary save_setting">{{ trans('form.btn_save') }}</button>
                            <!-- /.row (nested) -->
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

    </div>


@stop
