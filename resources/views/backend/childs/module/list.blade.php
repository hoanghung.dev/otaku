@extends('backend.layouts.master')

@section('main_content')

    <?php $itemModal = new $module['modal'];?>
    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ ucwords($module['name']) }}
                    <a class="btn btn-success"
                       href="{{ route($module['name'].'.getCreate') }}">{{ trans('system.btn_add') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--List--}}
        <div class="row list-item">
            <form class="col-xs-12 top-action" method="GET" action="">
                <div class="col-md-2">
                    <input type="date" name="date_start" class="form-control"
                           value="@if(isset($filter['date_start'])){{ date('Y-m-d', strtotime($filter['date_start'])) }}@else{{ date('Y-m-d') }}@endif">
                </div>
                <div class="col-md-2">
                    <input type="date" name="date_end" class="form-control"
                           value="@if(isset($filter['date_end'])){{ date('Y-m-d', strtotime($filter['date_end'])) }}@else{{ date('Y-m-d') }}@endif">
                </div>
                <div class="col-md-2">
                    <?php $listCategory = \App\Models\Category::where('parent_id', 0)->orderBy('name', 'asc')->get();?>
                    <select name="category" class="form-control">
                        <option value="0">{{ trans('form.all_category') }}</option>
                        @foreach($listCategory as $item)
                            {{--Option parent--}}
                            <option value="{{ $item->id }}" @if(isset($filter['category']) && $filter['category'] == $item->id){{ 'selected' }}@endif
                                    class="parent-id-{{ $item->id }}">{{ $item->name }}</option>
                            <?php
                            $listChilds = $item->childs; ?>
                            @if($listChilds !=null)
                                @foreach($listChilds as $one)
                                    {{--Option child level 1--}}
                                    <option value="{{ $one->id }}" @if(isset($filter['category']) && $filter['category'] == $one->id){{ 'selected' }}@endif
                                            class="parent-id-{{ $one->id }}">
                                        --{{ $one->name }}</option>
                                    <?php
                                    $listChilds2 = $one->childs;?>
                                    @if($listChilds2 != null)
                                        @foreach($listChilds2 as $one2)
                                            {{--Option child level 2--}}
                                            <option value="{{ $one2->id }}" @if(isset($filter['category']) && $filter['category'] == $one2->id){{ 'selected' }}@endif
                                                    class="parent-id-{{ $one2->id }}">
                                                ----{{ $one2->name }}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="status" class="form-control">
                        <option value="">{{ trans('post.status') }}</option>
                        <option value="1" @if(isset($filter['status']) && $filter['status'] == '1'){{ 'selected' }}@endif>{{ trans('post.publish') }}</option>
                        <option value="0" @if(isset($filter['status']) && $filter['status'] == '0'){{ 'selected' }}@endif>{{ trans('post.draf') }}</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input name="keyword" class="form-control"
                           value="{{ isset($filter['keyword']) ? $filter['keyword'] : '' }}">
                </div>
                <div class="col-md-1 btn_search">
                    <button class="btn btn-primary">Search</button>
                </div>
            </form>

            <div class="col-xs-12">
                <div class="panel panel-default list-item">
                    <div class="panel-heading">
                        <span>{{ trans('system.list') }} {{ ucwords($module['name']) }}</span>
                        <ul class="subsubsub">
                            <?php
                            $countAll = $itemModal->count();
                            ?>
                            <li class="all"><span><span data-type="all">All</span> <span
                                            class="count">({{ number_format($countAll, 0, '.', '.') }})</span></span>
                            </li>
                        </ul>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">Actions</option>
                                <option value="publish">Publish</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if($listItem)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.image') }}</th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('product.code') }}</th>
                                        <th>{{ trans('system.category') }}</th>
                                        <th>{{ trans('form.author') }}</th>
                                        <th style="text-align: center;">{{ trans('system.status') }}</th>
                                        <th>{{ trans('form.created_at') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listItem as $item)
                                        <tr>
                                            <td><input type="checkbox" class="ids" name="id[]"
                                                       value="{{ $item->$module['primaryKey'] }}">
                                            </td>
                                            <td class="item-image"><img
                                                        src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                                                        style="max-width: 97px;"/></td>
                                            <td class="item-name"><a
                                                        href="{{ route($module['name'].'.getEdit', ['id'=>$item->$module['primaryKey']]) }}">{{ $item->name != '' ? $item->name : 'Không rõ' }}</a>
                                            </td>
                                            <td class="item-code">{{ $item->code }}</td>
                                            <td class="item-category">@if(isset($item->category)){{ $item->category->name }}@endif</td>
                                            <td>{{ $item->user->name }}</td>
                                            <td style="text-align: center;" id="img-{{ $item->$module['primaryKey'] }}">
                                                <img
                                                        data-id="{{ $item->$module['primaryKey'] }}" class="publish"
                                                        style="cursor:pointer;"
                                                        src="@if($item->status==1){{ URL::asset('images/published.png') }}@else{{ URL::asset('images/unpublish.png') }}@endif">
                                            </td>
                                            <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                            <td style="display: none;"
                                                class="id id-{{ $item->$module['primaryKey'] }}">{{ $item->$module['primaryKey'] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- /.panel-body -->

                    {{--Paginate--}}
                    {!! $listItem->appends(isset($filter) ? $filter : '')->links() !!}
                </div>
            </div>
        </div>
        {{--END: List item--}}
    </div>


    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => $module['name']])

@stop

@section('custom_footer')

    <script>

        // Publish
        $('table img.publish').each(function () {
            $(this).click(function () {
                var id = $(this).data('id');

                if (id != '') {
                    $.ajax({
                        url: '{{ route($module['name'] . '.postPublish') }}',
                        type: 'post',
                        dataType: 'json',
                        data: {id: id, _token: '{{ csrf_token() }}'},
                        success: function (resp) {
                            if (resp.status == 'success') {
                                if (resp.status_publish == 1) $('#img-' + id).find('img').attr('src', '{{ URL::asset('images/published.png') }}');
                                else if (resp.status_publish == 0) $('#img-' + id).find('img').attr('src', '{{ URL::asset('images/unpublish.png') }}');
                            } else {
                                swal({title: resp.status, text: resp.msg, type: resp.status});
                            }
                        },
                        error: function (resp) {
                            swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                        }
                    });
                }
            });
        });
    </script>

@stop