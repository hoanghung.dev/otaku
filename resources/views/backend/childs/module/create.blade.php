@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.create') }} {{ ucwords($module['name']) }}
                    <a class="btn btn-success" href="javascript:;" onclick="history.go(-1);">{{ trans('system.btn_cancel') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        {{--Create item--}}
        <form class="row add-item" method="POST" action="" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name" style="width: 100%;">{{ trans('system.main_info') }}</div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('system.name') }}</label>
                                    <input class="form-control" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('system.slug') }}</label>
                                    <input class="form-control" name="slug" required>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('product.code') }}</label>
                                    <input class="form-control" name="code">
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.description') }}</label>
                                    <textarea class="form-control" name="intro" rows="5"></textarea>
                                </div>
                                <div class="form-group editor">
                                    <label>{{ trans('form.content') }}</label>
                                    <textarea class="form-control content" rows="15"
                                              name="content"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Core</label>
                                            <select name="core" class="form-control">
                                                <option>Chọn Core</option>
                                                <option value="Atom 1.8">Atom 1.8</option>
                                                <option value="Atom 2.13">Atom 2.13</option>
                                                <option value="Core 2 Duo 2.6">Core 2 Duo 2.6</option>
                                                <option value="I3">I3</option>
                                                <option value="I5">I5</option>
                                                <option value="I7">I7</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Hệ điều hành</label>
                                            <select name="system" class="form-control">
                                                <option>Chọn hệ điều hành</option>
                                                <option value="Windows 7">Windows 7</option>
                                                <option value="Pos Ready">Pos Ready</option>
                                                <option value="Dos">Dos</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="status" checked> {{ trans('form.publish') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12" style="padding: 0px 30px;">
                                <button class="btn btn-primary create_item">{{ trans('form.btn_create') }}</button>
                                <button class="btn btn-default reset" type="button"
                                        onclick="resetForm('.add-item')">{{ trans('form.btn_reset') }}</button>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.category_info') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">

                                @include('backend.partials.image_thumb_default', ['name'=>'image'])

                                <div class="form-group">
                                    <?php $listCategory = \App\Models\Category::where('parent_id', 0)->orderBy('name', 'asc')->get();?>
                                    <label>{{ trans('system.category') }}</label>
                                    <select class="form-control" name="category_id">
                                        @foreach($listCategory as $item)
                                            <option value="{{ $item->id }}"
                                                    class="category-id-{{ $item->id }}">{{ $item->name }}</option>
                                            <?php
                                            $listChilds = $item->childs;?>
                                            @if($listChilds != null)
                                                @foreach($listChilds as $one)
                                                    {{--Option child level 1--}}
                                                    <option value="{{ $one->id }}"
                                                            class="category-id-{{ $one->id }}">
                                                        --{{ $one->name }}</option>
                                                    <?php
                                                    $listChilds2 = $one->childs; ?>
                                                    @if($listChilds2)
                                                        @foreach($listChilds2 as $one2)
                                                            {{--Option child level 2--}}
                                                            <option value="{{ $one2->id }}"
                                                                    class="category-id-{{ $one2->id }}">
                                                                ----{{ $one2->name }}</option>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

        </form>
        {{--END: Create item--}}
    </div>
@stop
