@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.category') }}
                    <a class="btn btn-success" href="{{ route('productCategory.getCreate') }}">{{ trans('form.btn_add') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--Sidebar--}}
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name">Danh sách danh mục</div>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">{{ trans('system.actions') }}</option>
                                <option value="delete">{{ trans('form.delete') }}</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>Id</th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>Thứ tự</th>
                                        <th>{{ trans('system.count') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php ?>
                                    @if($listCategory)
                                        @foreach($listCategory as $item)
                                            <?php
                                            $count = \App\Models\Product::where('category_id', $item->id)->count();
                                            $childs = $item->childs;
                                            $id = $item->id;
                                            ?>
                                            <tr class="parent-{{$id}}" data-action="{{ $id }}">
                                                <td><input type="checkbox" class="ids" name="id[]"
                                                           value="{{ $item->id }}"></td>
                                                <td>{{ $item->id }}</td>
                                                <td class="item-name"><a href="{{ route('productCategory.getEdit', ['id'=>$item->id]) }}">{{ $item->name }}</a> @if(count($childs) > 0)<span class="down">+</span>@endif </td>
                                                <td class="item-intro">{{ $item->order_no }}</td>
                                                <td>{{ $count }}</td>
                                                <td style="display: none"
                                                    class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                            </tr>
                                            @if(count($childs) > 0)
                                                @foreach($childs as $item2)
                                                    <tr class="child child-{{$id}}">
                                                        <td><input type="checkbox" class="ids" name="id[]"
                                                                   value="{{ $item2->id }}"></td>
                                                        <td>{{ $item2->id }}</td>
                                                        <td class="item-name">___<a href="{{ route('productCategory.getEdit', ['id'=>$item2->id]) }}">{{ $item2->name }}</a></td>
                                                        <td class="item-intro">{{ $item2->order_no }}</td>
                                                        <td>{{ $count }}</td>
                                                        <td style="display: none"
                                                            class="id id-{{ $item2->id }}">{{ $item2->id }}</td>
                                                    </tr>
                                                    <?php $childs2 = $item2->childs;?>
                                                    @foreach($childs2 as $item3)
                                                        <tr class="child child-{{$id}}">
                                                            <td><input type="checkbox" class="ids" name="id[]"
                                                                       value="{{ $item3->id }}"></td>
                                                            <td>{{ $item3->id }}</td>
                                                            <td class="item-name">______<a href="{{ route('productCategory.getEdit', ['id'=>$item3->id]) }}">{{ $item3->name }}</a></td>
                                                            <td class="item-intro">{{ $item3->order_no }}</td>
                                                            <td>{{ $count }}</td>
                                                            <td style="display: none"
                                                                class="id id-{{ $item3->id }}">{{ $item3->id }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        {{--END: Sidebar--}}

        {{--Main--}}
        <div class="row">
            <div class="col-lg-8">

            </div>
        </div>
        {{--END: Main--}}
    </div>

    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'category'])

@stop

@section('custom_header')
    <link href="{{ URL::asset('css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style>
        .child {
            display: none;
        }
        span.down {
            color: red;
            font-weight: bold;
            float: right;
        }
    </style>
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('js/partials/initFormTable.js') }}"></script>

    <script>
        // DataTable
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true,
                paging: false
            });
        });

        $('span.down').each(function () {
            $(this).click(function () {
                var tr = $(this).parents('tr');
                var id = tr.data('action');
                $('tr.child.child-'+id).slideToggle();
            });
        });

        $('.panel').on('keyup', 'input', function () {

            if($(this).val().length > 0) {
                $('tr.child').fadeIn();
            } else {
                $('tr.child').fadeOut();
            }
        });

    </script>

@stop