@extends('backend.layouts.master')

@section('main_content')

    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.post') }}
                    <a class="btn btn-success" href="{{ route('post.getCreate') }}">{{ trans('form.btn_add') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        {{--List post--}}
        <div class="row list-post">
            <form class="col-xs-12 top-action" method="GET" action="">
                <div class="col-md-2">
                    <select name="search_date" class="form-control">
                        <option value="0">{{ trans('form.all_date') }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 1 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 1 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 2 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 2 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 3 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 3 month')) }}</option>
                        <option value="{{ date('m-Y', strtotime(date('M Y') . '- 4 month')) }}">{{ date('m-Y', strtotime(date('M Y') . '- 4 month')) }}</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="search_category" class="form-control">
                        <option value="0">{{ trans('form.all_category') }}</option>
                        @foreach($listCategory as $item)
                            @if($item->parent_id == 0)
                                {{--Option parent--}}
                                <option value="{{ $item->id }}"
                                        class="parent-id-{{ $item->id }}">{{ $item->name }}</option>
                                <?php
                                $list_childs = $item->childs; ?>
                                @if($list_childs != null)
                                    @foreach($list_childs as $one)
                                        {{--Option child level 1--}}
                                        <option value="{{ $one->id }}"
                                                class="parent-id-{{ $one->id }}">
                                            --{{ $one->name }}</option>
                                        <?php
                                        $list_childs2 = $one->childs;
                                        if($list_childs2 != null)?>
                                        @foreach($list_childs2 as $one2)
                                            {{--Option child level 2--}}
                                            <option value="{{ $one2->id }}"
                                                    class="parent-id-{{ $one2->id }}">
                                                ----{{ $one2->name }}</option>
                                        @endforeach
                                    @endforeach
                                @endif
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="search_status" class="form-control">
                        <option value="">{{ trans('post.status') }}</option>
                        <option value="1">{{ trans('post.publish') }}</option>
                        <option value="0">{{ trans('post.draf') }}</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input name="keyword" value="" class="form-control">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary">Search</button>
                </div>
            </form>

            <div class="col-xs-12">
                <div class="panel panel-default list-post">
                    <div class="panel-heading">
                        <span>{{ trans('system.list_post') }}</span>
                        <ul class="subsubsub">
                            <?php
                            $count_all_post = \App\Models\Post::withTrashed()->get()->count();
                            ?>
                            <li class="all"><span><span>All</span> <span class="count">({{ $count_all_post }}
                                        )</span></span>&nbsp;|&nbsp;
                            </li>
                            <li class="publish"><span><span>Published</span> <span
                                            class="count">({{ count($listPost) }})</span></span>&nbsp;|&nbsp;</li>
                            <li class="draft"><span><span>Drafts</span> <span
                                            class="count">({{ $count_all_post - count($listPost) }})</span></span></li>
                        </ul>
                        <div>
                            <select class="form-control actions" name="action">
                                <option value="">Actions</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @if($listPost)
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" value="" class="checkbox-master"></th>
                                        <th>{{ trans('form.image') }}</th>
                                        <th>{{ trans('form.name') }}</th>
                                        <th>{{ trans('system.category') }}</th>
                                        <th>{{ trans('system.tags') }}</th>
                                        <th>{{ trans('form.author') }}</th>
                                        <th>{{ trans('post.status') }}</th>
                                        <th>{{ trans('form.view') }}</th>
                                        <th>{{ trans('form.created_at') }}</th>
                                        <th style="display: none;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listPost as $item)
                                        <tr>
                                            <td><input type="checkbox" class="ids" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td class="item-image"><img
                                                        src="{{ getUrlImageThumb($item->image, env('IMAGE_THUMB_WIDTH'), 'auto') }}"/></td>
                                            <td class="item-name">@if(isset($item->category))<a href="{{ route('post.getEdit', ['id'=>$item->id]) }}">{{ $item->name }}</a>@endif</td>
                                            <td class="item-category">@if(isset($item->category)){{ $item->category->name }}@endif</td>
                                            <?php $tags = $item->tags;?>
                                            <td class="item-tags">
                                                @if(!empty($tags))
                                                    @foreach($tags as $key => $tag)
                                                        @if($key == 0)
                                                            @if(isset($tag->name)){{ $tag->name }}@endif,
                                                        @else
                                                            @if(isset($tag->name)){{ $tag->name }}@endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>@if(isset($item->user->name)){{ $item->user->name }}@endif</td>
                                            <td class="item-status"><img src="@if($item->status== 0){{ URL::asset('images/unpublish.png') }}@else{{ URL::asset('images/published.png') }}@endif"></td>
                                            <td>@if(isset($item->category))<a href="{{ URL::to($item->category->slug.'/'.$item->slug) }}">{{ trans('system.view') }}</a>@endif</td>
                                            <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                            <td style="display: none;"
                                                class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- /.panel-body -->

                    {{--Paginate--}}
                    {!! $listPost->appends($appends)->links() !!}
                </div>
            </div>
        </div>
        {{--END: List post--}}

    </div>


    {{--Include packge--}}
    @include('backend.partials.delete_publish_table', ['name' => 'post'])

@stop