@extends('backend.layouts.master')

@section('main_content')
    <div class="container-fluid">
        {{--Header page--}}
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ trans('system.post') }}
                    <a href="{{ route('post') }}"
                       class="btn btn-success btn-add-post">{{ trans('form.btn_cancel') }}</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{--END: Header page--}}

        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        {{--Create post--}}
        <form class="row add-post" method="POST" action="" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading table-heading">
                        <div class="table-heading-name">{{ trans('system.post_info') }}</div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('form.name') }}</label>
                                    <input class="form-control" name="name" value="{{ $post->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.slug') }}</label>
                                    <input class="form-control" name="slug" value="{{ $post->slug }}" required>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.description') }}</label>
                                    <textarea class="form-control" name="intro" rows="5">{!! $post->intro !!}</textarea>
                                </div>
                                <div class="form-group editor">
                                    <label>{{ trans('form.content') }}</label>
                                    <textarea class="form-control content" rows="15"
                                              name="content">{!! $post->content !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Video</label>
                                    @if($post->video !='')
                                        <input class="form-control" name="video"
                                               value="https://www.youtube.com/watch?v={{ $post->video }}">
                                    @else
                                        <input class="form-control" name="video"
                                               value="">
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"
                                                   name="status" @if($post->status==1){{ 'checked' }}@endif>{{ trans('form.publish') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.seo_title') }}</label>
                                    <input class="form-control" name="seo_title" value="{{ $post->seo_title }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.seo_description') }}</label>
                                    <input class="form-control" name="seo_description"
                                           value="{{ $post->seo_description }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('form.seo_keywords') }}</label>
                                    <input class="form-control" name="seo_keywords" value="{{ $post->seo_keywords }}">
                                </div>
                                <input class="btn btn-primary" value="{{ trans('form.btn_edit') }}" type="submit">
                                <button class="btn btn-default reset"
                                        onclick="resetForm('.add-post')">{{ trans('form.btn_reset') }}</button>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('system.category_info') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">

                                @include('backend.partials.image_thumb_default', ['name'=>'image', 'src' => $post->image])

                                <div class="form-group">
                                    <label>{{ trans('system.tags') }}</label>
                                    <textarea id="tags" name="tags" class="tag-editor-hidden-src" readonly="readonly"
                                              style="display: block;"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>{{ trans('system.category') }}</label>
                                    <select class="form-control" name="category_id">
                                        @foreach($listCategory as $item)
                                            @if($item->parent_id == 0)
                                                {{--Option parent--}}
                                                <option value="{{ $item->id }}"
                                                        @if($item->id==$post->category->id){{ 'selected' }}@endif
                                                        class="category-id-{{ $item->id }}">{{ $item->name }}</option>
                                                <?php
                                                $list_childs = $item->childs;?>
                                                @if($list_childs != null)
                                                    @foreach($list_childs as $one)
                                                        {{--Option child level 1--}}
                                                        <option value="{{ $one->id }}"
                                                                @if($one->id==$post->category->id){{ 'selected' }}@endif
                                                                class="category-id-{{ $one->id }}">
                                                            --{{ $one->name }}</option>
                                                        <?php
                                                        $list_childs2 = $one->childs;
                                                        if ($list_childs2 != null) ?>
                                                        @foreach($list_childs2 as $one2)
                                                            {{--Option child level 2--}}
                                                            <option value="{{ $one2->id }}"
                                                                    @if($one2->id==$post->category->id){{ 'selected' }}@endif
                                                                    class="category-id-{{ $one2->id }}">
                                                                ----{{ $one2->name }}</option>
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </form>
        {{--END: Create post--}}
    </div>

@stop

@section('custom_header')
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.tag-editor.css') }}">
@stop

@section('custom_footer')
    <script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.caret.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.tag-editor.js') }}"></script>
    <script src="{{ URL::asset('js/partials/initFormTable.js') }}"></script>

    <script>
        $('#tags').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: {collision: 'flip'}, // automatic menu position up/down
                source: []
            },
            initialTags: [
                @foreach($tags as $item)
                        '{{ $item }}',
                @endforeach
            ],
            forceLowercase: false,
            placeholder: 'Enter tags ...'
        });

    </script>

@stop