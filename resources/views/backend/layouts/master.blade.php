<head>

@include('backend.partials.header')

</head>

<body>

<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        @include('backend.partials.menu')

        @include('backend.partials.sidebar_left')

    </nav>

    <div id="page-wrapper" style="min-height: 196px;">
        @yield('main_content')
        <!-- /.container-fluid -->
    </div>

</div>

@include('backend.partials.footer')

</body>