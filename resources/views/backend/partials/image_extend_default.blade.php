<div class="file @if(isset($multiple)){{ 'multiple' }}@endif">
    <div class="div0-{{ $name }}">
        <div class="cmsms_img  cmsms_image_c image-holder{{ $name }}">
            @if(isset($src) && $src != '')
                @if(is_array($src))
                    @foreach($src as $item)
                        @if($item != '')
                            <div class="extend">
                                <img src="{{ URL::asset('filemanager/userfiles/' . $item) }}" alt="">
                                <span class="delete-img img-{{ $name }}" data-name="{{ $name }}">X</span>
                            </div>
                        @endif
                    @endforeach
                @else
                    <img src="{{ URL::asset('filemanager/userfiles/' . $src) }}" alt="">
                    <span class="delete-img img-{{ $name }}" data-name="{{ $name }}">X</span>
                @endif
            @endif
        </div>
        <input class="fileUpload{{ $name }} file"
               type="file" @if(isset($multiple)){{ ' name='.$name.'[] multiple' }}@else{{ ' name='.$name.' multiple=false' }}@endif>
    </div>
</div>


<script>
    $(".fileUpload{{ $name }}").on('change', function () {

        //Get count of selected files
        var countFiles = $(this)[0].files.length;

        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $(".image-holder{{ $name }}");
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == 'pdf' || extn == 'doc' || extn == 'docx') {
            if (typeof (FileReader) != "undefined") {

                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++) {

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "thumb-image"
                        }).appendTo(image_holder);
                    };

                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[i]);
                }

            } else {
                alert("This browser does not support FileReader.");
            }
        } else {
            alert("Vui lòng chọn file ảnh");
        }
    });

    @if(isset($id))
    $('.delete-img.img-{{ $name }}').each(function () {
        $(this).click(function () {
            var name = $(this).data('name');
            $.ajax({
                url: '{{ route('deleteImage') }}',
                type: 'POST',
                dataType: 'json',
                data: {name: name, id: '{{ $id }}', type: 'product', _token: '{{ csrf_token() }}'},
                success: function (resp) {
                    if (resp.status == 'success') {
                        $('.image-holder{{ $name }}').html('');
                    } else {
                        swal({title: 'Lỗi!', text: 'Không xóa được', type: 'error'});
                    }
                },
                error: function (resp) {
                    swal({title: '{{ trans('system.ajax_eidt_error') }}', type: 'error'});
                }
            });
        })
    });
    @endif
</script>