<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ URL::to('dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>

            <li>
                <a href="{{ route('category') }}"><i class="fa fa-folder-open fa-fw"></i> Danh mục</a>
            </li>

            <li>
                <a href="{{ route('post') }}"><i class="fa fa-tags fa-fw"></i> Bài viết</a>
            </li>

            <li>
                <a href="{{ route('housetype') }}"><i class="fa fa-folder-open fa-fw"></i> Loại nhà</a>
            </li>

            <li>
                <a href="{{ route('house') }}"><i class="fa fa-tags fa-fw"></i> Nhà</a>
            </li>

            <li>
                <a href="{{ route('savehouse') }}"><i class="fa fa-tags fa-fw"></i> Save nhà</a>
            </li>

            <li>
                <a href="{{ route('savelocation') }}"><i class="fa fa-tags fa-fw"></i> Save location</a>
            </li>

            <li>
                <a href="{{ route('notification') }}"><i class="fa fa-tags fa-fw"></i> Notification</a>
            </li>

            <li>
                <a href="{{ route('fllow') }}"><i class="fa fa-tags fa-fw"></i> Fllow</a>
            </li>

            <li>
                <a href="{{ route('service') }}"><i class="fa fa-tags fa-fw"></i> Service</a>
            </li>

            <li>
                <a href="{{ route('report') }}"><i class="fa fa-tags fa-fw"></i> Report</a>
            </li>

            <li>
                <a href="{{ route('user') }}"><i class="fa fa-users fa-fw"></i> User</a>
            </li>

            @if(\Auth::user()->can('view.setting'))
                <li>
                    <a href="{{ route('setting') }}"><i class="fa fa-cogs fa-fw"></i> {{ trans('system.settings') }}
                    </a>
                </li>
            @endif
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->