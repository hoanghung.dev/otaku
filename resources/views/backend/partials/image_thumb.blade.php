<div class="thumb">
    <div class="div0-{{ $name }}">
        <div class="div1-{{ $name }}" style="display: none;">
            <div class="thumbactions_img">
                <div class="wrap-thumbnail"></div>
                <div class="thumbactions" data-thumb="thumbnail">
                    <a class="btn btn-block btn-danger deleteimage" name="{{ $name }}"><i
                                class="fa fa-trash"></i></a>
                </div>
            </div>
        </div>
        <div class="div2-{{ $name }}" style="display: block;">
            <i class="fa fa-plus fa-2x" onclick="BrowseServer('{{ $name }}');"></i>
            <h4 class="text-muted">Pick a thumb {{ $name }}.</h4>
            <input type="hidden" id="{{ $name }}" name="{{ $name }}">
            <div style="font-size:12px;color:#ccc"> or</div>
            <div class="{{ $name }}-upload-actions">
                <a class="btn btn-success getimageurl" name="{{ $name }}">Get from the Url <i
                            class="fa fa-download"></i></a>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        @if(isset($src) && $src != '')
        $('.div0-{{ $name }}').find('.wrap-thumbnail').html('<img src="http://' + location.host + '/filemanager/userfiles/' + '{{ $src }}">');
        $('.thumb .div1-{{ $name }}').css('display', 'block');
        $('.thumb .div2-{{ $name }}').css('display', 'none');
        $('input[name={{ $name }}]').val('{{ $src }}');
        @endif

        $('.thumb input#{{ $name }}').change(function () {
            $('.div0-' + $(this).attr('name')).find('.wrap-thumbnail').html('<img src="' + $(this).val() + '">');
            $('.div1-' + $(this).attr('name')).css('display', 'block');
            $('.div2-' + $(this).attr('name')).css('display', 'none');
        });
    });


    $('.getimageurl').each(function () {
        var img = $(this).attr('name');
        var div = $('.div0-' + $(this).attr('name'));
        $(this).click(function () {
            swal({
                title: "An input!",
                text: "Write something interesting:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Write something"
            }, function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("You need to write something!");
                    return false
                }
                div.find('input[name=' + img + ']').val(inputValue);
            });
        });
    });

    $('.deleteimage').each(function () {          // Delete
        $(this).click(function () {
            $('.div0-' + $(this).attr('name')).find('.wrap-thumbnail').html('');
            $('.thumb .div1-' + $(this).attr('name')).css('display', 'none');
            $('.thumb .div2-' + $(this).attr('name')).css('display', 'block');
            $('#' + $(this).attr('name')).val('NULL');
        });
    });

</script>