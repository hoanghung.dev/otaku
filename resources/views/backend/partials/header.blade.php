<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="{{ URL::asset('filemanager/userfiles/'. $setting->favicon) }}" type="image/x-icon">

<meta content="noindex, nofollow" name="robots">

<title>@yield('title_page')</title>

<!-- Bootstrap Core CSS -->
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="{{ URL::asset('css/metisMenu.min.css') }}" rel="stylesheet">

<!-- Custom CSS -->
<link href="{{ URL::asset('css/sb-admin-2.css') }}" rel="stylesheet">

<link href="{{ URL::asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css">

<!-- Custom Fonts -->
<link href="{{ URL::asset('fonts/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

{{--Custom Css--}}
<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>

@yield('custom_header')