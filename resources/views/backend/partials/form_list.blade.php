@if($type == 'text')
    <td class="item-{{$feild['name']}}">{{ $item->$feild['name'] != '' ? $item->$feild['name'] : 'Không rõ' }}</td>
@elseif($type == 'price')
        <td class="item-{{$feild['name']}}">{{ $item->$feild['name'] != '' ? $item->$feild['name'] . ' triệu' : 'Không rõ' }}</td>
@elseif($type == 'name')
    <td class="item-{{$feild['name']}}"><a href="{{ route($module['name'].'.getEdit', ['id'=>$item->$module['primaryKey']]) }}">{{ $item->$feild['name'] }}</a></td>
@elseif($type == 'user_id')
    <td class="item-{{$feild['name']}}">@if(isset($item->user->name))<a href="{{ route($module['name'].'.getEdit', ['id'=>$item->$module['primaryKey']]) }}">{{ $item->user->name }}</a>@endif</td>
@elseif($type == 'created_at')
    <td>{{ date('d/m/Y H:i:s', strtotime($item->$feild['name'])) }}</td>
@elseif($type == 'status')
    <td style="text-align: center;" id="img{{ $feild['name'] }}-{{ $item->$module['primaryKey'] }}"><img
                data-id="{{ $item->$module['primaryKey'] }}" class="{{$feild['name']}}"
                style="cursor:pointer;"
                src="@if($item->$feild['name']==1){{ URL::asset('images/published.png') }}@elseif($item->$feild['name']==0){{ URL::asset('images/unpublish.png') }}@else{{ URL::asset('images/warning.png') }}@endif">
    </td>
@elseif($type == 'img_thumb')
    <td class="item-{{$feild['name']}}"><img
                src="{{ getUrlImageThumb($item->$feild['name'], env('IMAGE_THUMB_WIDTH'), 'auto') }}"
                style="max-width: 97px;"/></td>
@elseif($type == 'belongTo')
    <td class="item-{{$feild['name']}}">@if(isset($item->$feild['data']->name)){{ $item->$feild['data']->name }}@endif</td>
@endif