<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
    $('input[name={{ $name }}]').keyup(function () {
        name = $(this).val();
        if(name.length > 2) {
            $.ajax({
                url: '{{ URL::to('dashboard/autocomplete') }}',
                type: 'POST',
                data: { name : name, type: '{{ $type }}' , _token : '{{ csrf_token() }}' },
                success: function (resp) {
                    $(function() {
                        $("input[name={{ $name }}]").autocomplete({
                            source: resp.data
                        });
                    });
                },
                error: function (resp) {
                    alert('Complete error!');
                }
            });
        }
    });
</script>