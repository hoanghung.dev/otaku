<script>
    $('select[name=action]').change(function () {

        //  Get list id
        var list_id = [];
        var i = 0;
        $('.ids:checked').each(function () {
            list_id[i] = $(this).val();
            i++;
        });

        // If no selected alert error
        if (list_id.length == 0) {
            swal({title: 'Please select {{ $name }}', type: 'error'});
        } else {

            var action = $(this).val();

            if (action == 'delete') {

                swal({
                    title: "Bạn có chắc chắn xóa?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $.ajax({
                        url: '{{ route($name.'.postDelete') }}',
                        type: 'post',
                        dataType: 'json',
                        data: {list_id: list_id, _token: '{{ csrf_token() }}'},
                        success: function (resp) {
                            location.reload();
                            // Delete tr
                            resp.list_id.forEach(function (value, key) {
                                $('table .id-' + value).parent('tr').remove();
                            });

                            // Alert result
                            if (resp.status != 'success') {
                                swal({title: resp.status, text: resp.msg, type: resp.status});
                            }
                        },
                        error: function (resp) {
                            swal({title: '{{ trans('system.ajax_delete_error') }}', type: 'error'});
                        }
                    });
                });

            } else if (action == 'publish') {
                list_id.forEach(function (value, key) {
                    $('table #img-' + value + ' img').click();
                });
            }
        }
        /*Reset value select*/
        $(this).find('option:first').attr('selected', 'selected');
        $(this).find('option:first').removeAttr('selected');
    });
</script>