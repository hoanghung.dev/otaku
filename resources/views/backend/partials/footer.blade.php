
<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ URL::asset('js/metisMenu.min.js') }}"></script>

<script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ URL::asset('js/sb-admin-2.js') }}"></script>

<script type="text/javascript" src="{{ url('') }}/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/tinymce/tinymce_editor.js"></script>
<script src="{{ URL::asset('js/custom.js') }}"></script>
<script type="text/javascript">
    editor_config.selector = ".content";
    editor_config.path_absolute = "http://"+domain+"/public/";
    tinymce.init(editor_config);
</script>

@yield('custom_footer')