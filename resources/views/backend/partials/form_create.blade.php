@if($type == 'text')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <label>{{ $feild['label'] }}</label>
        <input type="text" class="form-control" name="{{ $feild['name'] }}" {!! $feild['inner'] !!}>
    </div>
@elseif($type == 'number')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <label>{{ $feild['label'] }}</label>
        <input type="number" class="form-control" name="{{ $feild['name'] }}" {!! $feild['inner'] !!}>
    </div>
@elseif($type == 'datetime')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <label>{{ $feild['label'] }}</label>
        <input type="datetime-local" class="form-control" name="{{ $feild['name'] }}" {!! $feild['inner'] !!}>
    </div>
@elseif($type == 'textarea')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <label>{{ $feild['label'] }}</label>
        <textarea class="form-control" name="{{ $feild['name'] }}"
                  rows="@if(isset($feild['rows'])){{ $feild['rows'] }}@else{{ 5 }}@endif" {!! $feild['inner'] !!}></textarea>
    </div>
@elseif($type == 'editor')
    <div class="form-group editor col-md-{{ $feild['col'] }}">
        <label>{{ $feild['label'] }}</label>
        <textarea class="form-control content" rows="@if(isset($feild['rows'])){{ $feild['rows'] }}@else{{ 15 }}@endif"
                  name="{{ $feild['name'] }}"></textarea>
    </div>
@elseif($type == 'select')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <label>{{ $feild['label'] }}</label>
        <select name="{{ $feild['name'] }}" class="form-control" {!! $feild['inner'] !!}>
            @foreach($feild['data'] as $key =>$value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
@elseif($type == 'select2')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <label>{{ $feild['label'] }}</label>
        <select class="{{ $feild['name'] }} col-md-{{ $feild['col'] }}" multiple="multiple" name="{{ $feild['name'] }}">
            @foreach($feild['data'] as $val)
                <option value="{{ $val }}">{{ $val }}</option>
            @endforeach
        </select>
        <script type="text/javascript">
            $(".{{ $feild['name'] }}").select2();
        </script>
    </div>
@elseif($type == 'checkbox')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="{{ $feild['name'] }}" {!! $feild['inner'] !!}> {{ $feild['label'] }}
            </label>
        </div>
    </div>
@elseif($type == 'img_filemanager')
    <div class="form-group col-md-{{ $feild['col'] }}">
        @include('backend.partials.image_thumb', ['name'=>$feild['name']])
    </div>
@elseif($type == 'img_default')
    <div class="form-group col-md-{{ $feild['col'] }}">
        @include('backend.partials.image_thumb_default', ['name'=>$feild['name'], 'multi'=>isset($feild['multi']) ? $feild['multi'] : false])
    </div>
@elseif($type == 'category')
    <div class="form-group col-md-{{ $feild['col'] }}">
        <?php $listCategory = \App\Models\Category::where('parent_id', 0)->orderBy('name', 'asc')->get();?>
        <label>{{ $feild['label'] }}</label>
        <select class="form-control" name="{{ $feild['name'] }}" {!! $feild['inner'] !!}>
            @foreach($listCategory as $item)
                {{--Option parent--}}
                <option value="{{ $item->id }}"
                        class="category-id-{{ $item->id }}">{{ $item->name }}</option>
                <?php
                $listChilds = $item->childs;?>
                @if($listChilds != null)
                    @foreach($listChilds as $one)
                        {{--Option child level 1--}}
                        <option value="{{ $one->id }}"
                                class="category-id-{{ $one->id }}">
                            --{{ $one->name }}</option>
                        <?php
                        $listChilds2 = $one->childs; ?>
                        @if($listChilds2)
                            @foreach($listChilds2 as $one2)
                                {{--Option child level 2--}}
                                <option value="{{ $one2->id }}"
                                        class="category-id-{{ $one2->id }}">
                                    ----{{ $one2->name }}</option>
                            @endforeach
                        @endif
                    @endforeach
                @endif
            @endforeach
        </select>
    </div>
@endif