@extends('backend.layouts.master')

@section('main_content')
    <?php $isAdmin = \Auth::user()->is('admin');?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ trans('system.dashboard') }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row dashboard">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <?php $count_category = \App\Models\Category::where('status', 1)->get()->count();?>
                            <div class="huge">{{ $count_category }}</div>
                            <div>{{ trans('system.category') }}</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">Xem chi tiết</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-cubes fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <?php $count_product = \App\Models\House::count();?>
                            <div class="huge">{{ $count_product }}</div>
                            <div>Nhà</div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('house') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Xem chi tiết</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        @if($isAdmin)
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <?php $count_user = \App\Models\User::get()->count();?>
                                <div class="huge">{{ $count_user }}</div>
                                <div>{{ trans('system.user') }}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('user') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Xem chi tiết</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        @endif
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">
            <div id="chartContainer3" style="width: 100%; height: 400px;display: inline-block;"></div>
            <div class="col-xs-12">
                <form class="col-xs-12" method="get" action="">
                    <div class="col-md-5">
                        Từ ngày: <input name="start_day" type="date" value="{{ date('Y-m-d', $start_day) }}"
                                        class="form-control">
                    </div>
                    <div class="col-md-5">
                        Đến ngày<input name="end_day" type="date" value="{{ date('Y-m-d', $end_day) }}"
                                       class="form-control">
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary" type="submit" style="margin-top: 20px;">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.col-lg-8 -->
        <div class="col-lg-4 new-product">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bell fa-fw"></i> Nhà mới
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="list-group">
                        @if($listHouse)
                            @foreach($listHouse as $item)
                                <li class="aa">
                                    <div class="col-md-4 item-image">
                                        <img src="{{ URL::asset('filemanager/userfiles/' . $item->image) }}">
                                    </div>
                                    <div class="col-md-8">
                                        <p class="top-title-ss">
                                            <a
                                                    href="#"
                                                    target="_blank">{{ $item->name }}</a></p>
                                        <p class="top-price-ss"><span
                                                    class="ms-number-money2">{!! number_format($item->price,0,",",".") !!}</span>
                                            đ</p>
                                    </div>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                    <!-- /.list-group -->
                    <a href="{{ route('product') }}"
                       class="btn btn-default btn-block">{{ trans('system.view_more') }}</a>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-4 -->
    </div>
    <!-- /.row -->
@endsection

@section('custom_footer')
    <script type="text/javascript" src="{{ URL::asset('js/canvasjs.min.js') }}"></script>
    <script type="text/javascript">
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer3", {
                title: {
                    text: "Thống kê từ {{ date('d/m/Y', $start_day) }} => {{ date('d/m/Y', $end_day) }}",
                    fontSize: 30
                },
                animationEnabled: true,
                axisX: {

                    gridColor: "Silver",
                    tickColor: "silver",
                    valueFormatString: "D/M"

                },
                toolTip: {
                    shared: true
                },
                theme: "theme2",
                axisY: {
                    gridColor: "Silver",
                    tickColor: "silver"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [
                    {
                        type: "line",
                        showInLegend: true,
                        lineThickness: 2,
                        name: "Sản phẩm",
                        markerType: "square",
                        color: "#F08080",
                        dataPoints: [
                                @foreach($data as $item)
                            {
                                x: new Date('{{ date('Y', $item['time']) }}', '{{ date('m', $item['time']) - 1 }}', '{{ date('d', $item['time']) }}'),
                                y: {{ $item['product'] }} },
                            @endforeach


                        ]
                    },
                        @if($isAdmin)
                    {
                        type: "line",
                        showInLegend: true,
                        name: "Đơn hàng",
                        color: "#20B2AA",
                        lineThickness: 2,

                        dataPoints: [
                                @foreach($data as $item)
                            {
                                x: new Date('{{ date('Y', $item['time']) }}', '{{ date('m', $item['time']) - 1 }}', '{{ date('d', $item['time']) }}'),
                                y: {{ $item['order'] }} },
                            @endforeach
                        ]
                    },
                    {
                        type: "line",
                        showInLegend: true,
                        name: "Tiền (đv: 100k)",
                        color: "#337ab7",
                        lineThickness: 2,

                        dataPoints: [
                                @foreach($data as $item)
                            {
                                x: new Date('{{ date('Y', $item['time']) }}', '{{ date('m', $item['time']) - 1 }}', '{{ date('d', $item['time']) }}'),
                                y: {{ $item['pay']/100000 }} },
                            @endforeach
                        ]
                    }
                    @endif

                ],
            });

            chart.render();
        }
    </script>
@stop