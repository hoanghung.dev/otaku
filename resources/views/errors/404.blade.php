@extends('frontend.layouts.master')

@section('main_content')

    <style>
        @media(max-width: 768px) {
            .notfound {
                padding: 0 !important;
            }
        }
    </style>


    <h1 class="notfound" style="text-align: center;padding: 198px;color: #425eab;">Không tìm thấy trang!</h1>

@stop



